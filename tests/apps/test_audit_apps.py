from pathlib import Path
from typing import List, Tuple
import pytest
import pandas as pd
from vod_analytics.audit_utils import (Headers, Labels, read_gt, read_pred, HOV_State,
    ThresholdsTuningApp, EnforcementPerformance)
from vod_analytics.apps import (diff_audits, check_audits_validity, reformat_pred_as_gt,
    make_enforcement_recommendations, audit_enforcement_performance)


def data2csv(data: List[str], directory_path: Path, csv_name: str) -> Path:
    datafile = directory_path / csv_name
    datafile.write_text("\n".join(data))
    return datafile


@pytest.fixture
def mock_supervisor_csv(tmp_path: Path) -> Path:
    data = [
        ','.join(Headers.gt_headers),
        "2023-07-20,15:06:30.239,2,excellent,,,Supervisor",
        "2023-07-21,11:05:21.138,3+,unreadable,,,Supervisor",
        "2023-07-21,18:54:10.935,0+,undeterminable,,,Supervisor",
        "2023-07-22,22:06:30.239,more,good,,,Supervisor",
        "2023-07-23,11:02:51.118,3,min_viable,,,Supervisor",
        ]
    return data2csv(data, tmp_path, 'supervisor.csv')


@pytest.fixture
def mock_annotator_csv(tmp_path: Path) -> Path:
    data = [
        ','.join(Headers.gt_headers),
        "2023-07-20,15:06:30.239,2,good,,,Annotator",
        "2023-07-21,11:05:21.138,3+,min_viable,,,Annotator",
        "2023-07-21,18:54:10.935,1+,undeterminable,,,Annotator",
        "2023-07-22,22:06:30.239,more,excellent,,,Annotator",
        "2023-07-23,11:02:51.118,1,min_viable,,,Annotator",
        ]
    return data2csv(data, tmp_path, 'annotator.csv')


@pytest.fixture
def mock_diff_csv(tmp_path: Path) -> Path:
    data = [
        ','.join([*Headers.gt_headers,
                  f'{Headers.gt_count}_suggested', f'{Headers.gt_countability}_suggested']),
        "2023-07-21,18:54:10.935,1+,undeterminable,,,Annotator,0+,undeterminable",
        "2023-07-23,11:02:51.118,1,min_viable,,,Annotator,3,min_viable",
        ]
    return data2csv(data, tmp_path, 'expected_diff.csv')


@pytest.fixture
def mock_invalid_annotator_csv(tmp_path: Path) -> Path:
    allowed = Labels.allowed_count_countability['3+']
    data = [
        ','.join([*Headers.gt_headers, 'inconsistency_explained']),
        ','.join(["2023-07-21,11:05:21.138,3+,min_viable,,,Annotator",
                  f"Only countabilities {' & '.join(allowed)} are allowed when count = 3+."])
        ]
    return data2csv(data, tmp_path, 'expected_invalid_count_countability.csv')


@pytest.fixture
def mock_predictions_csv(tmp_path: Path) -> Path:
    # pred_headers = [date, time, pred_count, pred_quality_score, pred_LOV_confidence]
    data_pred = [
        ','.join(Headers.pred_headers),
        "2023-07-20,15:06:30.239,2,99%,55%",
        "2023-07-21,11:05:21.138,3,10%,85%",
        "2023-07-21,18:54:10.935,1,30%,95%",
        "2023-07-22,22:06:30.239,5,75%,20%",
        "2023-07-23,11:02:51.118,3,61%,40%",
        "2023-07-23,17:10:43.420,1,96%,95%",
        ]
    
    return data2csv(data_pred, tmp_path, 'predictions.csv')


@pytest.fixture
def mock_pred_reformatted_as_gt(tmp_path: Path) -> Tuple[Path, List[str]]:
    # gt_headers = [date, time, gt_count, gt_countability, *gt_extras]
    data_as_gt = [
        ','.join(Headers.gt_headers),
        "2023-07-20,15:06:30.239,2,excellent,,,",
        "2023-07-21,11:05:21.138,3+,unreadable,,,",
        "2023-07-21,18:54:10.935,1+,undeterminable,,,",
        "2023-07-22,22:06:30.239,more,good,,,",
        "2023-07-23,11:02:51.118,3,min_viable,,,",
        "2023-07-23,17:10:43.420,1,excellent,,,",
        ]
    qual_thrs_str = ['0.25', '0.5', '0.7', '0.9']
    return data2csv(data_as_gt, tmp_path, 'expected_predictions_as_gt.csv'), qual_thrs_str


@pytest.fixture
def mock_gt_csv(tmp_path: Path) -> Path:
    # gt_headers = [date, time, gt_count, gt_countability, *gt_extras]
    data_as_gt = [
        ','.join(Headers.gt_headers),
        "2023-07-20,15:06:30.239,2,excellent,,,",
        "2023-07-21,11:05:21.138,3,min_viable,,,",
        "2023-07-21,18:54:10.935,1+,undeterminable,,,",
        "2023-07-22,22:06:30.239,more,good,,,",
        "2023-07-23,11:02:51.118,2,min_viable,,,",
        "2023-07-23,17:10:43.420,1,good,,,",
        ]
    return data2csv(data_as_gt, tmp_path, 'gt.csv')


def expected_predicted_enforcement() -> Tuple[str, Tuple[str, str], List[str]]:
    conf_thr_str = '0.8'
    qual_thrs_str = ['0.6', '0.9']
    enforcement_recommendations = [
        HOV_State.LOV_Review,
        HOV_State.LQ,
        HOV_State.LQ,
        HOV_State.HOV,
        HOV_State.HOV,
        HOV_State.LOV,
        ]
    return conf_thr_str, qual_thrs_str, [recom.name for recom in enforcement_recommendations]


def test_diff_audits_creates_correct_csv_output(
        mock_supervisor_csv,mock_annotator_csv, mock_diff_csv) -> None:
    
    diff_audits.main([str(mock_supervisor_csv), str(mock_annotator_csv)])
    diff_csv = mock_annotator_csv.parent.joinpath(f'{mock_annotator_csv.stem}_to_revisit.csv')
    assert diff_csv.is_file(), "The diff_audits app did not create a diff file output."
    
    df_diff = pd.read_csv(diff_csv, dtype=str)
    df_diff_expected = pd.read_csv(mock_diff_csv, dtype=str)
    assert len(df_diff) == len(df_diff_expected), \
        "diff_audits did not find correct # of differences."

    col_of_interest = f'{Headers.gt_count}_suggested'
    assert (df_diff[col_of_interest] == df_diff_expected[col_of_interest]).all(), \
        "diff_audits did not make the expected count suggestions."


def test_check_audits_validity_creates_correct_csv_output(
        mock_annotator_csv, mock_invalid_annotator_csv) -> None:

    check_audits_validity.main([str(mock_annotator_csv)])
    invalid_rows_csv = mock_annotator_csv.parent.joinpath(
        f'{mock_annotator_csv.stem}_invalid_count_countability.csv')
    assert invalid_rows_csv.is_file(), "check_audits_validity did not create an output file."

    df_invalid = pd.read_csv(invalid_rows_csv, dtype=str)
    df_invalid_expected = pd.read_csv(mock_invalid_annotator_csv, dtype=str)
    assert len(df_invalid) == len(df_invalid_expected), \
        "check_audits_validity did not find correct # of invalid rows."
    
    col_of_interest = 'inconsistency_explained'
    assert (df_invalid[col_of_interest] == df_invalid_expected[col_of_interest]).all(), \
        "check_audits_validity did not explain the incosistencies correctly."


def test_reformat_pred_as_gt_creates_correct_csv_output(
        mock_predictions_csv, mock_pred_reformatted_as_gt) -> None:
    
    expected_pred_as_gt_csv, quality_thrs_str = mock_pred_reformatted_as_gt
    reformat_pred_as_gt.main([*quality_thrs_str, '-f', str(mock_predictions_csv)])
    output_csv = mock_predictions_csv.parent.joinpath(f'{mock_predictions_csv.stem}_as_gt.csv')
    assert output_csv.is_file(), "reformat_pred_as_gt did not create an output file."

    df_pred_as_gt_output = read_gt(output_csv)
    df_pred_as_gt_expected = read_gt(expected_pred_as_gt_csv)
    cols_of_interest = [Headers.gt_count, Headers.gt_countability]
    for col in cols_of_interest:
        assert (df_pred_as_gt_output[col] == df_pred_as_gt_expected[col]).all(), \
            f"reformat_pred_as_gt did not derive expected {col} values."


def test_make_enforcement_recommendations_creates_correct_csv_output(mock_predictions_csv) -> None:
    
    conf_thr_str, qual_thrs_str, recom_expected = expected_predicted_enforcement()
    make_enforcement_recommendations.main([str(mock_predictions_csv),
                                        '-c', conf_thr_str, '-q', *qual_thrs_str])
    result_csv = mock_predictions_csv.parent.joinpath(
        f'{mock_predictions_csv.stem}_with_recommendations.csv')
    assert result_csv.is_file(), "make_enforcement_recommendations did not create an output file."
    result_df = pd.read_csv(result_csv, dtype=str)
    recom_predicted = result_df.loc[:, Headers.HOV_State].tolist()
    assert all([pred == expected for pred, expected in zip(recom_predicted, recom_expected)]), \
        "make_enforcement_recommendations did not produce expected recommendations"


def test_audit_enforcement_performance_creates_nonempty_pdf(
        mock_gt_csv, mock_predictions_csv) -> None:
    
    audit_enforcement_performance.main(['-g', str(mock_gt_csv), '-p', str(mock_predictions_csv),
        '-c', '0.8', '-q', '0.6','0.9'])
    result_folder = mock_predictions_csv.parent.joinpath('audit_results')
    result_pdf = result_folder.joinpath('audit_report.pdf')
    assert result_folder.is_dir(), "audit_enforcement_performance did not create a result folder"
    assert result_pdf.is_file(), "audit_enforcement_performance did not create a PDF file"
    assert result_pdf.stat().st_size > 1000, \
        "audit_enforcement_performance created a PDF that is too small (< 1KB)"
    
@pytest.mark.skip(reason="result is None without ipython mock display (TODO)")
def test_ThresholdsTuningApp_creates_EnforcementPerformance_result(
        mock_gt_csv, mock_predictions_csv) -> None:
    
    #TODO: add nbmake for notebook pytesting and separate notebook tests
    sliders_app = ThresholdsTuningApp(read_gt(mock_gt_csv), read_pred(mock_predictions_csv))
    assert isinstance(sliders_app.result, EnforcementPerformance), \
        "ThresholdTuningApp did not produce a result property of type EnforcementPerformance."