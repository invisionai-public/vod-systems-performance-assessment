import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path
import pytest
from typing import Tuple

from vod_analytics.apps import vod_performance_report as report
from vod_analytics.configs import (load_yaml_to_dict,
                                   VODPerformanceReportConfigs)


@pytest.fixture
def sample_tables() -> Tuple[pd.DataFrame, pd.DataFrame]:
    test_data_dir = Path(__file__).resolve().parents[1].joinpath('test-data')
    pred_csv = test_data_dir.joinpath('test_predictions_anonymized.csv')
    gt_csv = test_data_dir.joinpath('test_groundtruth_anonymized.csv')
    return pd.read_csv(pred_csv, dtype='str'), pd.read_csv(gt_csv, dtype='str')


@pytest.fixture
def sample_config() -> Tuple[pd.DataFrame, pd.DataFrame]:
    configs_yml_path = Path(__file__).resolve().parents[2].joinpath(
        'vod_analytics', 'apps', 'vod_performance_report_configs.yml')
    return load_yaml_to_dict(VODPerformanceReportConfigs, configs_yml_path)


def test_validate_and_format_pred_and_gt_dataframes(sample_tables,
                                                    sample_config) -> None:
    table_pred, table_gt = sample_tables
    table_pred_validated, table_gt_validated = report.validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, sample_config)

    # checks:
    # all columns names that we want exist
    EXPECTED_PRED_COLS = [
        'Datetime_pred', 'LPR_pred', 'frontcount_pred', 'rearcount_pred',
        'total_count_pred', 'total_count_confidence_pred', 'VOD_image_link',
        'LPR_image_link'
    ]
    table_pred_result = table_pred_validated.columns.tolist()
    assert set(table_pred_result) == set(
        EXPECTED_PRED_COLS
    ), f"Expected {EXPECTED_PRED_COLS}, found {table_pred_result}"

    EXPECTED_GT_COLS = [
        'Date_gt', 'Time_gt', 'Datetime_gt', 'frontcount_gt', 'rearcount_gt',
        'LPR_gt'
    ]
    table_gt_result = table_gt_validated.columns.tolist()
    assert set(table_gt_result) == set(
        EXPECTED_GT_COLS
    ), f"Expected {EXPECTED_GT_COLS}, found {table_gt_result}"

    assert len(
        table_pred_validated
    ) == 21948, f"Expected 21948 rows, found {len(table_pred_validated)}"
    assert len(table_gt_validated
               ) == 332, f"Expected 332 rows, found {len(table_gt_validated)}"


def test_pred_to_gt_association(sample_tables, sample_config) -> None:
    table_pred, table_gt = sample_tables
    table_pred_validated, table_gt_validated = report.validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, sample_config)

    associated_tables = report.pred_to_gt_association(table_pred_validated,
                                                      table_gt_validated,
                                                      sample_config)

    table_matched_len = len(associated_tables.get('table_matched'))
    assert table_matched_len == 327, ('Expected 327 matched rows, '
                                      f'found {table_matched_len}')
    table_matched_readable_len = len(
        associated_tables.get('table_matched_readable'))
    assert table_matched_readable_len == 311, (
        'Expected 311 matched readable rows, '
        f'found {table_matched_readable_len}')
    table_matched_unreadable_len = len(
        associated_tables.get('table_matched_unreadable'))
    assert table_matched_unreadable_len == 16, (
        'Expected 16 matched unreadable rows, '
        f'found {table_matched_unreadable_len}')
    table_unmatched_len = len(associated_tables.get('table_unmatched'))
    assert table_unmatched_len == 5, ('Expected 5 unmatched rows, '
                                      f'found {table_unmatched_len}')


def test_capture_success_metrics(sample_tables, sample_config) -> None:
    table_pred, table_gt = sample_tables
    table_pred_validated, table_gt_validated = report.validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, sample_config)

    associated_tables = report.pred_to_gt_association(table_pred_validated,
                                                      table_gt_validated,
                                                      sample_config)

    cap_success_metrics = report.capture_success_metrics(
        table_gt, associated_tables.get('table_matched'),
        associated_tables.get('table_matched_readable'), False)

    table_gt_len = len(table_gt)
    table_matched_len = len(associated_tables.get('table_matched'))
    table_matched_readable_len = len(
        associated_tables.get('table_matched_readable'))
    assert cap_success_metrics.get('num_gt_rows') == table_gt_len, (
        f"Expected num_gt_rows value {table_gt_len}, "
        f"found {cap_success_metrics.get('num_gt_rows')}")
    assert cap_success_metrics.get('num_matched_rows') == table_matched_len, (
        f"Expected num_matched_rows value {table_matched_len}, "
        f"found {cap_success_metrics.get('num_matched_rows')}")
    assert cap_success_metrics.get(
        'image_quality_score'
    ) == table_matched_readable_len / table_matched_len, (
        f"Expected image_quality_score value {table_matched_readable_len / table_gt_len}, "
        f"found {cap_success_metrics.get('image_quality_score')}")
    assert cap_success_metrics.get('num_readable_matched_rows') == len(
        associated_tables.get('table_matched_readable')
    ), (f"Expected num_readable_matched_rows value {table_matched_readable_len}, "
        f"found {cap_success_metrics.get('table_matched_readable')}")


def test_occupancy_detection_metrics(sample_tables, sample_config) -> None:
    table_pred, table_gt = sample_tables
    table_pred_validated, table_gt_validated = report.validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, sample_config)

    associated_tables = report.pred_to_gt_association(table_pred_validated,
                                                      table_gt_validated,
                                                      sample_config)

    occupancy_metrics = report.occupancy_detection_metrics(
        associated_tables.get('table_matched_readable'),
        associated_tables.get('table_matched_unreadable'),
        sample_config.count_thresholds)

    assert occupancy_metrics.get('frontwindow_det_accuracy') == 1, (
        "Expected front windows det accuracy as 1, "
        f"got {occupancy_metrics.get('frontwindow_det_accuracy')}")
    assert occupancy_metrics.get(
        'rearwindow_det_accuracy') == 0.9935691318327974, (
            "Expected rear windows det accuracy as 0.9935691318327974, "
            f"got {occupancy_metrics.get('rearwindow_det_accuracy')}")
    assert len(occupancy_metrics.get('occupancy_metrics')) == 4, (
        "Expected 4 dict entries, "
        f"found {len(occupancy_metrics.get('occupancy_metrics'))}")
    expected_table_occ_len = len(
        associated_tables.get('table_matched_readable').dropna())
    assert expected_table_occ_len == len(
        occupancy_metrics.get('table_occupancy_metrics')), (
            f"Expected table_occupacy num rows to be {expected_table_occ_len}"
            f"found {len(occupancy_metrics.get('table_occupancy_metrics'))}")
    assert len(occupancy_metrics.get('occupancy_plots')) == 4, (
        "Expected 4 plots in list, "
        f"found {len(occupancy_metrics.get('occupancy_plots'))}")

    plt.close('all')


def test_print_misclassifications_and_pdf_report_saving(
        tmp_path, sample_tables, sample_config) -> None:
    table_pred, table_gt = sample_tables
    table_pred_validated, table_gt_validated = report.validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, sample_config)

    associated_tables = report.pred_to_gt_association(table_pred_validated,
                                                      table_gt_validated,
                                                      sample_config)

    occupancy_metrics = report.occupancy_detection_metrics(
        associated_tables.get('table_matched_readable'),
        associated_tables.get('table_matched_unreadable'),
        sample_config.count_thresholds)

    # just make sure this runs all the way
    report.print_overcounted_and_undercounted_misclassifications(
        occupancy_metrics['table_occupancy_metrics'], False)
    # no lpr
    sample_config.system_includes_lpr = False
    report.pdf_report_generator(
        0.95,  # stand in value for image quality 
        occupancy_metrics.get('frontwindow_det_accuracy'),
        occupancy_metrics.get('rearwindow_det_accuracy'),
        occupancy_metrics.get('occupancy_metrics'),
        occupancy_metrics.get('table_occupancy_metrics'),
        occupancy_metrics.get('occupancy_plots'),
        associated_tables.get('table_matched_readable'),
        associated_tables.get('table_matched_unreadable'),
        associated_tables.get('table_unmatched'),
        sample_config,
        tmp_path)

    pdf_report_path = tmp_path.joinpath(
        (f'Report_{sample_config.report_gen_params.report_timespan}_'
         f'{sample_config.report_gen_params.vod_system_tag}.pdf'))
    assert pdf_report_path.is_file(
    ), f"Expected to find saved report at {str(pdf_report_path)} but did not."
    plt.close('all')
