import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pandas as pd
import pytest
from typing import Tuple

from tests.load_test_data import (read_from_predicted_annotations_csv,
                                  read_from_groundtruth_annotations_csv)
from vod_analytics.apps import vod_performance_report_2023 as report
from vod_analytics.configs import (load_yaml_to_dict,
                                   VODPerformanceReportConfigs2023)
from vod_analytics.matching import Match_Pred_and_Gt_With


@pytest.fixture(params=[2, 3])
def get_matched_tables(request, sample_config):
    table_pred = report.format_prediction_table(
        read_from_predicted_annotations_csv(), request.param,
        sample_config.prediction_params)
    table_gt = report.format_groundtruth_table(
        read_from_groundtruth_annotations_csv(), request.param,
        sample_config.groundtruth_params)
    table_matched, table_unmatched = Match_Pred_and_Gt_With.ID(
        table_pred, table_gt)
    table_matched_readable, table_matched_unreadable = report.separate_readable_and_unreadable_tables(
        table_matched)
    matched_tables = {
        'matched':
        table_matched,
        'unmatched':
        table_unmatched,
        'matched_readable':
        table_matched_readable,
        'matched_unreadable':
        table_matched_unreadable
    }
    yield matched_tables, request.param
    plt.close('all')


@pytest.fixture
def sample_config() -> Tuple[pd.DataFrame, pd.DataFrame]:
    configs_yml_path = Path(__file__).resolve().parents[2].joinpath(
        'vod_analytics', 'apps', 'vod_performance_report_2023_configs.yml')
    return load_yaml_to_dict(VODPerformanceReportConfigs2023, configs_yml_path)


def test_format_prediction_table(
        sample_config: VODPerformanceReportConfigs2023) -> None:
    table_pred = report.format_prediction_table(
        read_from_predicted_annotations_csv(), 3,
        sample_config.prediction_params)
    assert len(
        table_pred
    ) == 352, f"Expected to find 352 rows. Found {len(table_pred)} instead."
    expected_cols = [
        'vehicle_id_pred', 'timestamp_pred', 'vehicle_class_pred',
        'total_count_pred', 'total_count_confidence_pred',
        'vod_image_link_pred', 'total_count_int_pred', 'hov_status_pred'
    ]
    result_cols = list(table_pred.columns.values)
    assert result_cols == expected_cols, (
        f"Expected column headers: [{expected_cols}]. "
        f"Instead got: [{result_cols}]")


def test_format_groundtruth_table(
        sample_config: VODPerformanceReportConfigs2023) -> None:
    table_gt = report.format_groundtruth_table(
        read_from_groundtruth_annotations_csv(), 3,
        sample_config.groundtruth_params)
    # one row is skipped and
    # two rows have been modified to fail the sanity check
    assert len(
        table_gt
    ) == 349, f"Expected to find 349 rows. Found {len(table_gt)} instead."

    expected_cols = [
        'vehicle_id_gt', 'timestamp_gt', 'total_count_gt', 'countability_gt',
        'vod_image_link_gt', 'hov_status_gt', 'total_count_int_gt', 'bin_countability_gt'
    ]
    result_cols = list(table_gt.columns.values)
    assert result_cols == expected_cols, (
        f"Expected column headers: [{expected_cols}]. "
        f"Instead got: [{result_cols}]")


def test_separate_readable_and_unreadable_tables(
        get_matched_tables: Tuple[dict, int]) -> None:
    table_matched = get_matched_tables[0].get('matched')

    table_matched_readable, table_matched_unreadable = report.separate_readable_and_unreadable_tables(
        table_matched)

    assert len(table_matched_readable) == 150, (
        "Expected 150 readable matched rows. "
        f"Got {len(table_matched_readable)}.")

    assert len(table_matched_unreadable) == 199, (
        "Expected 201 readable matched rows. "
        f"Got {len(table_matched_unreadable)}.")


def test_calculate_image_quality_score(
        get_matched_tables: Tuple[dict, int]) -> None:
    matched_tables, _ = get_matched_tables
    image_quality_score = report.calculate_image_quality_score(
        matched_tables.get('matched'))

    expected_img_quality = len(matched_tables.get('matched_readable')) / len(
        matched_tables.get('matched'))
    assert image_quality_score == expected_img_quality, (
        f"Expected image quality score to be {expected_img_quality}. "
        f"Instead is {image_quality_score}")


def test_find_and_print_misclassifications(
        get_matched_tables: Tuple[dict, int]) -> None:
    tables_matched, count_threshold = get_matched_tables
    result_misclass = report.find_and_print_misclassifications(
        tables_matched.get('matched_readable'))

    EXPECTED_COLS = [
        'vod_image_link_pred', 'vod_image_link_gt', 'total_count_pred',
        'total_count_gt'
    ]
    assert list(result_misclass.get('overcounted').columns) == EXPECTED_COLS
    assert list(result_misclass.get('undercounted').columns) == EXPECTED_COLS
    assert len(result_misclass.get('overcounted')) == 12, (
        "Expected 12 overcounted. "
        f"Got {len(result_misclass.get('overcounted'))}")
    assert len(result_misclass.get('undercounted')) == 11, (
        "Expected 11 undercounted. "
        f"Got {len(result_misclass.get('undercounted'))}")


@pytest.mark.parametrize("count_threshold", [2, 3])
def test_consolidate_all_performance_metrics(count_threshold: int) -> None:

    occ_det_metrics = {
        'accuracy': 0.97,
        'overcounting': 0.01,
        'undercounting': 0.02,
        'CM': np.array([[45, 1], [2, 52]])
    }
    cm_results = {
        "tpr_count_only": 0.88,
        "fpr_count_only": 0.12,
        "roc": b''  # stand-in
    }

    perf_mets = report.consolidate_all_performance_metrics(
        0.95, occ_det_metrics, cm_results, count_threshold)

    assert perf_mets.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plot_pred_gt_comparisons(
        get_matched_tables: Tuple[dict, int]) -> None:
    tables_matched, count_threshold = get_matched_tables
    plots = report.plot_pred_gt_comparisons(
        tables_matched.get('matched_readable'),
        tables_matched.get('matched_unreadable'),
        tables_matched.get('matched')['hov_status_pred'], count_threshold)

    assert plots.get('LQ Predicted Counts').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'
    assert plots.get('GT vs Pred HOV Status').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'
    assert plots.get('LQ LOcc Pred Total Confidence').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'
    assert plots.get('HQ LOcc Pred Total Confidence').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


@pytest.mark.parametrize("count_threshold", [2, 3])
def test_generate_performance_report(
        tmp_path: Path, count_threshold: int,
        sample_config: VODPerformanceReportConfigs2023) -> None:
    table_pred = report.format_prediction_table(
        read_from_predicted_annotations_csv(), count_threshold,
        sample_config.prediction_params)
    table_gt = report.format_groundtruth_table(
        read_from_groundtruth_annotations_csv(), count_threshold,
        sample_config.groundtruth_params)

    sample_config.count_threshold = count_threshold
    # make sure it runs all the way through
    report_buf = report.generate_performance_report(table_pred, table_gt,
                                                    sample_config, tmp_path)

    assert report_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'

    plt.close('all')
