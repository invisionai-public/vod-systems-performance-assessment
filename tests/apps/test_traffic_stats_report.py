import matplotlib.pyplot as plt
import pandas as pd
from pathlib import Path

from vod_analytics.apps import traffic_stats_report as report
from vod_analytics.configs import (load_yaml_to_dict,
                                   TrafficStatsReportConfigs)


def test_generate_traffic_statistics_report():
    # make sure it runs all the way through and saves a report
    pred_csv = Path(__file__).resolve().parents[1].joinpath(
        'test-data', 'test_predictions_anonymized.csv')
    table_pred = pd.read_csv(pred_csv, dtype='str')
    configs = load_yaml_to_dict(
        TrafficStatsReportConfigs,
        Path(__file__).resolve().parents[2].joinpath(
            'vod_analytics', 'apps', 'traffic_stats_configs.yml'))

    report_bytes, report_name = report.generate_traffic_statistics_report(
        table_pred, "test_report", configs)
    plt.close('all')

    assert report_bytes.getbuffer(
    ).nbytes > 0, "Expected to get more than 0 bytes in report"
    expected_report_name = 'Traffic_Statistics_07-07-2021_20-00_to_07-08-2021_19-58_test_report.pdf'
    assert report_name == expected_report_name, (
        "Expected report name "
        f"{expected_report_name}, got {report_name} instead.")
