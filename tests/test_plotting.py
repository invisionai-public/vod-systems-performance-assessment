from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pytest

import vod_analytics.plotting as vp


@pytest.fixture
def create_test_fig() -> Figure:
    plt.ioff()
    fig, ax = plt.subplots()
    x = np.arange(0, 26)
    y = x
    ax.scatter(x, y, label='y=x')
    plt.title('Graph of y=x')
    return fig


@pytest.fixture
def pets_data() -> pd.Series:
    plt.ioff()
    return pd.Series([
        'dog', 'dog', 'dog', 'cat', 'dog', 'cat', 'cat', 'bird', 'dog', 'bird',
        'dog', 'dinosaur'], name="Pets")


@pytest.fixture
def another_pets_data() -> pd.Series:
    plt.ioff()
    yield pd.Series([
        'cat', 'bird', 'dinosaur', 'dog', 'cat', 'dinosaur', 'bird',
        'dinosaur', 'cat', 'cat', 'cat', 'cat'], name="More_Pets")


@pytest.fixture
def dt_data() -> pd.Series:
    plt.ioff()
    data = pd.Series([
        '2023-03-03 00:00:12.123', '2023-03-03 01:03:34.062',
        '2023-03-03 04:17:46.941', '2023-03-03 07:32:55.643',
        '2023-03-03 08:56:12.234', '2023-03-03 11:11:11.111',
        '2023-03-03 13:25:39.529', '2023-03-03 15:09:07.425',
        '2023-03-03 16:40:12.345', '2023-03-03 19:28:28.853',
        '2023-03-03 22:22:22.222', '2023-03-03 22:34:56.789'
    ], name="Datetime")
    return pd.to_datetime(data, format='%Y-%m-%d %H:%M:%S.%f')


@pytest.fixture
def xy_data() -> pd.Series:
    plt.ioff()
    x = [np.arange(0, 5), np.arange(5, 10), np.arange(10, 15)]
    y = [
        2 * np.arange(0, 5), 3 * np.arange(5, 10) - 5, 250 / np.arange(10, 15)
    ]
    return x, y


def test_saveFigToDisk(tmp_path, create_test_fig) -> None:
    vp.saveFigToDisk(create_test_fig, "Test Scatter Plot", tmp_path)

    expected_img_path = tmp_path.joinpath('Test_Scatter_Plot.png')
    assert expected_img_path.is_file(
    ), f'Expected {str(expected_img_path)} was saved to disk, but cannot find it.'


def test_saveFigToMem(create_test_fig) -> None:
    fig_buf = vp.saveFigToMem(create_test_fig)
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'

    # make sure the decorator works as well
    @vp.saveFigToMemDec
    def another_test_fig():
        fig, ax = plt.subplots()
        x = np.arange(0, 26)
        y = 2 * x
        ax.scatter(x, y, label='y=2x')
        plt.title('Graph of y=2x')
        return fig, "test fig2"

    fig2_buf = another_test_fig()
    assert fig2_buf.getbuffer(
    ).nbytes > 0, 'Expected second image buffer size to be larger than 0 but it is not.'


def test_saveBufToDisk(tmp_path, create_test_fig) -> None:
    fig_buf = vp.saveFigToMem(create_test_fig)
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'

    vp.saveBufToDisk(fig_buf, "Test Figure", tmp_path)

    expected_img_path = tmp_path.joinpath('Test_Figure.png')
    assert expected_img_path.is_file(
    ), f'Expected {str(expected_img_path)} was saved to disk, but cannot find it.'


def test_plotPieChart(pets_data) -> None:
    fig_buf = vp.plotPieChart(pets_data, "Pets")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotTimeHistoryCounts(dt_data) -> None:
    fig_buf_no_rush_hour = vp.plotTimeHistoryCounts(
        dt_data,
        24,
        "Test Time Plot",
        "Counts",
    )
    assert fig_buf_no_rush_hour.getbuffer(
    ).nbytes > 0, 'Expected image buffer without rush hour size to be larger than 0 but it is not.'
    fig_buf_with_rush_hour = vp.plotTimeHistoryCounts(
        dt_data, 24, "Test Time Plot (Rush Hour)", "Counts", True)
    assert fig_buf_with_rush_hour.getbuffer(
    ).nbytes > 0, 'Expected image buffer with rush hour size to be larger than 0 but it is not.'


def test_plotTimeHistoryStackedBars(dt_data, pets_data) -> None:
    fig_buf = vp.plotTimeHistoryStackedBars(dt_data, pets_data, 24,
                                            "Pets Over Time", "# pet owners")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotSimpleBarChart(pets_data) -> None:
    fig_buf = vp.plotSimpleBarChart(pets_data, "Pets", "Pet Species")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotLayeredHistogram(pets_data, another_pets_data) -> None:
    fig_buf = vp.plotLayeredHistogram([pets_data, another_pets_data],
                                      ["Pets", "More Pets"], "Lots of Pets",
                                      "Pet Species", "# pet owners")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotPieceWiseWithLegend(xy_data) -> None:
    x_pieces, y_pieces = xy_data
    fig_buf = vp.plotPieceWiseWithLegend(x_pieces, y_pieces,
                                         ["2x", "3x-5", "250/x"], (0, 30),
                                         (0, 30), "Random Functions", "x vals",
                                         "y vals")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotConnectedScatterPlot(xy_data) -> None:
    x_pieces, y_pieces = xy_data
    fig_buf = vp.plotConnectedScatterPlot(x_pieces, y_pieces,
                                          "Random Functions", "x vals",
                                          "y vals")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotStackedBars(pets_data, another_pets_data) -> None:
    fig_buf = vp.plotStackedBars(pets_data.unique(), [
        pets_data.value_counts().sort_index(),
        another_pets_data.value_counts().sort_index()
    ], ["Some Pets", "Even More Pets"], "Many Pets", "Pet Species",
                                 "# pet owners")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotLayeredBarChart(pets_data, another_pets_data) -> None:
    fig_buf = vp.plotLayeredBarChart([pets_data, another_pets_data],
                                     ["Some Pets", "Even More Pets"],
                                     "Many Pets", "Pet Species",
                                     "# pet owners")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotSideBySideBarChart(pets_data, another_pets_data) -> None:
    fig_buf = vp.plotSideBySideBarChart([pets_data, another_pets_data],
                                        ["Some Pets", "Even More Pets"],
                                        "Many Pets", "Pet Species",
                                        "# pet owners")
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotOccupancyDetectionResults() -> None:
    occ_det_metrics = {
        'accuracy': 0.97,
        'overcounting': 0.01,
        'undercounting': 0.02,
        'CM': np.array([[53, 0, 0], [0, 23, 2], [2, 2, 8]])
    }

    fig_buf = vp.plotOccupancyDetectionResults(occ_det_metrics)

    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotErrorStatistics() -> None:
    pred_error_stats = {
        'correct':
        pd.Series([0.97, 0.65, 0.34, 0.99, 0.98, 0.12, 0.87, 0.76]),
        'incorrect':
        pd.Series([0.97, 0.65, 0.34, 0.99, 0.98, 0.12]),
        'low quality':
        pd.Series([0.09, 0.11, 0.23, 0.35, 0.81]),
        'undercounted':
        pd.Series([0.34, 0.99, 0.98]),
        'overcounted':
        pd.Series([0.97, 0.65, 0.12]),
        'total readable':
        pd.Series([
            0.97, 0.65, 0.34, 0.99, 0.98, 0.12, 0.87, 0.76, 0.97, 0.65, 0.34,
            0.99, 0.98, 0.12
        ]).sort_values().reset_index(drop=True)
    }
    plots = vp.plotErrorStatistics(pred_error_stats)
    assert plots.get('pred_conf_hist').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'
    assert plots.get('error_vs_conf').getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'


def test_plotTwoColumnTable() -> None:
    col1 = ["Value 1", "Value 2", "Value 3", "Value 4", "Value 5"]
    col2 = [1, 2, 3, 4, 5]

    fig_buf = vp.plotTwoColumnTable(col1, col2, "Test Table",
                                   ("Column 1", "Column 2"))
    assert fig_buf.getbuffer(
    ).nbytes > 0, 'Expected image buffer size to be larger than 0 but it is not.'
