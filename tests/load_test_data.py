from itertools import product
from pathlib import Path
from typing import Tuple

import pandas as pd


def load_test_table(csv_file: Path) -> pd.DataFrame:
    '''Read data from existing matched and unmatched groundtruth files
    Format so pandas.read_csv defaults do not change the original values
    Return these DataFrames
    '''
    # stops pandas.Dataframe.read_csv from replacing 'N/A' with nan (but let it replace the other defaults)
    # needed to make sure matching functions write unmatched tables as expected
    # by being able to compare N/A and n/a strings
    na_values = [
        '', '#N/A', '#N/A N/A', '#NA', '-1.#IND', '-1.#QNAN', '-NaN', '-nan',
        '1.#IND', '1.#QNAN', 'NA', 'NULL', 'NaN', 'nan', 'null', 'na'
    ]
    return pd.read_csv(csv_file,
                       dtype='str',
                       na_values=na_values,
                       keep_default_na=False)


def load_formatted_gt_and_pred_tables(
        path_to_gt_table: Path,
        path_to_pred_table: Path) -> Tuple[pd.DataFrame, pd.DataFrame]:
    '''Read data from inputed groundtruth and prediction csvs and format as done in the Jupyter Notebook
    Return these formatted DataFrames
    '''
    # import files
    table_gt = load_test_table(path_to_gt_table)
    table_pred_raw = load_test_table(path_to_pred_table)

    # format the tables
    table_gt.fillna('nan', inplace=True)
    table_pred_raw.fillna('nan', inplace=True)
    table_gt.frontcount_gt = table_gt.frontcount_gt.str.strip()
    table_gt.rearcount_gt = table_gt.rearcount_gt.str.strip()
    table_gt['Datetime_gt'] = pd.to_datetime(table_gt['Date_gt'] + ' ' +
                                             table_gt['Time_gt'],
                                             format='%d/%m/%Y %H:%M:%S.%f')
    table_pred_raw['Datetime'] = pd.to_datetime(table_pred_raw['Date'] + ' ' +
                                                table_pred_raw['Time'],
                                                format='%d/%m/%Y %H:%M:%S.%f')
    for column, symbol in product(['frontcount_gt', 'rearcount_gt'],
                                  ['N/A', 'n/a']):
        table_gt[column] = table_gt.loc[:, column].str.replace(symbol, 'nan')
    RENAME_PRED_COLS = {
        'Datetime': 'Datetime_pred',
        'LPR': 'LPR_pred',
        'Number of Passengers in first row (driver)': 'frontcount_pred',
        'Number of Passengers in second row': 'rearcount_pred',
        'Link to picture of 1st row': 'VOD_image_link',
        'Link to LPR file': 'LPR_image_link'
    }
    table_temp = table_pred_raw.loc[:, list(RENAME_PRED_COLS.keys())]
    table_pred = table_temp.rename(columns=RENAME_PRED_COLS)
    table_pred['VOD_image_link'] = table_pred['VOD_image_link'].str.replace(
        'row_image/1/', 'row_image/0/').str.replace(' ', '%20')
    table_pred['LPR_image_link'] = table_pred['LPR_image_link'].str.replace(
        ' ', '%20')
    return table_gt, table_pred


def load_unmatched_gt_table(path_to_table_unmatched_gt: str) -> pd.DataFrame:
    '''Read data from existing unmatched groundtruth files
    Format so table types match Jupyter Notebook
    Return DataFrame
    '''
    table_unmatched_gt = load_test_table(path_to_table_unmatched_gt)
    # convert this col from str to datetime to be able to compare empty cells as NaT (not a time)
    # necessary because 'Datetime_pred' col from pred file gets converted to datetime in Jupyter formatting
    table_unmatched_gt['Datetime_pred'] = pd.to_datetime(
        table_unmatched_gt['Datetime_pred'])
    return table_unmatched_gt


def load_table_matched_sample():
    path_to_table_matched_sample = Path(__file__).parent.joinpath(
        'test-data', 'table_matched_sample.csv')
    table_matched = load_test_table(path_to_table_matched_sample)
    table_matched[[
        'frontcount_gt', 'rearcount_gt'
    ]] = table_matched.loc[:, ['frontcount_gt', 'rearcount_gt']].astype(float)
    table_matched[[
        'frontcount_pred', 'rearcount_pred'
    ]] = table_matched.loc[:,
                           ['frontcount_pred', 'rearcount_pred']].astype(float)
    return table_matched


# For loading sample predictions and groundtruth from 2023 vod annotation workflow
def read_from_predicted_annotations_csv():
    REQUIRED_PRED_COLS = [
        'Vehicle', 'Timestamp', 'Vehicle class', 'Total number of Passengers',
        'Total Confidence', 'Link to picture of 1st row'
    ]
    # read in csv and make sure all required columns exist
    pred_csv = Path(__file__).parent.joinpath(
        'test-data', 'test_predicted_annotations.csv')
    table_pred_raw = pd.read_csv(Path(pred_csv).resolve(),
                                 dtype='str',
                                 usecols=REQUIRED_PRED_COLS)
    return table_pred_raw


def read_from_groundtruth_annotations_csv():
    REQUIRED_GT_COLS = [
        'Vehicle', 'Timestamp', 'total_count', 'countability', 'Link'
    ]
    gt_csv = Path(__file__).parent.joinpath('test-data',
                                            'test_groundtruth_annotations.csv')
    table_gt_raw = pd.read_csv(Path(gt_csv).resolve(),
                               dtype='str',
                               usecols=REQUIRED_GT_COLS)
    return table_gt_raw
