import numpy as np
import pandas as pd
import pytest

from tests.load_test_data import load_table_matched_sample
from vod_analytics.metrics import (reportWindowDetectionAccuracy,
                                   reportOccupancyMetrics, binarizeDecision,
                                   CountType, reportCountsTprAndFpr,
                                   groupProbabilitiesByErrorType)


@pytest.mark.parametrize("window_row", ["front", "rear"])
def test_reportWindowDetectionAccuracy(window_row: str) -> None:
    table_matched = load_table_matched_sample()

    result = reportWindowDetectionAccuracy(table_matched, window_row)
    expected = 0.95
    assert result == expected, f"Expected accuracy score of {expected} but got {result}."


def test_reportOccupancyMetrics() -> None:
    table_matched = load_table_matched_sample()
    table_matched = table_matched.dropna()

    # from manual calculation
    expected = {
        'accuracy': 84 / len(table_matched),
        'overcounting': 4 / len(table_matched),
        'undercounting': 2 / len(table_matched),
        'CM': [[53, 0, 0], [0, 23, 2], [2, 2, 8]]
    }
    result = reportOccupancyMetrics(
        table_matched['frontcount_gt'] + table_matched['rearcount_gt'],
        table_matched['frontcount_pred'] + table_matched['rearcount_pred'])

    assert expected['accuracy'] == result['accuracy'],\
           (f"Expected accuracy {expected['accuracy']} does not match"
            f"result {result['accuracy']}")
    assert expected['overcounting'] == result['overcounting'],\
           (f"Expected overcounting {expected['overcounting']} does not match"
            f"result {result['overcounting']}")
    assert expected['undercounting'] == result['undercounting'],\
           (f"Expected undercounting {expected['undercounting']} does not match"
            f"result {result['undercounting']}")
    assert np.array_equal(
        expected['CM'], result['CM']
    ), f"Expected confusion matrix {expected['CM']} does not match result {result['CM']}"


@pytest.mark.parametrize("decision_type",
                         ['minimize_fps', 'minimize_fns', 'count_only'])
def test_binarizeDecision(decision_type: str):
    counts = np.array([1, 2, 3, 4, 1])
    confidences = np.array([0.99, 0.78, 0.25, 0.89, 0.32])
    binarized = binarizeDecision(counts, confidences, 0.5, 3, decision_type)
    expected = {
        'minimize_fps': [1, 1, 0, 0, 0],
        'minimize_fns': [1, 1, 1, 0, 1],
        'count_only': [1, 1, 0, 0, 1]
    }
    assert np.array_equal(
        binarized, expected[decision_type]
    ), f"binarized list for {decision_type} should be {expected[decision_type]} but should be {binarized}"


def test_reportCountsTprAndFpr_front_rear_counts():
    table_matched = pd.DataFrame(
        [[1, "99%", 1, 0], [2, "78%", 1, 0], [3, "85%", 2, 1],
         [4, "89%", 2, 2], [1, "32%", 1, 2]],
        columns=[
            "total_count_pred", "total_count_confidence_pred", 'frontcount_gt',
            'rearcount_gt'
        ])

    expected = {
        'tpr_allcases': [[1.0], [1.0], [1.0]],
        'fpr_allcases': [[0.0], [float(1 / 3)], [float(1 / 3)]],
        'decision_types': ['minimize_fps', 'minimize_fns', 'count_only'],
        'tpr_count_only': 1.0,
        'fpr_count_only': float(1 / 3)
    }
    result = reportCountsTprAndFpr(CountType.FRONT_REAR_COUNTS, table_matched,
                                   3, [0.5])

    assert expected['tpr_allcases'] == result['tpr_allcases'],\
           f"Expected tpr of {expected['tpr_allcases']}. Got {result['tpr_allcases']} instead."
    assert expected['fpr_allcases'] == result['fpr_allcases'],\
           f"Expected fpr_allcases of {expected['fpr_allcases']}. Got {result['fpr_allcases']} instead."
    assert expected['decision_types'] == result['decision_types'],\
           (f"Expected decision_types of {expected['decision_types']}."
            f"Got {result['decision_types']} instead.")
    assert expected['tpr_count_only'] == result['tpr_count_only'],\
           (f"Expected tpr_count_only of {expected['tpr_count_only']}."
            f"Got {result['tpr_count_only']} instead.")
    assert expected['fpr_count_only'] == result['fpr_count_only'],\
           (f"Expected fpr_count_only of {expected['fpr_count_only']}."
            f"Got {result['fpr_count_only']} instead.")


def test_reportCountsTprAndFpr_total_count():
    table_matched = pd.DataFrame([[1, "99%", 1], [2, "78%", 1], [3, "85%", 3],
                                  [4, "89%", 4], [1, "32%", 3]],
                                 columns=[
                                     "total_count_pred",
                                     "total_count_confidence_pred",
                                     'total_count_gt'
                                 ])

    expected = {
        'tpr_allcases': [[1.0], [1.0], [1.0]],
        'fpr_allcases': [[0.0], [float(1 / 3)], [float(1 / 3)]],
        'decision_types': ['minimize_fps', 'minimize_fns', 'count_only'],
        'tpr_count_only': 1.0,
        'fpr_count_only': float(1 / 3)
    }
    result = reportCountsTprAndFpr(CountType.TOTAL_COUNT, table_matched, 3,
                                   [0.5])

    assert expected['tpr_allcases'] == result['tpr_allcases'],\
           f"Expected tpr_allcases of {expected['tpr_allcases']}. Got {result['tpr_allcases']} instead."
    assert expected['fpr_allcases'] == result['fpr_allcases'],\
           f"Expected fpr_allcases of {expected['fpr_allcases']}. Got {result['fpr_allcases']} instead."
    assert expected['decision_types'] == result['decision_types'],\
           (f"Expected decision_types of {expected['decision_types']}."
            f"Got {result['decision_types']} instead.")
    assert expected['tpr_count_only'] == result['tpr_count_only'],\
           (f"Expected tpr_count_only of {expected['tpr_count_only']}."
            f"Got {result['tpr_count_only']} instead.")
    assert expected['fpr_count_only'] == result['fpr_count_only'],\
           (f"Expected fpr_count_only of {expected['fpr_count_only']}."
            f"Got {result['fpr_count_only']} instead.")


def test_groupProbabilitiesByErrorType_FRONT_REAR_COUNTS():
    table_matched = pd.DataFrame(
        [[2, 1, 2, 1, "95.0%"], [1, 0, 1, 1, "45.34%"], [1, 0, 1, 0, "87.0%"],
         [2, 2, 2, 1, "79.12%"], [1, 2, 1, 2, "83.22%"],
         [1, 1, 2, 2, "99.99%"]],
        columns=[
            'frontcount_gt', 'rearcount_gt', 'frontcount_pred',
            'rearcount_pred', 'total_count_confidence_pred'
        ])
    table_matched['total_count_pred'] = table_matched[
        'frontcount_pred'] + table_matched['rearcount_pred']
    table_matched_unreadable = pd.DataFrame(
        ["99.45%", "75.1%", "34.5%", "87.0%", "93.9%"],
        columns=['total_count_confidence_pred'])
    expected = {
        'correct': [95.0, 87.0, 83.22],
        'incorrect': [45.34, 79.12, 99.99],
        'low quality': [34.5, 75.1, 87.0, 93.9, 99.45],
        'undercounted': [79.12],
        'overcounted': [45.34, 99.99],
        'total readable': [45.34, 79.12, 83.22, 87.0, 95.0, 99.99]
    }
    results = groupProbabilitiesByErrorType(CountType.FRONT_REAR_COUNTS,
                                            table_matched,
                                            table_matched_unreadable)

    assert np.array_equal(
        expected['correct'], results['correct']
    ), f"Expected correct list of {expected['correct']}. Got {results['correct']} instead."
    assert np.array_equal(
        expected['incorrect'], results['incorrect']
    ), f"Expected incorrect list of {expected['incorrect']}. Got {results['incorrect']} instead."
    assert np.array_equal(
        expected['low quality'], results['low quality']
    ), f"Expected low quality list of {expected['low quality']}. Got {results['low quality']} instead."
    assert np.array_equal(
        expected['undercounted'], results['undercounted']
    ), f"Expected undercounted list of {expected['undercounted']}. Got {results['undercounted']} instead."
    assert np.array_equal(
        expected['overcounted'], results['overcounted']
    ), f"Expected overcounted list of {expected['overcounted']}. Got {results['overcounted']} instead."
    assert np.array_equal(
        expected['total readable'], results['total readable']
    ), f"Expected total readable list of {expected['total readable']}. Got {results['total readable']} instead."


def test_groupProbabilitiesByErrorType_TOTAL_COUNT() -> None:
    table_matched = pd.DataFrame(
        [[3, 3, "95.0%"], [1, 2, "45.34%"], [1, 1, "87.0%"], [4, 3, "79.12%"],
         [3, 3, "83.22%"], [2, 4, "99.99%"]],
        columns=[
            'total_count_gt', 'total_count_pred', 'total_count_confidence_pred'
        ])
    table_matched_unreadable = pd.DataFrame(
        ["99.45%", "75.1%", "34.5%", "87.0%", "93.9%"],
        columns=['total_count_confidence_pred'])
    expected = {
        'correct': [95.0, 87.0, 83.22],
        'incorrect': [45.34, 79.12, 99.99],
        'low quality': [34.5, 75.1, 87.0, 93.9, 99.45],
        'undercounted': [79.12],
        'overcounted': [45.34, 99.99],
        'total readable': [45.34, 79.12, 83.22, 87.0, 95.0, 99.99]
    }

    results = groupProbabilitiesByErrorType(CountType.TOTAL_COUNT,
                                            table_matched,
                                            table_matched_unreadable)

    assert np.array_equal(
        expected['correct'], results['correct']
    ), f"Expected correct list of {expected['correct']}. Got {results['correct']} instead."
    assert np.array_equal(
        expected['incorrect'], results['incorrect']
    ), f"Expected incorrect list of {expected['incorrect']}. Got {results['incorrect']} instead."
    assert np.array_equal(
        expected['low quality'], results['low quality']
    ), f"Expected low quality list of {expected['low quality']}. Got {results['low quality']} instead."
    assert np.array_equal(
        expected['undercounted'], results['undercounted']
    ), f"Expected undercounted list of {expected['undercounted']}. Got {results['undercounted']} instead."
    assert np.array_equal(
        expected['overcounted'], results['overcounted']
    ), f"Expected overcounted list of {expected['overcounted']}. Got {results['overcounted']} instead."
    assert np.array_equal(
        expected['total readable'], results['total readable']
    ), f"Expected total readable list of {expected['total readable']}. Got {results['total readable']} instead."