from dataclasses import dataclass

import yaml

from vod_analytics.configs import load_yaml_to_dict


@dataclass(eq=False)
class DataclassTestClass:
    str_param: str
    int_param: int
    float_param: float
    bool_param: bool
    list_param: list
    dict_param: dict

    def __eq__(self, other):
        if not isinstance(other, DataclassTestClass):
            return NotImplemented
        return self.str_param == other.str_param and \
            self.int_param == other.int_param


@dataclass(eq=False)
class NestedDataclassTestClass:
    nested_param: DataclassTestClass
    another_param: str

    def __eq__(self, other):
        if not isinstance(other, NestedDataclassTestClass):
            return NotImplemented
        return self.nested_param == other.nested_param and \
            self.another_param == other.another_param


def test_load_yaml_to_dict_returns_test_dataclass_correctly(tmp_path):
    test_dataclass = DataclassTestClass("hello world", 42, 123.21, True,
                                        ['lorem', 'ipsum', 'dolor'], {
                                            "marco": "polo",
                                            "olly": "oxenfree"
                                        })
    test_nested_dataclass = NestedDataclassTestClass(test_dataclass,
                                                     "dlrow olleh")
    test_dataclass_str = {
        'nested_param': {
            'str_param': test_dataclass.str_param,
            'int_param': test_dataclass.int_param,
            'float_param': test_dataclass.float_param,
            'bool_param': test_dataclass.bool_param,
            'list_param': test_dataclass.list_param,
            'dict_param': test_dataclass.dict_param
        },
        'another_param': test_nested_dataclass.another_param
    }

    tmp_file = tmp_path / 'tmp_config.yml'
    with open(tmp_file, mode="w+") as temp_config:
        yaml.dump(test_dataclass_str, temp_config)
        temp_config.flush()
        test_result = load_yaml_to_dict(NestedDataclassTestClass, tmp_file)
        assert test_result.nested_param == test_dataclass
        assert test_result == test_nested_dataclass