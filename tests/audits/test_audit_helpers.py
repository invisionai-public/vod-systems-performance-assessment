import pandas as pd
import pytest

from conftest import TestExample


@pytest.mark.parametrize("operator, operand",
    [(">", "'1'"), ("in", "['1', '2'"), ("in", "['2', '3', '4']")],
    ids=["'>' operation not in allowed set: '>=', '<=', '==', 'in'",
         "operand is a list missing closing bracket (unparsable)",
         "operand has unallowed values"])
def test_labeling_rules_fail_parsing_if_any_condition_has_bad_operator_or_operand(operator, operand):
    rules_example = lambda operator_str, operand_str: [{"count": f"{operator_str}{operand_str}"}]
    with pytest.raises(Exception):
        rules = TestExample.build_hov_labeling_rules(hov_rules=rules_example(operator, operand))
        rules.parse(TestExample.labels)


def test_annotation_evaluation_fails_if_does_not_contain_all_labels():
    with pytest.raises(Exception):
        TestExample.parsed_hov_labeling_rules.evaluate({"count": "1"}) == "INVALID"  # missing visibility label


@pytest.mark.parametrize("annotation, expected_evaluation",
    [({"count": "skip", "visibility": "skip"}, "SKIP"),
     ({"count": "1", "visibility": "good"}, "LOV"),
     ({"count": "more", "visibility": "medium"}, "HOV"),
     ({"count": "1+", "visibility": "poor"}, "UNKNOWN"),
     ({"count": "1", "visibility": "poor"}, "INVALID"),
     ({"count": "1", "visibility": "good", "vehicle_type": "sedan"}, "LOV")],
    ids=["correct_SKIP",
         "correct_LOV",
         "correct_HOV",
         "correct_UNKNOWN",
         "correct_INVALID (solid count requires medium/good visibility)",
         "extra label in annotation is permitted"])
def test_annotation_evaluates_correctly_when_all_expected_labels_provided(annotation, expected_evaluation):
    labeling_rules = TestExample.parsed_hov_labeling_rules
    assert labeling_rules.evaluate(annotation) == expected_evaluation


def test_TrafficTable_intializes_with_correctly_parsed_timestamp_col(mock_table_gt):
    df_gt = mock_table_gt.dataframe
    assert df_gt[mock_table_gt.timestamp_col].to_list() == TestExample.timestamps_gt.to_list()


@pytest.mark.parametrize("table_fixture, rules, expected_determination", 
    [("mock_table_gt", TestExample.parsed_hov_labeling_rules, TestExample.hov_gt),
    ("mock_table_pred", TestExample.parsed_hov_prediction_rules, TestExample.hov_pred)],
    ids=["gt", "pred"])
def test_TrafficTable_adds_correct_hov_determination_based_on_respective_rules(
        table_fixture, rules, expected_determination, request
    ):

    table = request.getfixturevalue(table_fixture)
    table.add_hov_determination_inplace(rules.evaluate)
    assert table.dataframe[table.hov_col].to_list() == expected_determination


@pytest.mark.parametrize("table_fixture, table_ref_fixture, time_delta, expected_unmatched_num",
    [("mock_table_pred", "mock_table_gt", "6s", 0), ("mock_table_pred", "mock_table_gt", "2s", 1)],
    ids=["all_1s_or_5s_async_match_within_6s", "all_except_the_one_5s_async_match_within_2s"])
def test_TrafficTable_matches_against_other_table_correctly(
        table_fixture, table_ref_fixture, time_delta, expected_unmatched_num, request
    ):

    table = request.getfixturevalue(table_fixture)
    table_ref = request.getfixturevalue(table_ref_fixture)
    matched, unmatched = table.match_timestamps_against(table_ref, tolerance=pd.Timedelta(time_delta))
    assert len(matched) == len(table_ref.dataframe) - expected_unmatched_num and \
        len(unmatched) == expected_unmatched_num
