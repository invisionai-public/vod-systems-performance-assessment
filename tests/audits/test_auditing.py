from dataclasses import asdict
from pathlib import Path
import yaml

import numpy as np
import pytest

import vod_analytics
from vod_analytics.auditing import AuditConfigs, run_audit_pipeline
from conftest import TestExample


@pytest.fixture(scope="session", autouse=True)
def example_audit_configs() -> AuditConfigs:
    return AuditConfigs(
        groundtruth_format=TestExample.groundtruth_format,
        prediction_format=TestExample.predictions_format,
        gt_pred_matching=AuditConfigs.MatchConfigs(ts_timedelta_tol="2s", direction="nearest"),
        labels=TestExample.labels,
        HOV_labeling_rules=TestExample.parsed_hov_labeling_rules,
        HOV_prediction_rules=TestExample.parsed_hov_prediction_rules,
    )


@pytest.fixture(scope="session", autouse=True)
def mock_configs_yaml(example_audit_configs, tmp_path_factory) -> Path:
    file = tmp_path_factory.mktemp("audit_tests") / "configs.yaml"
    with open(file, "w") as stream:
        yaml.dump(asdict(example_audit_configs), stream)
    return file


def test_AuditConfigs_from_yaml_loads_expected_content(mock_configs_yaml, example_audit_configs):
    configs = AuditConfigs.from_yaml(mock_configs_yaml)
    assert configs == example_audit_configs


def test_audit_config_yaml_in_sample_data_folder_is_loadable():
    sample_yaml = Path(vod_analytics.__path__[0]).parent.joinpath("sample-data", "audit_configs.yaml")
    sample_configs = AuditConfigs.from_yaml(sample_yaml)


def test_AuditConfig_raises_if_labels_and_gt_essential_cols_dont_match():
    too_many_labels = {**TestExample.labels, "dummy_label_name": ["hi", "bye"]}
    with pytest.raises(Exception):
        AuditConfigs(
            groundtruth_format=TestExample.groundtruth_format,
            prediction_format=TestExample.predictions_format,
            labels=too_many_labels,
            HOV_labeling_rules=TestExample.parsed_hov_labeling_rules,
            HOV_prediction_rules=TestExample.parsed_hov_prediction_rules,
            )


def test_AuditConfig_raises_if_labeling_rules_have_nonlabel_variable():
    altered_labeling_rules = TestExample.build_hov_labeling_rules(
        skip_rules=[{"count": "== 'skip'", "nonlabel_variable": "== 'skip'"}])
    with pytest.raises(Exception):
        AuditConfigs(
            groundtruth_format=TestExample.groundtruth_format,
            prediction_format=TestExample.predictions_format,
            labels=TestExample.labels,
            HOV_labeling_rules=altered_labeling_rules,
            HOV_prediction_rules=TestExample.parsed_hov_prediction_rules,
            )


def test_run_audit_pipeline_delivers_correct_CM43_and_CM33_matrices(
        mock_gt_file, mock_pred_file, mock_configs_yaml):

    report = run_audit_pipeline(mock_gt_file, mock_pred_file, mock_configs_yaml)
    assert np.all(report["CM43"].to_numpy() == TestExample.expected_CM43_matrix
                  ) and np.all(report["CM33"].to_numpy() == TestExample.expected_CM33_matrix)
