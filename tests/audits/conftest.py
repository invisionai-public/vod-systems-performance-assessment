from pathlib import Path
from typing import Dict, List, Tuple, Optional

import numpy as np
import pandas as pd
import pytest

from vod_analytics.audit_helpers import HeadersAndFormats, HovLabelingRules, HovPredictionRules, TrafficTable


class TestExample:
    def build_hov_labeling_rules(
        skip_rules: Optional[List[Dict[str, str]]]=None,
        lov_rules: Optional[List[Dict[str, str]]]=None,
        hov_rules: Optional[List[Dict[str, str]]]=None,
        unknown_rules: Optional[List[Dict[str, str]]]=None
        ) -> HovLabelingRules:
    
        if skip_rules is None:
            skip_rules = [{"count": "== 'skip'", "visibility": "== 'skip'"}]
        if lov_rules is None:
            lov_rules = [{"count": "== '1'", "visibility": "in ['medium', 'good']"}]
        if hov_rules is None:
            hov_rules = [
                {"count": "=='2'", "visibility": "in ['medium', 'good']"},
                {"count": "=='2+'", "visibility": "== 'poor'"},
                {"count": "== 'more'"},
            ]
        if unknown_rules is None:
            unknown_rules = [{"count": "in ['0+', '1+']",
                            "visibility": "== 'poor'"}]

        return HovLabelingRules(skip_rules, lov_rules, hov_rules, unknown_rules)
    
    groundtruth_format = HeadersAndFormats(
        timestamp={"Date": "%Y-%m-%d", "Time": "%H:%M:%S.%f"},
        hov_rules={"count": "string", "visibility": "string"},
        other={"user": "string"}
    )

    predictions_format =  HeadersAndFormats(
        timestamp={"Datetime": "%Y-%m-%d_%H:%M:%S.%f"},
        hov_rules={"visibility": "percentage", "p_violation_HOV2": "percentage"},
        other={"count": "number"}
    )

    labels = {"count": ["0+", "1", "1+", "2", "2+", "more", "skip"],
        "visibility": ["poor", "medium", "good", "skip"]}
    
    parsed_hov_labeling_rules = build_hov_labeling_rules()
    parsed_hov_labeling_rules.parse(labels)

    parsed_hov_prediction_rules = HovPredictionRules(
        LOV_sure=[{'p_violation_HOV2': ">= 0.8", 'visibility': ">=0.9"}],
        LOV_maybe=[{'p_violation_HOV2': ">= 0.5", 'visibility': ">=0.7"}],
        HOV=[{'p_violation_HOV2': "<= 0.4999", 'visibility': ">=0.7"}],
    )
    parsed_hov_prediction_rules.parse({variable: None for variable in predictions_format.hov_rules})

    gt_file_content = [
        "Date,Time,count,visibility,user",
        "2023-07-20,01:00:00.000,skip,skip,Supervisor",   # SKIP
        "2023-07-20,01:01:00.000,1,good,Supervisor",      # LOV
        "2023-07-20,01:02:00.000,1,medium,Supervisor",    # LOV
        "2023-07-20,01:03:00.000,1,good,Supervisor",      # LOV
        "2023-07-20,01:04:00.000,1,medium,Supervisor",    # LOV
        "2023-07-20,01:05:00.000,1,poor,Supervisor",      # lov but INVALID!
        "2023-07-20,01:06:00.000,2,good,Supervisor",      # HOV
        "2023-07-20,01:07:00.000,2,good,Supervisor",      # HOV
        "2023-07-20,01:08:00.000,more,good,Supervisor",   # HOV
        "2023-07-20,01:09:00.000,2,good,Supervisor",      # HOV
        "2023-07-20,01:10:00.000,more,medium,Supervisor", # HOV
        "2023-07-20,01:11:00.000,1+,poor,Supervisor",     # UNK
        "2023-07-20,01:12:00.000,0+,poor,Supervisor",     # UNK
        "2023-07-20,01:13:00.000,1+,poor,Supervisor",     # UNK
        "2023-07-20,01:14:00.000,1+,poor,Supervisor",     # UNK
    ]
    timestamps_gt = pd.date_range(start="2023-07-20 01:00:00.000", end="2023-07-20 01:14:00.000", periods=15)
    hov_gt = ["SKIP"] + ["LOV"]*4 + ["INVALID"] + ["HOV"]*5 + ["UNKNOWN"]*4

    # timestamps have 1 sec sync error w.r.t. gt except one case that has 5 sec sync error
    pred_file_content = [
        "Datetime,visibility,p_violation_HOV2,count",
        "2023-07-20_01:00:01.000,50.0%,50.0%,2",         # SKIP_gt, UNK_pred
        "2023-07-20_01:00:59.000,95.0%,90.0%,1",         # LOV_gt, LOV_sure_pred
        "2023-07-20_01:02:00.100,75.0%,90.0%,1",         # LOV_gt, LOV_maybe_pred
        "2023-07-20_01:03:01.000,71.0%,40.0%,3",         # LOV_gt, HOV_pred
        "2023-07-20_01:04:01.000,30.0%,95.0%,1",         # LOV_gt, UNK_pred
        "2023-07-20_01:05:00.100,75.0%,30.0%,2",         # INVALID_gt, HOV_pred
        "2023-07-20_01:06:05.000,95.0%,20.0%,3",         # HOV_gt, HOV_pred but 5s delay
        "2023-07-20_01:07:01.000,94.0%,99.0%,1",         # HOV_gt, LOV_sure_pred
        "2023-07-20_01:07:59.000,99.0%,60.0%,1",         # HOV_gt, LOV_maybe_pred
        "2023-07-20_01:09:00.100,80.0%,20.0%,4",         # HOV_gt, HOV_pred
        "2023-07-20_01:10:01.000,30.0%,99.0%,1",         # HOV_gt, UNK_pred
        "2023-07-20_01:11:00.100,99.0%,98.0%,1",         # UNK_gt, LOV_sure_pred
        "2023-07-20_01:11:59.000,75.0%,98.0%,1",         # UNK_gt, LOV_maybe_pred
        "2023-07-20_01:13:01.000,99.0%,20.0%,4",         # UNK_gt, HOV_pred
        "2023-07-20_01:14:00.100,20.0%,50.0%,2",         # UNK_gt, UNK_pred
    ]
    hov_pred = ["UNKNOWN", "LOV_sure", "LOV_maybe", "HOV", "UNKNOWN"] + ["HOV"]*2 \
        + ["LOV_sure", "LOV_maybe", "HOV", "UNKNOWN"]*2
    
# To recap the above example, it includes 15 vehicle entries with the following truth and predictions:

# |vehicle_entry |HOV_gt  |HOV_pred   |
# | :---         | :---   | :---      |
# | 1            |SKIP    |UNK        |
# | 2            |LOV     |LOV_sure   |
# | 3            |LOV     |LOV_maybe  |
# | 4            |LOV     |HOV        |
# | 5            |LOV     |UNK        |
# | 6            |INVALID |HOV        |
# | unmatched    |HOV     |HOV        |
# | 7            |HOV     |LOV        |
# | 8            |HOV     |LOV        |
# | 9            |HOV     |HOV        |
# | 10           |HOV     |UNK        |
# | 11           |UNK     |LOV_sure   |
# | 12           |UNK     |LOV_maybe  |
# | 13           |UNK     |HOV_pred   |
# | 14           |UNK     |UNK_pred   |

# where its 14 entries that are expected to be matched with a tolerance < 2s can be reorganized into:

# |pred_classes    |true_SKIP |true_LOV |true_HOV |true_UNKNOWN |true_INVALID |
# | :---           | :---     | :---    | :---    | :---        | :---        |
# |pred_LOV_sure   |        0 |       1 |       1 |           1 |           0 |
# |pred_LOV_maybe  |        0 |       1 |       1 |           1 |           0 |
# |pred_HOV        |        0 |       1 |       1 |           1 |           1 |
# |pred_UNKNOWN    |        1 |       1 |       1 |           1 |           0 |

# So the CM43 (after omission of the SKIP and INVALID columns) is a 4x3 matrix of 1s, while CM33
# is a a 3x3 matrix with 2s in the first row and 1s elsewhere.

    expected_CM43_matrix = np.ones((4, 3))
    expected_CM33_matrix = np.array([[2, 2, 2], [1, 1, 1], [1, 1, 1]])


def data2csv(data: List[str], directory_path: Path, csv_name: str) -> Path:
    datafile = directory_path / csv_name
    datafile.write_text("\n".join(data))
    return datafile


@pytest.fixture(scope="session", autouse=True)
def mock_gt_file(tmp_path_factory) -> Path:
    return data2csv(TestExample.gt_file_content, tmp_path_factory.mktemp("audit_tests"), 'gt.csv')


@pytest.fixture(scope="session", autouse=True)
def mock_table_gt(mock_gt_file) -> TrafficTable:
    return TrafficTable(tag="gt", file=mock_gt_file, headers_and_formats=TestExample.groundtruth_format)


@pytest.fixture(scope="session", autouse=True)
def mock_pred_file(tmp_path_factory) -> Path:
    return data2csv(TestExample.pred_file_content, tmp_path_factory.mktemp("audit_tests"), 'pred.csv')


@pytest.fixture(scope="session", autouse=True)
def mock_table_pred(mock_pred_file) -> TrafficTable:
    return TrafficTable(tag="pred", file=mock_pred_file, headers_and_formats=TestExample.predictions_format)
