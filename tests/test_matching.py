from datetime import timedelta
import pandas as pd
from pathlib import Path

import pytest

from tests.load_test_data import (load_formatted_gt_and_pred_tables,
                                  load_test_table, load_unmatched_gt_table,
                                  read_from_predicted_annotations_csv,
                                  read_from_groundtruth_annotations_csv)
from vod_analytics.matching import Match_Pred_and_Gt_With


@pytest.mark.parametrize("ocr_max_dist", [1, 2, 3])
def test_Match_Pred_and_Gt_With_STR(ocr_max_dist: int) -> None:
    TIMEDIFF_TOL = 1

    path_to_gt_table = Path(__file__).parent.joinpath(
        'test-data', 'test_groundtruth_anonymized.csv')
    path_to_pred_table = Path(__file__).parent.joinpath(
        'test-data', 'test_predictions_anonymized.csv')
    table_gt, table_pred = load_formatted_gt_and_pred_tables(
        path_to_gt_table, path_to_pred_table)

    path_to_table_matched_gt = Path(__file__).parent.joinpath(
        'test-data', 'with_lpr', 'table_matched_gt.csv')
    table_matched_gt = load_test_table(path_to_table_matched_gt)
    expected_matched_rows = len(table_matched_gt.index)

    path_to_table_unmatched_gt = Path(__file__).parent.joinpath(
        'test-data', 'with_lpr', f'table_unmatched_gt_OCR{ocr_max_dist}.csv')
    table_unmatched_gt = load_unmatched_gt_table(path_to_table_unmatched_gt)
    table_matched, table_unmatched = Match_Pred_and_Gt_With.STR(
        table_pred, table_gt, TIMEDIFF_TOL, ocr_max_dist)

    # check if number of rows adds up correctly
    expected_unmatched_rows = len(table_unmatched_gt.index)
    len_table_matched = len(table_matched.index)
    len_table_unmatched = len(table_unmatched.index)
    assert len(table_gt.index) == expected_matched_rows + expected_unmatched_rows,\
           ('Inconsistent number of rows in test data: test_groundtruth_anonymized.csv,'
           f'table_matched_gt.csv and table_unmatched_gt_OCR{ocr_max_dist}.csv')
    assert len_table_matched == expected_matched_rows,\
           (f'table_matched OCR {ocr_max_dist} has {len_table_matched} rows.'
            f'Should have {expected_matched_rows}.')
    assert len_table_unmatched == expected_unmatched_rows,\
           (f'table_unmatched OCR {ocr_max_dist} has {len_table_unmatched} rows.'
            f'Should have {expected_unmatched_rows}.')
    # check if groundtruth tables match what the function returned tables
    assert table_matched_gt.astype(str).isin(table_matched.astype(str)).all(
        axis=None
    ), f'groundtruth for matched tables OCR {ocr_max_dist} did not match test result'
    assert table_unmatched_gt.astype(str).isin(
        table_unmatched.astype(str)
    ).all(
        axis=None
    ), f'groundtruth for unmatched tables OCR {ocr_max_dist} did not match test result'

    # make sure dtypes from table_pred and table_gt are preserved
    old_dtypes = {**dict(table_pred.dtypes), **dict(table_gt.dtypes)}
    assert old_dtypes == dict(
        table_matched.dtypes
    ), 'dtypes for matched table differ from the original'
    assert old_dtypes == dict(
        table_unmatched.dtypes
    ), 'dtypes for unmatched table differ from the original'


@pytest.mark.parametrize("timediff_tol", [1, 2, 3])
def test_Match_Pred_and_Gt_With_TIMESTAMP(timediff_tol: int) -> None:
    # load groundtruth and pred csvs and modify to have 4 unmatched
    path_to_gt_table = Path(__file__).parent.joinpath(
        'test-data', 'test_groundtruth_anonymized.csv')
    path_to_pred_table = Path(__file__).parent.joinpath(
        'test-data', 'test_predictions_anonymized.csv')
    table_gt, table_pred = load_formatted_gt_and_pred_tables(
        path_to_gt_table, path_to_pred_table)
    # modify timings for the same 4 rows that aren't matched in `table_matched_gt_with_lpr.csv`
    # timedelta seconds manually chosen so time_diff tolerance is not met
    table_gt.at[table_gt.loc[table_gt['LPR_gt'] == 'NZHDKYQW'].index[0],
                'Datetime_gt'] += timedelta(seconds=5)
    table_gt.at[table_gt.loc[table_gt['LPR_gt'] == 'NYHNCFMR'].index[0],
                'Datetime_gt'] += timedelta(seconds=2)
    table_gt.at[table_gt.loc[table_gt['LPR_gt'] == 'YAEADNRW'].index[0],
                'Datetime_gt'] += timedelta(seconds=3)
    table_gt.at[table_gt.loc[table_gt['LPR_gt'] == 'TFSGIBZR'].index[0],
                'Datetime_gt'] -= timedelta(seconds=8)
    table_gt.at[table_gt.loc[table_gt['LPR_gt'] == 'VVCXQSPT'].index[0],
                'Datetime_gt'] -= timedelta(seconds=2)
    # remove LPR-related cols
    table_gt.drop(['LPR_gt'], axis=1, inplace=True)
    table_pred.drop(['LPR_pred', 'LPR_image_link'], axis=1, inplace=True)

    # load matched and unmatched groundtruths
    path_to_table_matched_gt = Path(__file__).parent.joinpath(
        'test-data', 'without_lpr',
        f'table_matched_gt_timetol{timediff_tol}.csv')
    table_matched_gt = load_test_table(path_to_table_matched_gt)
    expected_matched_rows = len(table_matched_gt.index)

    path_to_table_unmatched_gt = Path(__file__).parent.joinpath(
        'test-data', 'without_lpr',
        f'table_unmatched_gt_timetol{timediff_tol}.csv')
    table_unmatched_gt = load_unmatched_gt_table(path_to_table_unmatched_gt)
    expected_unmatched_rows = len(table_unmatched_gt.index)

    table_matched, table_unmatched = Match_Pred_and_Gt_With.TIMESTAMP(
        table_pred, table_gt, timediff_tol)

    # check if number of rows adds up correctly
    len_table_matched = len(table_matched.index)
    len_table_unmatched = len(table_unmatched.index)
    assert len(table_gt.index) == expected_matched_rows + expected_unmatched_rows,\
           ('Inconsistent number of rows in test data: test_groundtruth_anonymized.csv,'
            f'table_matched_gt.csv and table_unmatched_gt_timetol{timediff_tol}.csv')
    assert len_table_matched == expected_matched_rows,\
           (f'table_matched time_tol {timediff_tol} has {len_table_matched} rows.'
            f'Should have {expected_matched_rows}.')
    assert len_table_unmatched == expected_unmatched_rows,\
           (f'table_unmatched time_tol {timediff_tol} has {len_table_unmatched} rows.'
            f'Should have {expected_unmatched_rows}.')

    # check if groundtruth tables match what the function returned tables
    # note that the 'Time_gt' col will still have the original times as the manual time modification
    # was only applied to 'Datetime_gt' which is used in `Match_Pred_and_Gt_With.TIMESTAMP`
    assert table_matched_gt.astype(str).isin(table_matched.astype(str)).all(axis=None),\
           f'groundtruth for matched tables time_tol {timediff_tol} did not match test result'
    assert table_unmatched_gt.astype(str).isin(table_unmatched.astype(str)).all(axis=None),\
           f'groundtruth for unmatched tables time_tol {timediff_tol} did not match test result'

    # make sure dtypes from table_pred and table_gt are preserved
    old_dtypes = {**dict(table_pred.dtypes), **dict(table_gt.dtypes)}
    assert old_dtypes == dict(
        table_matched.dtypes
    ), 'dtypes for matched table differ from the original'
    assert old_dtypes == dict(
        table_unmatched.dtypes
    ), 'dtypes for unmatched table differ from the original'


def test_Match_Pred_and_Gt_With_ID() -> None:
    # count threshold is hardcoded here because
    # it won't affect the matching process itself
    table_pred = read_from_predicted_annotations_csv()
    table_gt = read_from_groundtruth_annotations_csv()

    # format the relevant parts of the test data
    # 'Timestamp' col from string to Datetime type
    table_pred['Timestamp'] = pd.to_datetime(table_pred['Timestamp'],
                                             format='%d/%m/%Y %H:%M:%S.%f')

    table_gt['Timestamp'] = pd.to_datetime(
        table_gt['Timestamp'].map(lambda x: x.rsplit('-', 1)[0]),
        format='%Y-%m-%d %H:%M:%S.%f')

    # rename cols so there are no repeat col names
    RENAME_PRED_COLS = {
        'Vehicle': 'vehicle_id',
        'Timestamp': 'timestamp',
        'Total number of Passengers': 'total_count',
        'Total Confidence': 'total_count_confidence',
        'Vehicle class': 'vehicle_class',
        'Link to picture of 1st row': 'vod_image_link'
    }
    table_pred = table_pred.rename(columns=RENAME_PRED_COLS).sort_values(
        by='timestamp', ascending=True).add_suffix('_pred')

    RENAME_GT_COLS = {
        'Vehicle': 'vehicle_id',
        'Timestamp': 'timestamp',
        'total_count': 'total_count',
        'countability': 'countability',
        'Link': 'vod_image_link'
    }
    table_gt = table_gt.rename(columns=RENAME_GT_COLS).sort_values(
        by='timestamp', ascending=True).add_suffix('_gt')

    table_matched, table_unmatched = Match_Pred_and_Gt_With.ID(
        table_pred, table_gt)
    assert len(table_matched) == 352, ("Expected to find 352 matched rows. "
                                       f"Found {len(table_matched)} instead")
    assert len(table_unmatched) == 0, ("Expected to find 0 unmatched rows. "
                                       f"Found {len(table_unmatched)} instead")

    expected_cols = [
        'vehicle_id_gt', 'timestamp_gt', 'total_count_gt', 'countability_gt',
        'vod_image_link_gt', 'vehicle_id_pred', 'timestamp_pred',
        'vehicle_class_pred', 'total_count_pred',
        'total_count_confidence_pred', 'vod_image_link_pred'
    ]
    result_cols = list(table_matched.columns.values)
    assert result_cols == expected_cols, (f"Expected columns {expected_cols}. "
                                          f"Found {result_cols} instead.")

    # make sure dtypes from table_pred and table_gt are preserved
    old_dtypes = {**dict(table_pred.dtypes), **dict(table_gt.dtypes)}
    assert old_dtypes == dict(
        table_matched.dtypes
    ), 'dtypes for matched table differ from the original'
    assert old_dtypes == dict(
        table_unmatched.dtypes
    ), 'dtypes for unmatched table differ from the original'
