import pandas as pd

from tests.load_test_data import load_table_matched_sample
from vod_analytics.data_cleaning import removeRowsWithNanColValues


def test_removeRowsWithNanColValues_int():
    table_matched = load_table_matched_sample()
    # the last 10 rows have nan values in cols frontcount_pred and rearcount_pred (5 each)
    table_pred_nan_removed = removeRowsWithNanColValues(
        table_matched,
        ['frontcount_pred', 'rearcount_pred']).reset_index(drop=True)
    # manually removing the last 10 rows
    expected_table_pred_nan_removed = table_matched.iloc[:-10, :].reset_index(
        drop=True)
    # shouldn't remove anything because there isn't any nan in the specified columns
    table_matched_unchanged = removeRowsWithNanColValues(
        table_matched, ['frontcount_gt', 'rearcount_gt'])

    num_differing_nan_rows = len(pd.concat([expected_table_pred_nan_removed,
                                            table_pred_nan_removed]).
                                 drop_duplicates(keep=False, ignore_index=True))
    assert expected_table_pred_nan_removed.equals(
        table_pred_nan_removed
    ), f"The tables differ by {num_differing_nan_rows} rows."

    num_differing_matched_rows = len(pd.concat([table_matched,
                                                table_matched_unchanged]).
                                     drop_duplicates(keep=False, ignore_index=True))
    assert table_matched.equals(
        table_matched_unchanged
    ), f"The tables differ by {num_differing_matched_rows} rows."


def test_removeRowsWithNanColValues_str():
    table_matched = pd.DataFrame(
        [[2, 1, 2, 1, "95.0%"], [1, 0, 1, 1, "nan"], [1, 0, 1, 0, "87.0%"],
         [2, 2, 2, 1, "NAN"], [1, 2, 1, 2, "83.22%"], [1, 1, 2, 2, ""],
         [1, 2, 1, 1, "n/a"], [1, 1, 2, 2, "N/A"]],
        columns=[
            'frontcount_gt', 'rearcount_gt', 'frontcount_pred',
            'rearcount_pred', 'total_count_confidence_pred'
        ])

    # should remove 3 of the entries
    table_pred_nan_removed = removeRowsWithNanColValues(
        table_matched, ['total_count_confidence_pred']).reset_index(drop=True)
    expected_table_pred_nan_removed = pd.DataFrame(
        [[2, 1, 2, 1, "95.0%"], [1, 0, 1, 0, "87.0%"], [1, 2, 1, 2, "83.22%"]],
        columns=[
            'frontcount_gt', 'rearcount_gt', 'frontcount_pred',
            'rearcount_pred', 'total_count_confidence_pred'
        ])

    num_differing_rows = len(pd.concat([expected_table_pred_nan_removed,
                                        table_pred_nan_removed]).
                             drop_duplicates(keep=False, ignore_index=True))
    assert expected_table_pred_nan_removed.equals(
        table_pred_nan_removed
    ), f"The tables differ by {num_differing_rows} rows."
