# vod-assessment

Open-source repo containing tools to facilitate assessment and computation of performance metrics for Vehicle Occupancy
Detection (VOD) systems. 

A VOD system is a smart road sensor that detects vehicle passage events and counts the number of occupants (driver & passengers)
sitting in the vehicles. This is mainly used in carpooling lanes, a.k.a. managed lanes (HOV or HOT lanes). 
This is a relatively new technology and there are no established standards for assessing the performance of such systems
and comparing them. While at a superficial level, this seems like a simple binary classification (low-occupancy vs.
high-occupancy) problem, portraying a full perspective of performance is far more nuanced. The ground-truth fidelity is
itself dependent on the system's image acquisition performance, as well as the rigor in reviewing the images.
Some metrics are traffic-skew dependent and some are not. Due to these reasons, it is very difficult to compare the
performance of different VOD systems as reported by the respective vendor under different traffic circumstances and
arbitrary sets of data, review practices, assumptions and metrics.

This repository is an attempt to standardize the framework by promoting transparent assumption, metrics and review prac-
tices. There are certain requirements for the tools in this repo to be applicable in analyzing the performance of a VOD
sytem. Regardless of the way predictions, meta-data and images are saved in the backend, the system is expected to be 
capable of exporting results for a timespan of interest (e.g. a day or a week) as a CSV file. The predictions CSV
file will then get compared to another CSV file resulting from image review and audit. All the requirement and assumptions
are documented transparently in this open-source repository. All road operators and VOD vendors are welcome to use and
contribute to this codebase.

This is the repo's folder structure:

```
|
├── vod_analytics      # our vod_analytics library (python package)
│   └── apps           # all commandline apps
│   └── notebooks      # all jupyter and colab notebooks
│
├── tests              # tests for use with pytest
│   └── apps           # app tests
│   └── test-data      # data used in tests
│
├── sample-data        # anonymized sample data for demo-ing apps and notebooks 
│
├── private-data       # feel free to create this non-existing folder and store your gitignored data
│
├── scripts            # CI and utility scripts
│                   
├── README.md          # this file :)
├── requirements.txt   # strict requirements for replicating a guaranteed working environment
├── setup.py           # minimal requirements for installing vod_analytics package in other projects
├── Dockerfile         # Dockerfile for optional running in a Docker container
└── LICENSE            # MIT License for using this codebase
```

### Running on Colab (no programming experience needed):

#### Traffic_Stats_Report
This Colab notebook contains the code for calculating traffic statistics based on the VOD System data. It expects a prediction
CSV file as an input and displays the plots of corresponding statistics like traffic flux or observed vehicle classes. At the end,
a downloadable PDF is generated that can serve as an overview of the traffic statistics as gathered by the VOD system over the 
timespan of interest, e.g. a day or a week.

#### VOD_Performance_Report
This Colab notebook contains the theory and code for assessing the performance of a VOD System. It expects a prediction
CSV file and a groundtruth CSV file as inputs, and it explains and quantifies the performance step by step. At the end,
a downloadable PDF is generated that can serve as a summary performance report for the VOD system over the timespan of 
interest, e.g. a day or a week. The notebook contains a step-by-step user's guide.

#### Groundtruth_Annotation_Subsample_Generator
This Colab notebook is a simple tool to randomly subsample a prediction CSV file and create a template file for annotation.
For example, if you have a prediction file with 20,000 vehicles from a day of running a VOD system, you can employ the
following workflow for assessing the system performance (using the natural traffic testing paradigm):

1. Feed the prediction CSV file into `Groundtruth_Annotation_Subsample_Generator.ipynb` and download the resulting 
template file with `X` randomly selected vehicles. We recommend `X` to be 300 or larger.
2. Pass the downloaded template file to a team of reviewers and have them fill all the occupancy counts and license
plates by manually going through license plate and occupancy images for vehicles (carried over as links from the 
prediction file into the annotation template file).
3. Feed the prediction CSV file and the above human-filled annotation file into `VOD_Performance_Report.ipynb`, follow
the instructions and download the resulting PDF summary report or save the entire notebook itself in html format as a
detailed report.

#### Interactive Performance Audit for VOD Systems
This Colab notebook evaluates the performance of a VOD system in terms of its capability to produce HOV enforcement
recommendations. It expects a prediction CSV file and a groundtruth CSV file with particular column names and formatted
contents as inputs, and offers a simple interactive GUI to tune confidence and quality thresholds. Once the thresholds
are selected, a full report is generated outlining the performance in terms of classifying vehicles into enforcement
buckets. Invision's VOD dashboard produces predictions and groundtruth CSV files that are compatible with this
notebook requirements. 

### Running on your machine (some programming experience needed):

We assume you have git already installed on your machine (Windows, Linux or Mac). The next step is 
to set up a Python environment. To make the instructions easier and more OS-agnostic, we will use 
`conda`. Install miniconda for your OS from [here](https://docs.conda.io/en/latest/miniconda.html#installing).

Then open a terminal or command window, navigate to where you want to clone the repository and enter
these commands:

```
git clone git@gitlab.com:invisionai-public/vod-systems-performance-assessment.git
cd vod vod-systems-performance-assessment
conda create -n vodperf python=3.10.12
conda activate vodperf
pip install -r requirements.txt
pip install -e .  # sets up vod_analytics in editable mode and adds the repo folder to sys path
```

Note that mixing conda and pip installations sometimes causes troubles, but the above should work
fine in this case. Alternatively you can use a pure python `venv` and `pip` installation.

After completing the above steps, you can use all the modules in `vod_analytics` and import functions
or classes to develop your own custom analytics scripts, or run the ready-to-use apps.

If you have another environment for another project and you want to add vod_analytics in it, you only
need to run the last command from above, i.e. `pip install -e .`. This command will use `setup.py`to
set up vod_analytics with minimal requirements (works also with sligthly older python and package
dependencies).


#### Commandline apps:
See the apps in `vod_analytics/apps`. All the apps have descriptions and argument helps.

#### Jupyter notebooks:
We have only one jupyter notebook called `audit_jupyter_notebook.ipynb` which is similar to
`audit_colab_notebook.ipynb`.

### Contributing to this repo
You can clone the repository and create merge requests. Check the members of the project on gitlab
and talk to any of the maintaners to coordinate contributions.