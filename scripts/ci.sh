#!/bin/bash
set -e

# Check if valid flag is used. 
if [[ ! -z "$@" ]] && [[ "$@" != *"--build"* ]]; then
    echo "Unknown argument '$@' was entered."
    echo "Current supported args are: "
    echo "        '--build' if the docker image should be rebuilt before the tests are run. "
    exit 1
fi

# Figure out where ROOT is, based on the location of this script
# which is assumed to be in the scripts/ dir.
ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/.. >/dev/null 2>&1 && pwd )"
DOCKER_IMAGE="python-tests"

# Build the docker image if it doesn't exist or if the user has added the '--build' flag
if [[ "$(docker images -q ${DOCKER_IMAGE}:latest 2> /dev/null)" == "" ]] || [[ "$@" == *"--build"* ]]; then
    docker build -t "${DOCKER_IMAGE}" "${ROOT}"
fi

# will run any tests found in the current dir ie ${ROOT}
echo "Running tests found in ${ROOT}"
docker run --rm "${DOCKER_IMAGE}" "pytest"