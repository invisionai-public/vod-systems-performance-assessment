from functools import wraps
import heapq
import io
from pathlib import Path
import pytz
from typing import List, Tuple, Union, Callable, Optional
from PIL import Image

from matplotlib.axes import Axes
import matplotlib.dates as mdates
from matplotlib.figure import Figure
from matplotlib.lines import Line2D
from matplotlib.patches import Circle, RegularPolygon, Polygon
from matplotlib.path import Path as MatPath
import matplotlib.pyplot as plt
from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
import numpy as np
import pandas as pd
from scipy.signal import find_peaks, peak_prominences
import seaborn as sns


def saveFigToDisk(fig: Figure,
                  title: str,
                  dest_path: Path,
                  dpi: int = 300) -> None:
    assert dest_path.is_dir(
    ), f'Given destination path ({dest_path}) does not exist.'
    fig_file = dest_path.joinpath(title.replace(' ', '_') + '.png')
    if fig_file.exists():
        print(f'WARNING: {fig_file} already exists. It will be overwritten.')
    fig.savefig(fig_file, dpi=dpi, bbox_inches='tight', pad_inches=0)


def saveFigToMem(fig: Figure, dpi: int = 300) -> Callable:
    buf = io.BytesIO()
    fig.savefig(buf, format='png', dpi=dpi, bbox_inches='tight', pad_inches=0)
    buf.seek(0)
    return buf


def saveFigToMemDec(func: Callable) -> Callable:

    @wraps(func)
    def wrapper(*args) -> io.BytesIO:
        fig, _ = func(*args)
        return saveFigToMem(fig)

    return wrapper


def saveBufToDisk(img_buf: io.BytesIO, img_title: str,
                  dest_path: Path) -> Path:
    image = Image.open(img_buf).convert("RGBA")
    img_path = dest_path.joinpath(img_title.replace(' ', '_') + '.png')
    image.save(img_path)
    return img_path


@saveFigToMemDec
def plotPieChart(
    quantity_to_plot: pd.Series,
    title: str,
    figsize: Tuple[float, float] = (12 / 2.54, 10 / 2.54)
) -> Tuple[Figure, str]:
    variable = quantity_to_plot.value_counts(sort=False,
                                             ascending=True).sort_index()
    fig, ax = plt.subplots(figsize=figsize)
    plt.title(title)
    ax.pie(variable.values / variable.sum(),
           labels=variable.index,
           explode=[0.025] * len(variable),
           pctdistance=0.7,
           autopct='%1.1f%%',
           labeldistance=1.1)
    return fig, title


def addRushHourToPlot(ax: Axes, signal_datetime: pd.Series,
                      traffic_plot_hours: int) -> None:
    vehicles = pd.DataFrame({'Datetime': signal_datetime.values})
    vehicle_count = vehicles.groupby(
        pd.Grouper(
            key='Datetime',
            freq=f'{traffic_plot_hours}H')).size().reset_index(name='count')
    peaks, _ = find_peaks(vehicle_count['count'])
    if len(peaks) > 2:
        prominences = peak_prominences(vehicle_count['count'], peaks)[0]
        largest_peak_indices = heapq.nlargest(2,
                                              range(len(prominences)),
                                              key=prominences.__getitem__)
        peaks = [
            peaks[largest_peak_indices[0]], peaks[largest_peak_indices[1]]
        ]
    for peak in peaks:
        left_of_peak = peak - 1
        right_of_peak = peak + 2
        if left_of_peak < 0:
            left_of_peak = 0
            right_of_peak = peak + 3
        if right_of_peak > len(vehicle_count.index) - 1:
            right_of_peak = peak
            left_of_peak = peak - 3
        ax.axvspan(vehicle_count.loc[left_of_peak]['Datetime'],
                   vehicle_count.loc[right_of_peak]['Datetime'],
                   alpha=0.5,
                   color='orange')
        ax.text(vehicle_count.loc[left_of_peak]['Datetime'],
                vehicle_count.loc[peak]['count'],
                "Rush Hour",
                fontsize=8)


@saveFigToMemDec
def plotTimeHistoryCounts(
    signal_datetime: pd.Series,
    traffic_plot_hours: int,
    title: str,
    ylabel: str,
    include_rush_hours: bool = False,
    timezone: str = 'UTC',
    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54)
) -> Tuple[Figure, str]:
    # get timestamps and number of hours in sample
    num_hours = round((signal_datetime.max() - signal_datetime.min()) /
                      pd.Timedelta(hours=traffic_plot_hours))

    # plot data
    fig, ax = plt.subplots(figsize=figsize)
    _, bins, _ = ax.hist(signal_datetime, bins=num_hours, rwidth=0.9)

    if len(bins) > 25:
        bins = bins[::int(len(bins) / 25)]
    plt.xticks(bins, rotation=75)

    if num_hours == 24:
        parplot_dt_format = '%H:%M'
    elif traffic_plot_hours == 24:
        parplot_dt_format = '%h-%d'
    else:
        parplot_dt_format = '%h-%d, %H:%M'

    ax.xaxis.set_major_formatter(
        mdates.DateFormatter(parplot_dt_format, pytz.timezone(timezone)))
    plt.ylabel(ylabel)
    if include_rush_hours:
        addRushHourToPlot(ax, signal_datetime, traffic_plot_hours)
    plt.title(title)
    ax.set_frame_on(True)

    return fig, title


@saveFigToMemDec
def plotTimeHistoryStackedBars(
    signal_datetime: pd.Series,
    signal_value: pd.Series,
    traffic_plot_hours: int,
    title: str,
    ylabel: str,
    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54)
) -> Tuple[Figure, str]:
    # prepare the data
    data = pd.concat([signal_datetime, signal_value], axis=1)
    data[signal_datetime.name] = pd.to_datetime(data[signal_datetime.name])
    # get counts for each timespan determined by traffic_plot_hours
    # fill in missing quantity_to_plot values that don't show up in timespan with 0
    # sort by datetime, then quantity_to_plot values
    sorted_data = data.groupby([
        pd.Grouper(key=signal_datetime.name, freq=f'{traffic_plot_hours}H'),
        data[signal_value.name]
    ]).size().unstack(fill_value=0).stack().reset_index(
        name='count').sort_values(by=[signal_datetime.name, signal_value.name])

    # fill in missing datetimes between sparse data points if applicable
    sorted_data = sorted_data.set_index([signal_datetime.name, signal_value.name]).unstack(fill_value=0).\
        asfreq('D' if traffic_plot_hours == 24 else 'H', fill_value=0).stack().reset_index()
    # set up for plotting
    # make a list for each unique data category in quantity_to_plot of counts during each time span
    # ie data_list_per_index could have one list representing `CAR` with
    # list of counts [5, 67, 12] for 1pm, 2pm and 3pm repsectively
    data_list_per_index = []
    for index in sorted_data[signal_value.name].unique():
        data_per_index = []
        for time in sorted_data[signal_datetime.name].unique():
            val = sorted_data.loc[(sorted_data[signal_value.name] == index) &
                                  (sorted_data[signal_datetime.name] == time),
                                  'count'].item()
            data_per_index.append(val)
        data_list_per_index.append(data_per_index)
    assert len(data_list_per_index) == len(
        sorted_data[signal_value.name].unique())

    # plot data
    fig, ax = plt.subplots(figsize=figsize)
    # indices have to match the shape of the data_list_per_index
    indices = range(len(data_list_per_index[-1]))
    time_xlabels = pd.Series(sorted_data[signal_datetime.name].unique())

    data_list_per_index = sorted(data_list_per_index, key=sum, reverse=True)
    sum_prev_bars = np.array(data_list_per_index[0])
    for index in range(0, len(data_list_per_index)):
        if index > 0:
            ax.bar(indices, data_list_per_index[index], bottom=sum_prev_bars)
            sum_prev_bars += np.array(data_list_per_index[index])
        else:
            ax.bar(indices, data_list_per_index[index])

    plt.ylabel(ylabel)
    ax.axes.set_xticks(indices)
    # show `year-month-day hour:min` if date differs, otherwise only show `hour:min`
    ax.set_xticklabels([
        ts.strftime('%Y-%m-%d %H:%M')
        if ts.date() != time_xlabels[max(idx - 1, 0)].date() or idx == 0 else
        ts.strftime('%H:%M') for idx, ts in time_xlabels.items()
    ])

    ax.figure.autofmt_xdate(rotation=75)
    plt.title(title)
    ax.set_frame_on(True)
    ax.legend(sorted_data[signal_value.name].unique())

    return fig, title


@saveFigToMemDec
def plotSimpleBarChart(
    quantity_to_plot: pd.Series,
    title: str,
    ylabel: str,
    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54)
) -> Tuple[Figure, str]:
    # prepare the data
    counts = quantity_to_plot.value_counts().sort_index(ascending=True)

    # plot data
    fig, ax = plt.subplots(figsize=figsize)
    ax.bar(counts.index.tolist(), counts)

    plt.ylabel(ylabel)
    plt.title(title)
    ax.set_frame_on(True)

    return fig, title


def radar_factory(num_vars: int, frame: str = 'circle') -> np.ndarray:
    ''' This code creates a tool for radarcharts from:
    https://matplotlib.org/stable/gallery/specialty_plots/radar_chart.html
    '''
    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2 * np.pi, num_vars, endpoint=False)

    class RadarAxes(PolarAxes):

        name = 'radar'
        # use 1 line segment to connect specified points
        RESOLUTION = 1

        def __init__(self, *args, **kwargs) -> None:
            super().__init__(*args, **kwargs)
            # rotate plot such that the first axis is at the top
            self.set_theta_zero_location('N')

        def fill(self, *args, closed: bool = True, **kwargs) -> List[Polygon]:
            """Override fill so that line is closed by default"""
            return super().fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs) -> None:
            """Override plot so that line is closed by default"""
            lines = super().plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line: Line2D) -> None:
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.append(x, x[0])
                y = np.append(y, y[0])
                line.set_data(x, y)

        def set_varlabels(self, labels: List[str]) -> None:
            self.set_thetagrids(np.degrees(theta), labels)

        def _gen_axes_patch(self) -> RegularPolygon:
            # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
            # in axes coordinates.
            if frame == 'circle':
                return Circle((0.5, 0.5), 0.5)
            elif frame == 'polygon':
                return RegularPolygon((0.5, 0.5),
                                      num_vars,
                                      radius=.5,
                                      edgecolor="k")
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

        def _gen_axes_spines(self) -> dict:
            if frame == 'circle':
                return super()._gen_axes_spines()
            elif frame == 'polygon':
                # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
                spine = Spine(axes=self,
                              spine_type='circle',
                              path=MatPath.unit_regular_polygon(num_vars))
                # unit_regular_polygon gives a polygon of radius 1 centered at
                # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
                # 0.5) in axes coordinates.
                spine.set_transform(Affine2D().scale(.5).translate(.5, .5) +
                                    self.transAxes)
                return {'polar': spine}
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

    register_projection(RadarAxes)
    return theta


@saveFigToMemDec
def plotLayeredHistogram(
    quantities_to_plot: List[pd.Series],
    labels: List[str],
    title: str,
    xlabel: str,
    ylabel: str,
    bins: int = 100,
    plot_as_pdf: bool = False,
    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54)
) -> Tuple[Figure, str]:
    ''' plot layered histograms for list of numerical data '''
    assert len(quantities_to_plot) == len(
        labels
    ), f"Have {len(labels)} labels. Should have {len(quantities_to_plot)} to match number of plots."
    # plot histogram
    fig, ax = plt.subplots(figsize=figsize)
    for quantity, label in zip(quantities_to_plot, labels):
        _ = ax.hist(quantity,
                    bins=bins,
                    density=plot_as_pdf,
                    label=label,
                    alpha=0.5)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if len(quantities_to_plot) > 1:
        plt.legend(loc="upper left")
    plt.title(title)
    return fig, title


@saveFigToMemDec
def plotPieceWiseWithLegend(
    x_pieces: List[np.ndarray],
    y_pieces: List[np.ndarray],
    labels_pieces: List[str],
    xlim: Tuple[float, float],
    ylim: Tuple[float, float],
    title: str,
    xlabel: str,
    ylabel: str,
    reference_plot: Union[None, Tuple] = None,
    figsize: Tuple[float, float] = (24 / 2.54, 16 / 2.54)
) -> Tuple[Figure, str]:

    assert len(x_pieces) == len(
        y_pieces
    ), f"size of x_lists_to_plot ({len(x_pieces)}) should be equal to size of y_lists_to_plot ({len(y_pieces)})"
    assert len(y_pieces) == len(
        labels_pieces
    ), f"Have {len(labels_pieces)} labels. Should have {len(y_pieces)} to match number of pieces."

    fig, _ = plt.subplots(figsize=figsize)
    for x, y, label in zip(x_pieces, y_pieces, labels_pieces):
        plt.plot(x, y, marker='.', label=label)
    # plot reference data
    if reference_plot is not None:
        plt.plot(reference_plot[0],
                 reference_plot[1],
                 color="navy",
                 lw=2,
                 linestyle="--")
    plt.xlim(xlim[0], xlim[1])
    plt.ylim(ylim[0], ylim[1])
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend(loc="lower right")
    plt.title(title)
    return fig, title


@saveFigToMemDec
def plotConnectedScatterPlot(
    x_quantity_to_plot: pd.Series,
    y_quantity_to_plot: pd.Series,
    title: str,
    xlabel: str,
    ylabel: str,
    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54)
) -> Tuple[Figure, str]:

    fig, _ = plt.subplots(figsize=figsize)
    plt.plot(x_quantity_to_plot, y_quantity_to_plot, marker='.')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    return fig, title


@saveFigToMemDec
def plotStackedBars(x_bars: pd.Series,
                    y_bars_values: List[pd.Series],
                    stack_labels: List[str],
                    title: str,
                    xlabel: str,
                    ylabel: str,
                    figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54),
                    align: str = 'center') -> Tuple[Figure, str]:
    # make sure you have the same number of bar values as labels
    assert len(y_bars_values) == len(stack_labels)

    # plot data
    fig, ax = plt.subplots(figsize=figsize)

    sum_prev_bars = np.array(y_bars_values[0])
    for index in range(0, len(y_bars_values)):
        if index > 0:
            ax.bar(x_bars,
                   y_bars_values[index],
                   bottom=sum_prev_bars,
                   label=stack_labels[index],
                   align=align)
            sum_prev_bars += np.array(y_bars_values[index])
        else:
            ax.bar(x_bars,
                   y_bars_values[index],
                   label=stack_labels[index],
                   align=align)

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    ax.set_frame_on(True)
    ax.legend(loc="upper right")

    return fig, title


@saveFigToMemDec
def plotLayeredBarChart(quantities_to_plot: List[pd.Series],
                        labels: List[str],
                        title: str,
                        xlabel: str,
                        ylabel: str,
                        figsize: Tuple[float, float] = (18 / 2.54, 6 / 2.54),
                        align: str = 'center') -> Tuple[Figure, str]:
    ''' plot layered bar graph for list of numerical data '''
    assert len(quantities_to_plot) == len(
        labels
    ), f"Have {len(labels)} labels. Should have {len(quantities_to_plot)} to match number of plots."
    # plot bar graph
    fig, ax = plt.subplots(figsize=figsize)
    for quantity, label in zip(quantities_to_plot, labels):
        counts = quantity.value_counts().sort_index(ascending=True)
        _ = ax.bar(counts.index.tolist(),
                   counts,
                   label=label,
                   alpha=0.5,
                   align=align)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if len(quantities_to_plot) > 1:
        plt.legend(loc="upper left")
    plt.title(title)
    return fig, title


@saveFigToMemDec
def plotSideBySideBarChart(
    quantities_to_plot: List[pd.Series],
    labels: List[str],
    title: str,
    xlabel: str,
    ylabel: str,
    norm_divs: List[float] = [],
    bar_width: float = 0.3,
    figsize: Tuple[float, float] = (
        18 / 2.54,
        6 / 2.54,
    )
) -> Tuple[Figure, str]:
    ''' plot layered bar graph for list of numerical data '''
    assert len(quantities_to_plot) == len(
        labels
    ), f"Have {len(labels)} labels. Should have {len(quantities_to_plot)} to match number of plots."

    # if there are different unique values, keep track of them all
    unique_vals = list(quantities_to_plot[0].unique())
    for series in quantities_to_plot:
        if len(unique_vals) != len(series.unique()):
            unique_vals = unique_vals + list(
                set(unique_vals).symmetric_difference(series.unique()))

    assert len(norm_divs) <= 1 or len(norm_divs) == len(
        quantities_to_plot
    ), f"Number of divisors ({len(norm_divs)}) does not match number of plots ({len(quantities_to_plot)})."
    divisors = norm_divs
    if len(divisors) == 1:
        divisors = norm_divs[0]

    # plot bar graph
    fig, ax = plt.subplots(figsize=figsize)
    for index in range(0, len(quantities_to_plot)):
        counts = quantities_to_plot[index].value_counts().sort_index(
            ascending=True
        ) if not divisors else quantities_to_plot[index].value_counts().div(
            divisors).sort_index(ascending=True)
        # add a value of 0 for indices that may not exist in every series
        for missing_index in list(
                set(unique_vals).symmetric_difference(counts.index)):
            counts[missing_index] = int(0)

        indices = np.arange(len(counts.index))
        _ = ax.bar(indices + bar_width * index,
                   counts,
                   width=0.3,
                   label=labels[index],
                   align='edge')
    plt.xlabel(xlabel)
    ax.set_xticks(indices + bar_width * (len(quantities_to_plot) / 2))
    ax.set_xticklabels(counts.index.tolist())
    plt.ylabel(ylabel)
    if len(quantities_to_plot) > 1:
        plt.legend(loc="upper right")
    plt.title(title)
    return fig, title


@saveFigToMemDec
def plotOccupancyDetectionResults(
    occupancy_results: dict,
    figsize: Tuple[float, float] = (
        13 / 2.54,
        13 / 2.54,
    )
) -> Tuple[Figure, str]:
    """
    Create a confusion matrix figure for occupancy results calculated
    using the 'reportOccupancyMetrics' function in 'metrics.py' and
    display the accuracy, overcounting, and undercounting as a string
    """
    fig, ax = plt.subplots(figsize=figsize)
    a1, a2, a3 = [
        occupancy_results.get('accuracy'),
        occupancy_results.get('overcounting'),
        occupancy_results.get('undercounting')
    ]
    ax.set_title('Confusion Matrix',
                 weight='bold',
                 size='medium',
                 position=(0.5, 1.25),
                 horizontalalignment='center',
                 verticalalignment='center')
    ax.text(
        -0.1, -0.25,
        f'Occupancy Detection Accuracy = {a1*100:.1f}%\n({a2*100:.1f}% and {a3*100:.1f}% over- and under-counting)'
    )
    num_rows, _ = occupancy_results.get('CM').shape
    classes = np.arange(1, num_rows + 1).astype(str)
    classes[-1] += '+'  # add plus to last label
    sns.heatmap(occupancy_results.get('CM'),
                cmap="Blues",
                annot=True,
                fmt='.4g',
                xticklabels=classes,
                yticklabels=classes,
                cbar=False)
    plt.xlabel('True Class')
    plt.ylabel('Predicted Class')

    return fig, "Confusion Matrix"


def plotErrorStatistics(error_stats: dict,
                        correct_label: str = "Correct",
                        incorrect_label: str = "Incorrect",
                        low_quality_label: str = "Low Quality") -> dict:
    """
    Plot the results of the 'groupProbabilitiesByErrorType' functions
    Assumes an input dict with keys:
    {'correct', 'incorrect', 'low quality',
     'undercounted', 'overcounted', 'total readable'}
    Returns dict of the two plots as io.BytesIO
    """

    pred_conf_hist = plotLayeredHistogram(
        [
            error_stats.get('correct'),
            error_stats.get('incorrect'),
            error_stats.get('low quality')
        ], [correct_label, incorrect_label, low_quality_label],
        "Histogram of Prediction Confidences", "Confidence (%)",
        "# Occurrences", 50, False, (24 / 2.54, 16 / 2.54))

    # return the bins here so they are consistent across the grouping
    num_bins = 50
    total_cut, edges = pd.cut(error_stats.get('total readable'),
                              num_bins,
                              retbins=True)
    total = pd.Series(total_cut).value_counts().sort_index()
    undercounted = pd.Series(pd.cut(error_stats.get('undercounted'),
                                    edges)).value_counts().sort_index()
    overcounted = pd.Series(pd.cut(error_stats.get('overcounted'),
                                   edges)).value_counts().sort_index()
    labels = [index.left for index in total.index.values]

    error_vs_conf = plotStackedBars(labels, [
        undercounted.divide(total, fill_value=0),
        overcounted.divide(total, fill_value=0)
    ], ["Undercounted", "Overcounted"], "Error Probability vs Confidence",
                                    "Confidence (%)", "Probability",
                                    (24 / 2.54, 16 / 2.54), 'edge')

    return {'pred_conf_hist': pred_conf_hist, 'error_vs_conf': error_vs_conf}


@saveFigToMemDec
def plotTwoColumnTable(
    col1_values: list,
    col2_values: list,
    title: str,
    col_labels: Tuple[str, str],
    col_widths: Tuple[float, float] = (0.7, 0.3),
    figsize: Tuple[float, float] = (9 / 2.54, 9 / 2.54)
) -> Tuple[Figure, str]:
    """
    Create a table with N rows (where N is the size of the data)
    and 2 columns.
    """
    assert len(col1_values) == len(col2_values), (
        f"Amount of data in col1_values ({len(col1_values)}) "
        f"must be the same as in col2_values ({len(col2_values)})")

    fig, ax = plt.subplots(figsize=figsize)
    ax.set_axis_off()
    ax.set_title(title,
                 weight='bold',
                 size='medium',
                 position=(0.5, 1.0),
                 horizontalalignment='center',
                 verticalalignment='center')
    # convert list values to string when plotting
    ax.table(
        cellText=list(zip(col1_values, col2_values)),
        colLabels=list(col_labels),
        cellLoc='center',
        colWidths=list(col_widths),
        loc='upper left',
    )

    return fig, title


@saveFigToMemDec
def plotNColumnTableFromDataFrame(
    data: pd.DataFrame,
    title: str,
    col_labels: list,
    row_labels: list,
    col_widths: Optional[list] = None,
    figsize: Tuple[float, float] = (9 / 2.54, 9 / 2.54)
) -> Tuple[Figure, str]:
    """
    Create a table with N rows (where N is the size of the data)
    and N columns.
    """
    if col_widths is None:
        col_widths = np.full((len(col_labels),), float(1/len(col_labels))).tolist()

    data_as_list = []
    for _, row in data.iterrows():
        data_as_list.append(row.values.flatten().tolist())

    fig, ax = plt.subplots(figsize=figsize)
    ax.set_axis_off()
    ax.set_title(title,
                 weight='bold',
                 size='medium',
                 position=(0.5, 1.0),
                 horizontalalignment='center',
                 verticalalignment='center')
    # convert list values to string when plotting
    ax.table(
        cellText=data_as_list,
        colLabels=list(col_labels),
        rowLabels=list(row_labels),
        cellLoc='center',
        colWidths=list(col_widths),
        loc='upper left',
    )

    return fig, title


@saveFigToMemDec
def plotConfusionMatrix(confusion_matrix: np.ndarray,
                        title: str,
                        true_classes: list,
                        pred_classes: list,
                        additional_text: Optional[str] = None,
                        figsize: Tuple[float, float] = (
                            13 / 2.54,
                            13 / 2.54,
                        )
                        ) -> Tuple[Figure, str]:
    fig, ax = plt.subplots(figsize=figsize)
    ax.set_title(title,
                 weight='bold',
                 size='medium',
                 position=(0.5, 1.25),
                 horizontalalignment='center',
                 verticalalignment='center')
    ax.text(-0.1, -0.25, additional_text if additional_text is not None else "")
    sns.heatmap(confusion_matrix,
                cmap="Blues",
                annot=True,
                fmt='.4g',
                xticklabels=pred_classes,
                yticklabels=true_classes,
                cbar=False)
    plt.xlabel('True Class')
    plt.ylabel('Predicted Class')

    return fig, title
