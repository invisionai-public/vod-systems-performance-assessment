import io
from pathlib import Path
from typing import Optional

from fpdf import FPDF
from fpdf.enums import XPos, YPos
from PIL import Image


def imgInPDF(plot_title: str,
             pdf: FPDF,
             x: float = 0,
             y: float = 0,
             h: float = 50,
             full_img_path: Optional[Path] = None) -> None:
    img_name = plot_title.replace(' ', '_') + '.png'
    if full_img_path is not None:
        img_path = Path(full_img_path)
    else:
        img_path = Path.cwd().joinpath('Figures', img_name)
    img = Image.open(img_path)
    width, height = img.size
    pdf.image(str(img_path), x=x, y=y, w=h * width / height, h=h)


def imgFromMemoryInPDF(image: io.BytesIO,
                       pdf: FPDF,
                       x: float = 0,
                       y: float = 0,
                       h: float = 50) -> None:
    image.seek(0)
    img = Image.open(image)
    width, height = img.size
    pdf.image(img, x=x, y=y, w=h * width / height, h=h)


def addHeader(pdf: FPDF,
              report_tag: str,
              logo_img_path: Optional[Path] = None) -> None:
    pdf.set_xy(0, 0)
    pdf.set_font('helvetica', '', 10)
    pdf.set_text_color(49, 82, 158)  # to match Invision's Logo
    pdf.cell(60, 5, '', new_x=XPos.LMARGIN, new_y=YPos.NEXT)
    pdf.cell(100,
             10,
             'Summary Report: ' + report_tag,
             new_x=XPos.LMARGIN,
             new_y=YPos.NEXT)
    if logo_img_path:
        imgInPDF('Invision Logo',
                 pdf,
                 x=163,
                 y=5,
                 h=9,
                 full_img_path=logo_img_path)
    pdf.cell(60, 5, '', new_x=XPos.LMARGIN, new_y=YPos.NEXT)
    # leave font ready for a title
    pdf.set_font('helvetica', 'B', 12)
    pdf.set_text_color(0, 0, 0)


def addPage_ifNecessary(pdf: FPDF, printed_entries: int, report_timespan: str,
                        vod_system_tag: str) -> int:
    if printed_entries > 35:
        pdf.add_page()
        addHeader(pdf, f'{report_timespan}, {vod_system_tag}')
        printed_entries = 0
    return printed_entries
