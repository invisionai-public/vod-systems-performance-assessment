#!/usr/bin/env python3
HELP = """VOD_Performance_Report.py
Expecting the prediction file to contain the following rows: [Date, Time, Vehicle class, Total number of Passengers, Total Confidence,
                                                              Link to picture of 1st row,	Link to picture of 2nd row]
Expecting the groundtruth file to contain the following rows: [Timestamp, total_count, countability, License Plate, Link]

"""
import sys
import argparse
import io
from fpdf import FPDF, FlexTemplate
import logging
from pathlib import Path
from PIL import Image
from typing import Tuple, Optional

import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters

from vod_analytics.configs import (load_yaml_to_dict, HovDecision,
                                   SanityCheckBehaviour, PredictionParams2023,
                                   GroundtruthParams2023,
                                   VODPerformanceReportConfigs2023)
from vod_analytics.metrics import (reportCountsTprAndFpr,
                                   reportOccupancyMetrics, CountType,
                                   groupProbabilitiesByErrorType)
from vod_analytics.plotting import (plotPieChart, plotLayeredHistogram,
                                    plotPieceWiseWithLegend,
                                    plotErrorStatistics,
                                    plotSideBySideBarChart,
                                    plotOccupancyDetectionResults,
                                    plotTwoColumnTable)
from vod_analytics.matching import Match_Pred_and_Gt_With

register_matplotlib_converters()


def format_prediction_table(
        table_pred_raw: pd.DataFrame, count_threshold: int,
        pred_configs: PredictionParams2023) -> pd.DataFrame:
    """
    Given a DataFrame containing columns: ['Vehicle', 'Timestamp', 'Total number of Passengers', 'Vehicle class', 'Link to picture of 1st row']
    Returns a prediction DataFrame with the following formatting:
    - columns converted from str to their respective datatypes
    - column headers renamed to be more uniform with script
    - HOV status column added based on given 'count_threshold' (when vehicle is considered an HOV)
    """
    pred_cols = pred_configs.prediction_col_names
    # convert time strings to datetime objects (remove time offset ie -0700)
    table_pred_raw[pred_cols.timestamp] = pd.to_datetime(
        table_pred_raw[pred_cols.timestamp].map(lambda x: x.rsplit('-', 1)[0]),
        format=pred_configs.datetime_format)

    # if '%' is included in the values for the confidence column, remove it so it can be converted to numeric
    table_pred_raw[pred_cols.total_confidence] = table_pred_raw[
        pred_cols.total_confidence].str.replace('%', '').astype(float)

    # change all confidences to be between [0, 100] if necessary
    if table_pred_raw[pred_cols.total_confidence].max() <= 1:
        table_pred_raw[pred_cols.total_confidence] = table_pred_raw[
            pred_cols.total_confidence].multiple(100)

    # keep and rename only required pred and gt cols to standardize naming for rest of analysis
    RENAME_PRED_COLS = {
        pred_cols.vehicle_id: 'vehicle_id',
        pred_cols.timestamp: 'timestamp',
        pred_cols.total_count: 'total_count',
        pred_cols.total_confidence: 'total_count_confidence',
        pred_cols.vehicle_class: 'vehicle_class',
        pred_cols.vehicle_image_link: 'vod_image_link'
    }
    # rename prediction columns and sort by timestamp
    table_pred = table_pred_raw.rename(columns=RENAME_PRED_COLS).sort_values(
        by='timestamp', ascending=True)

    # fix space in links and change to unblurred full VOD image link for better later use
    table_pred['vod_image_link'] = table_pred['vod_image_link'].str.replace(
        'row_image/1/', 'row_image/0/').str.replace(' ', '%20')

    # add column converting str total_count values to int
    table_pred['total_count_int'] = table_pred['total_count'].astype(int)

    # add column for what is and isn't HOV compliant -> keeping as string to be consistent with groundtruth
    table_pred['hov_status'] = table_pred['total_count_int'].apply(
        lambda count: f'{HovDecision.HOV.value}{count_threshold}+'
        if count >= count_threshold else HovDecision.LOV.value)

    return table_pred.add_suffix('_pred')


def format_groundtruth_table(
        table_gt_raw: pd.DataFrame, count_threshold: int,
        gt_configs: GroundtruthParams2023) -> pd.DataFrame:
    """
    Given a DataFrame containing columns: ['Vehicle', 'Timestamp', 'total_count', 'countability', 'Link']
    Make sure the CountLabel with 'as_int' value >= count_threshold is set to HOV
    Run sanity check to make sure "countability" column values is in the 'is_countable' set
        (ie only countablity == true cols should correspond with CountLabels of is_countable == [true] etc.)
    Returns a groundtruth DataFrame with the following formatting:
    - columns converted from str to their datatypes
    - column headers renamed to be more uniform with script
    - HOV status column added based on given hov_decision
    - bin_countability column added based on countable vehicles having 'countability' values of ['min_viable', 'good', 'excellent']
    """
    # makes sure that prediction formatting and groundtruth formatting both use the same count_threhold
    met_threshold_keys = [
        k for k, v in gt_configs.allowed_counts.items()
        if v.as_int >= count_threshold
    ]
    for key in met_threshold_keys:
        assert gt_configs.allowed_counts[
            key].hov_decision == HovDecision.HOV, (
                f"CountLabel {key} with an integer value "
                f"{gt_configs.allowed_counts[key].as_int} > {count_threshold}."
                "It should be HOV to match the prediction hov decisions.")

    gt_cols = gt_configs.groundtruth_col_names
    # convert time strings to datetime objects (remove time offset ie -0700)
    table_gt_raw[gt_cols.timestamp] = pd.to_datetime(
        table_gt_raw[gt_cols.timestamp].map(lambda x: x.rsplit('-', 1)[0]),
        format=gt_configs.datetime_format)

    RENAME_GT_COLS = {
        gt_cols.vehicle_id: 'vehicle_id',
        gt_cols.timestamp: 'timestamp',
        gt_cols.total_count: 'total_count',
        gt_cols.countability: 'countability',
        gt_cols.vehicle_image_link: 'vod_image_link'
    }
    # rename groundtruth columns and sort by timestamp
    table_gt = table_gt_raw.rename(columns=RENAME_GT_COLS).sort_values(
        by='timestamp', ascending=True)

    # remove the skipped vehicles from table_gt
    skip_list_pairs = [
        f"({key} != '{val}')"
        for key, val in gt_configs.skip_column_name_and_value.items()
    ]
    query = ' | '.join(skip_list_pairs)

    table_gt = table_gt.query(query).reset_index(drop=True)
    logging.info(
        f'Removed {len(table_gt_raw) - len(table_gt)} skipped groundtruth cases.'
    )

    # SANITY CHECK: Make sure countability values correspond with CountLabel is_countable expectations
    countability_bool = pd.Series([
        gt_configs.allowed_countability[row]
        for row in table_gt['countability']
    ])
    len_countability_bool = len(countability_bool)
    assert len_countability_bool == len(table_gt['countability']), (
        f"Length of countability_dict_vals ({len_countability_bool}) "
        f"should equal length of table_gt['countability'] ({len(table_gt['countability'])})"
    )

    total_count_bool = pd.Series([
        gt_configs.allowed_counts[row].is_countable
        for row in table_gt['total_count']
    ])
    len_total_count_bool = len(total_count_bool)
    assert len_total_count_bool == len(table_gt['total_count']), (
        f"Length of countability_bool ({len_total_count_bool}) "
        f"should equal length of table_gt['total_count'] ({len(table_gt['total_count'])})"
    )

    assert len_countability_bool == len_total_count_bool, (
        f"Series length mismatch between countability ({len_countability_bool}) "
        f"and total_count ({len_total_count_bool})")

    compare_rows = pd.Series(
        [c in t for c, t in zip(countability_bool, total_count_bool)])
    # get rows where compare_rows is false
    inconsistent_rows = compare_rows[~compare_rows]
    if len(inconsistent_rows) > 0:
        logging.warning(
            f"Found {len(inconsistent_rows)} rows that are inconsistent "
            "between the count and countability labels.")
        if gt_configs.sanity_check_behaviour == SanityCheckBehaviour.REMOVE:
            logging.warning(
                f"Removing the {len(inconsistent_rows)} inconsistent rows.")
            table_gt.drop(list(inconsistent_rows.index.values), inplace=True)
        elif gt_configs.sanity_check_behaviour == SanityCheckBehaviour.THROW:
            sys.exit("Exiting vod_performance_report_2023.py script.")
        else:
            sys.exit(
                f"SanityCheckBehaviour {gt_configs.sanity_check_behaviour} not supported."
            )

    # add column for what is and isn't HOV compliant
    table_gt['hov_status'] = table_gt['total_count'].apply(
        lambda count: gt_configs.allowed_counts[count].hov_decision.value)
    table_gt['hov_status'] = table_gt['hov_status'].replace(
        HovDecision.HOV.value, f'{HovDecision.HOV.value}{count_threshold}+')

    # uses CountLabel member 'as_int' to replace groundtruth str values
    table_gt['total_count_int'] = table_gt['total_count'].apply(
        lambda count: int(gt_configs.allowed_counts[count].as_int))

    # create column for grouping readable vehicles
    replace_dict = {
        key: "countable" if value else "uncountable"
        for key, value in gt_configs.allowed_countability.items()
    }
    table_gt['bin_countability'] = table_gt['countability'].copy()
    table_gt['bin_countability'] = table_gt['bin_countability'].replace(
        replace_dict)

    return table_gt.add_suffix('_gt')


def separate_readable_and_unreadable_tables(
        table_to_be_separated: pd.DataFrame
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Separate a matched table into readable and unreadable tables
    """
    table_readable = table_to_be_separated[
        table_to_be_separated['bin_countability_gt'].isin(
            ['countable'])].reset_index(drop=True)
    table_unreadable = table_to_be_separated[
        ~table_to_be_separated['bin_countability_gt'].
        isin(['countable'])].reset_index(drop=True)

    return table_readable, table_unreadable


def calculate_image_quality_score(table_matched: pd.DataFrame) -> float:
    """
    Calculate and return the image quality score
    """

    readable_count = table_matched['bin_countability_gt'].value_counts(
    )['countable']
    img_quality_score = readable_count / len(table_matched)
    logging.info(f'Out of {len(table_matched)} matched vehicles, '
                 f'{readable_count} are perfectly '
                 'readable based on the ground truth.')
    return img_quality_score


def find_and_print_misclassifications(
        table_matched_readable: pd.DataFrame) -> dict:
    """
    Classify the overcounted and undercounted predictions in the readable table.
    Print out the corresponding information for undercounted and overcounted errors.
    """
    pd.set_option('display.max_colwidth', 5000)
    totcount_clipped_pred = np.clip(
        table_matched_readable['total_count_int_pred'], 0,
        3)  # clip to make 3 represent 3+
    totcount_clipped_gt = np.clip(table_matched_readable['total_count_int_gt'],
                                  0, 3)

    COLS_TO_PRINT = [
        'vod_image_link_pred', 'vod_image_link_gt', 'total_count_pred',
        'total_count_gt'
    ]

    overcounted_condition = totcount_clipped_pred > totcount_clipped_gt
    undercounted_condition = totcount_clipped_pred < totcount_clipped_gt

    misclassifications = {
        "overcounted":
        table_matched_readable.loc[overcounted_condition, COLS_TO_PRINT],
        "undercounted":
        table_matched_readable.loc[undercounted_condition, COLS_TO_PRINT]
    }

    pd.set_option('display.max_colwidth', 5000)
    logging.info(
        'This is a list of all allegedly overcounted occupancy misclassifications:'
    )
    logging.info(misclassifications.get('overcounted').to_string(index=False))

    logging.info(
        'This is a list of all allegedly undercounted occupancy misclassifications:'
    )
    logging.info(misclassifications.get('undercounted').to_string(index=False))

    return misclassifications


def consolidate_all_performance_metrics(img_quality_score: float,
                                        occ_det_metrics: dict,
                                        hov_status_metrics: dict,
                                        count_threshold: int) -> io.BytesIO:
    """
    Create a Performance Metrics table
    """
    metrics = [
        'Image Quality', 'Occupancy Detection Accuracy', 'Over-counting',
        'Under-counting'
    ]

    metrics.append(f'HOV Status({str(count_threshold)}+) TPR')
    metrics.append(f'HOV Status({str(count_threshold)}+) FPR')

    metrics_vals = [
        img_quality_score,
        occ_det_metrics.get('accuracy'),
        occ_det_metrics.get('overcounting'),
        occ_det_metrics.get('undercounting'),
        hov_status_metrics.get("tpr_count_only"),
        hov_status_metrics.get("fpr_count_only")
    ]
    vals = [f"{metric * 100 : .1f}%" for metric in metrics_vals]

    return plotTwoColumnTable(metrics, vals, 'Performance Metrics Summary',
                              ('Metrics', 'Metric Values'))


def plot_pred_gt_comparisons(table_matched_readable: pd.DataFrame,
                             table_matched_unreadable: pd.DataFrame,
                             table_matched_hov: pd.Series,
                             count_threshold: int) -> io.BytesIO:
    """
    Plot figures with comparisons between the matched gt and pred tables
    Returns a dict of the 5 plots: {'LQ Predicted Counts', 'GT vs Pred Total Counts',
                                    'GT vs Pred HOV Status',
                                    'LQ LOcc Pred Total Confidence','HQ LOcc Pred Total Confidence'}
    """

    lq_pred_counts = plotPieChart(table_matched_unreadable['total_count_pred'],
                                  "Low Quality Image Predicted Counts")

    low_occupancy = np.arange(0, count_threshold).astype(str)
    consolidated_totals = np.arange(count_threshold, 4).astype(str)
    consolidated_totals = np.append(consolidated_totals,
                                    np.char.add(consolidated_totals, "+"))
    consolidated_totals = np.append(consolidated_totals, 'more')

    total_count_threshold_str = str(count_threshold) + '+'
    table_matched_readable_str = table_matched_readable.copy()
    table_matched_readable_str['total_count_pred'].where(
        table_matched_readable['total_count_pred'].astype(int) <
        count_threshold,
        total_count_threshold_str,
        inplace=True)
    table_matched_readable_str['total_count_gt'] = table_matched_readable_str[
        'total_count_gt'].replace(consolidated_totals,
                                  total_count_threshold_str)

    # Plot groundtruth vs prediction hov status counts
    gt_vs_pred_hov = plotSideBySideBarChart(
        [
            table_matched_readable_str['hov_status_gt'],
            table_matched_readable_str['hov_status_pred'], table_matched_hov
        ], [
            "Readable Groundtruth", "Predictions with Readable GT",
            "All Predictions"
        ], f"Groundtruth and Predicted HOV{str(count_threshold)}+ Status",
        f"HOV{str(count_threshold)}+ Status", "Percent",
        [len(table_matched_hov)])

    # Plot histogram of confidences for low quality and predicted low occupancy vehicles
    unreadable_LO_pred_confs = table_matched_unreadable.loc[
        table_matched_unreadable['total_count_pred'].isin(low_occupancy),
        'total_count_confidence_pred']
    lq_lo_conf = plotLayeredHistogram(
        [unreadable_LO_pred_confs], ["Low Quality Low Occupancy"],
        "Histogram of Low Quality Low Occupancy Predicted Total Confidences",
        "Confidence (%)", "# Occurrences", 50, False)

    # Plot histogram of confidences for high quality and predicted low occupancy vehicles
    readable_LO_pred_confs = table_matched_readable_str.loc[
        table_matched_readable_str['total_count_pred'].isin(low_occupancy),
        'total_count_confidence_pred']
    hq_lo_conf = plotLayeredHistogram(
        [readable_LO_pred_confs], ["High Quality Low Occupancy"],
        "Histogram of High Quality Low Occupancy Predicted Total Confidences",
        "Confidence (%)", "# Occurrences", 50, False)

    return {
        'LQ Predicted Counts': lq_pred_counts,
        'GT vs Pred HOV Status': gt_vs_pred_hov,
        'LQ LOcc Pred Total Confidence': lq_lo_conf,
        'HQ LOcc Pred Total Confidence': hq_lo_conf
    }


def generate_performance_report(
        table_pred: pd.DataFrame,
        table_gt: pd.DataFrame,
        configs: VODPerformanceReportConfigs2023,
        results_dir: Optional[Path] = None,
        logo_path: Optional[Path] = None) -> io.BytesIO:
    """
    Generates a performance report that creates various plots
    """

    # match pred and gt tables by timestamp
    table_matched, table_unmatched = Match_Pred_and_Gt_With.ID(
        table_pred, table_gt)
    if results_dir:
        table_matched.to_csv(results_dir.joinpath("matched_vehicles.csv"))
        table_unmatched.to_csv(results_dir.joinpath("unmatched_vehicles.csv"))

    table_matched_readable, table_matched_unreadable = separate_readable_and_unreadable_tables(
        table_matched)

    # Analysis
    # image quality
    countability_plot = plotPieChart(table_matched['countability_gt'],
                                     "Countability Percentages")
    image_quality_score = calculate_image_quality_score(table_matched)

    # occupancy detection
    cm_results = reportCountsTprAndFpr(
        CountType.TOTAL_COUNT, table_matched, configs.count_threshold, None,
        ('total_count_int_pred', 'total_count_int_gt'))
    # plot roc curve
    roc = plotPieceWiseWithLegend(
        cm_results.get('fpr_allcases'), cm_results.get('tpr_allcases'),
        cm_results.get('decision_types'), (0.0, 1.0), (0.0, 1.05),
        f"ROC for count threshold {configs.count_threshold}",
        "False Positive Rate", "True Positive Rate", ([0, 1], [0, 1]))

    occ_det_metrics = reportOccupancyMetrics(
        count_truth=table_matched_readable['total_count_int_gt'],
        count_pred=table_matched_readable['total_count_int_pred'],
        labels=[1, 2, 3],
        title='total count')

    occ_results = plotOccupancyDetectionResults(occ_det_metrics)
    pred_error_stats = groupProbabilitiesByErrorType(
        CountType.TOTAL_COUNT, table_matched_readable,
        table_matched_unreadable,
        ("total_count_int_pred", "total_count_int_gt"))
    error_plots = plotErrorStatistics(pred_error_stats)
    find_and_print_misclassifications(table_matched_readable)

    metrics_summary = consolidate_all_performance_metrics(
        image_quality_score, occ_det_metrics, cm_results,
        configs.count_threshold)

    pred_gt_plots = plot_pred_gt_comparisons(table_matched_readable,
                                             table_matched_unreadable,
                                             table_matched['hov_status_pred'],
                                             configs.count_threshold)

    # create the pdf
    def openImgFromMem(image: io.BytesIO) -> Image:
        image.seek(0)
        return Image.open(image)

    elements = [
        # page 1
        {
            'name': 'logo_1',
            'type': 'I',
            'x1': 145.0,
            'y1': 10.0,
            'x2': 200.0,
            'y2': 25.0
        },
        {
            'name': 'title',
            'type': 'T',
            'x1': 65.0,
            'y1': 30.0,
            'x2': 165.0,
            'y2': 40.0,
            'font': 'Helvetica',
            'size': 20.0,
            'bold': 1
        },
        {
            'name': 'metrics_summary',
            'type': 'I',
            'x1': 20.0,
            'y1': 45.0,
            'x2': 100.0,
            'y2': 125.0
        },
        {
            'name': 'count_percentages',
            'type': 'I',
            'x1': 110.0,
            'y1': 45.0,
            'x2': 190.0,
            'y2': 110.0
        },
        {
            'name': 'confusion_matrix',
            'type': 'I',
            'x1': 15.0,
            'y1': 90.0,
            'x2': 105.0,
            'y2': 190.0
        },
        {
            'name': 'lq_predicted_counts',
            'type': 'I',
            'x1': 120.0,
            'y1': 120.0,
            'x2': 180.0,
            'y2': 185.0
        },
        {
            'name': 'gt_pred_hov_status',
            'type': 'I',
            'x1': 15.0,
            'y1': 200.0,
            'x2': 175.0,
            'y2': 270.0
        },
        # page 2
        {
            'name': 'logo_2',
            'type': 'I',
            'x1': 145.0,
            'y1': 10.0,
            'x2': 200.0,
            'y2': 25.0
        },
        {
            'name': 'pred_confidence_histogram',
            'type': 'I',
            'x1': 20.0,
            'y1': 30.0,
            'x2': 190.0,
            'y2': 140.0
        },
        {
            'name': 'roc',
            'type': 'I',
            'x1': 20.0,
            'y1': 150.0,
            'x2': 190.0,
            'y2': 260.0
        },
        # page 3
        {
            'name': 'logo_3',
            'type': 'I',
            'x1': 145.0,
            'y1': 10.0,
            'x2': 200.0,
            'y2': 25.0
        },
        {
            'name': 'error_prob_vs_conf',
            'type': 'I',
            'x1': 20.0,
            'y1': 30.0,
            'x2': 190.0,
            'y2': 140.0
        },
    ]
    path_to_logo = logo_path if logo_path else Image.new('RGB', (55, 15))

    pdf = FPDF()
    templ = FlexTemplate(pdf, elements)
    # page 1
    pdf.add_page()
    templ['logo_1'] = path_to_logo
    templ["title"] = "VOD Performance Report"
    templ["metrics_summary"] = openImgFromMem(metrics_summary)
    templ["count_percentages"] = openImgFromMem(countability_plot)
    templ["confusion_matrix"] = openImgFromMem(occ_results)
    templ["lq_predicted_counts"] = openImgFromMem(
        pred_gt_plots.get('LQ Predicted Counts'))
    templ["gt_pred_hov_status"] = openImgFromMem(
        pred_gt_plots.get('GT vs Pred HOV Status'))
    templ.render()
    # page 2
    pdf.add_page()
    templ['logo_2'] = path_to_logo
    templ["pred_confidence_histogram"] = openImgFromMem(
        error_plots.get('pred_conf_hist'))
    templ["roc"] = openImgFromMem(roc)
    templ.render()
    # page 3
    pdf.add_page()
    templ['logo_3'] = path_to_logo
    templ["error_prob_vs_conf"] = openImgFromMem(
        error_plots.get('error_vs_conf'))
    templ.render()
    return io.BytesIO(pdf.output())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('-g',
                        '--groundtruth',
                        type=str,
                        help='Path to the groundtruth csv file.',
                        required=True)
    parser.add_argument('-p',
                        '--prediction',
                        type=str,
                        help='Path to the prediction csv file.',
                        required=True)
    parser.add_argument('-c',
                        '--config',
                        type=Path,
                        help='Path to the configs yaml file.',
                        required=True)
    parser.add_argument(
        '-r',
        '--results-dir',
        type=Path,
        help='Path to the directory the results should be saved in',
        required=True)
    args = parser.parse_args()

    configs_path = Path(args.config).resolve()
    assert configs_path.is_file(
    ), f'Configuration file {configs_path} does not exist.'

    report_configs = load_yaml_to_dict(VODPerformanceReportConfigs2023,
                                       args.config)

    # read in required cols from csv
    REQUIRED_PRED_COLS = [
        'Vehicle', 'Timestamp', 'Vehicle class', 'Total number of Passengers',
        'Total Confidence', 'Link to picture of 1st row'
    ]
    # read in csv and make sure all required columns exist
    table_pred_raw = pd.read_csv(Path(args.prediction).resolve(),
                                 dtype='str',
                                 usecols=REQUIRED_PRED_COLS)
    # Prepare data
    table_pred = format_prediction_table(table_pred_raw,
                                         report_configs.count_threshold,
                                         report_configs.prediction_params)

    REQUIRED_GT_COLS = [
        'Vehicle', 'Timestamp', 'total_count', 'countability', 'Link', 'User'
    ]
    table_gt_raw = pd.read_csv(Path(args.groundtruth).resolve(),
                               dtype='str',
                               usecols=REQUIRED_GT_COLS)
    table_gt = format_groundtruth_table(table_gt_raw,
                                        report_configs.count_threshold,
                                        report_configs.groundtruth_params)

    logging.info("Generating VOD Performance Report...")

    report_bytes = generate_performance_report(
        table_pred, table_gt, report_configs, args.results_dir,
        Path(__file__).resolve().parents[2].joinpath('Invision_Logo.png'))

    with open(args.results_dir.joinpath(f"{report_configs.pdf_filename}.pdf"),
              'wb') as f:
        f.write(report_bytes.read())

    logging.info(f'{report_configs.pdf_filename} generated.')