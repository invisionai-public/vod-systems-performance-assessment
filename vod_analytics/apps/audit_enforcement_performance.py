HELP = """ Given a groundtruth and a prediction csv, this script first makes enforcement recommendations based 
on predicted LOV confidence and quality scores, then compares those recommendations with the
groundtruth and produces an overall audit performance report that includes a summary PDF and wrong predictions CSVs.

Example:
python audit_enforcement_performance.py -g annotator.csv -p predictions.csv -c 0.8 -q 0.6 0.9
"""

import argparse
from pathlib import Path
from typing import Optional, List, Tuple
import numpy as np
import pandas as pd
from vod_analytics.audit_utils import (assess_enforcement_classification, EnforcementType, 
                                       generate_audit_report_pdf, read_gt, read_pred)


def main(args: Optional[List[str]] = None) -> None:
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('-g', '--gt_csv', type=Path, required=True, help="groundtruth csv file")
    parser.add_argument('-p', '--pred_csv', type=Path, required=True, help="predictions csv file")
    parser.add_argument('-c', '--confidence_thr', type=float, required=True,
        help="confidence threshold that separates LOV and LOV_review buckets")
    parser.add_argument('-q', '--quality_thrs', type=float, nargs=2, required=True,
        help="two quality thresholds: thr_low defines the low quality bucket, "\
             "thr_high separates LOV and LOV_review buckets.")
    parser.add_argument('-o', '--output_dir', type=Path, required=False,
        help="output directory (if not specified, an 'audit_results' subdir will be created "\
             "in pred_csv parent)")
    parser.add_argument('-e', '--enforcement_type', type=str, required=False,
        choices=[e.name for e in EnforcementType], default=EnforcementType.HOV3.name)

    args = parser.parse_args(args)

    df_gt = read_gt(args.gt_csv)
    pred_csv = args.pred_csv
    df_pred = read_pred(pred_csv)
    result = assess_enforcement_classification(df_gt, df_pred, args.confidence_thr, 
        sorted(args.quality_thrs), EnforcementType[args.enforcement_type])
    
    result.print_cm_and_metrics()    
    if args.output_dir is not None:
        output_dir = args.output_dir
    else:
        output_dir = pred_csv.parent.joinpath('audit_results')
    result.save_to_folder(output_dir)    
    
    generate_audit_report_pdf(result, args.confidence_thr, sorted(args.quality_thrs),
        path_to_logo=Path(__file__).resolve().parents[2] / 'Invision_Logo.png',
        output_folder=output_dir)


if __name__ == "__main__":
    main()    