HELP = """ Reformat a prediction CSV into a groundtruth CSV as if the VOD system was an annotator.
The prediction CSV is expected to have an integer count column and a quality percentage column.
Then given 4 quality thresholds, each count and quality, can be converted into a uncertainty-encoded
count of 0+, 1, 1+, 2, 2+, 3, 3+ or more, and a 5-valued countability of unreadable, underterminab-
le, min_viable, good and excellent. Note that the 4 quality thresholds determine the boundary bet-
ween the 5 contability qualities. For instance, with thresholds of [0.25, 0.5, 0.7, 0.9]:

- count = 1, quality = 30% ==> uncertainty_encode_count = 1+, countability = undeterminable
- count = 1, quality = 80% ==> uncertainty_encode_count = 1, countability = good
- count = 4, quality = 95% ==> uncertainty_encode_count = more, countability = excellent

Example:
python reformat_pred_as_gt.py 0.25 0.5 0.7 0.9 -f ../../sample-data/predictions.csv
"""

import argparse
from pathlib import Path
from typing import Tuple, Optional, List
import numpy as np
from vod_analytics.audit_utils import Headers, Labels, read_pred 


def reformat_pred_as_gt(csv_pred: Path, quality_thresholds: Tuple[float, float, float, float],
                        csv_pred_as_gt: Optional[Path] = None) -> None:
    df = read_pred(csv_pred)
    quality_bin_edges = np.array([0.0, *sorted(quality_thresholds)])
    assert np.logical_and(quality_bin_edges>=0, quality_bin_edges<=1).all()

    def reformat_count(count: float, quality: float) -> str:
        # TODO: deal with n/a, nans, etc => return skip
        if np.isnan(count):
            return 'skip'
        countable_threshold = quality_bin_edges[2]
        count = str(int(count)) if quality > countable_threshold else f'{int(count)}+'
        return 'more' if int(count[0]) > 3 else count
    
    df[Headers.gt_count] = df[[Headers.pred_count, Headers.pred_quality_score]].apply(
        lambda x: reformat_count(*x), axis=1)
    
    def quality_to_countability(quality_score: float) -> str:
        # TODO: deal with n/a, nans, etc and skip when count=skip
        if np.isnan(quality_score):
            return 'skip'
        idx = np.max(np.where(quality_score >= quality_bin_edges))
        return Labels.allowed_countabilities[idx]

    df[Headers.gt_countability] = df[Headers.pred_quality_score].apply(quality_to_countability)
    df[Headers.gt_extras] = ''

    if csv_pred_as_gt is None:
        csv_pred_as_gt = csv_pred.parent.joinpath(f'{csv_pred.stem}_as_gt.csv')
        print(f'Reformatting predictions in {csv_pred_as_gt}.')
    df.to_csv(csv_pred_as_gt, index=False, columns=Headers.gt_headers)


def main(args: Optional[List[str]] = None) -> None:
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('thresholds', type=float, nargs=4)
    parser.add_argument('-f', '--file', type=Path, required=True)
    args = parser.parse_args(args)                           
    reformat_pred_as_gt(args.file, tuple(args.thresholds))


if __name__ == "__main__":
    main()