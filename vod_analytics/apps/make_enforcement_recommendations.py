HELP = """ makes enforcement recommendations based on predicted LOV confidence and quality scores

Example:
python make_enforcement_recommendations.py predictions.csv -c 0.97 -q 0.6 0.985
"""

import argparse
from pathlib import Path
from typing import Optional, List
from vod_analytics.audit_utils import read_pred, add_enforcement_recommendation_col 


def main(args: Optional[List[str]] = None) -> None:
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('pred_csv', type=Path, help="predictions csv file")
    parser.add_argument('-c', '--confidence_thr', type=float, required=True,
        help="confidence threshold that separates LOV and LOV_review buckets")
    parser.add_argument('-q', '--quality_thrs', type=float, nargs=2, required=True,
        help="two quality thresholds: thr_low defines the low quality bucket, "\
            "thr_high separates LOV and LOV_review buckets.")
    parser.add_argument('-o', '--output_csv', type=Path, required=False,
        help="output csv file (if not specified, one will be created next to "\
            "pred_csv with the same name suffixed with '_with_recommendations'")
    args = parser.parse_args(args)

    pred_csv = args.pred_csv
    df_pred = read_pred(pred_csv)
    add_enforcement_recommendation_col(df_pred, args.confidence_thr, sorted(args.quality_thrs))
    
    if args.output_csv is not None:
        output_csv = args.output_csv
    else:
        output_csv = pred_csv.parent / f'{pred_csv.stem}_with_recommendations.csv'
    df_pred.to_csv(output_csv, index=False)


if __name__ == "__main__":
    main()    