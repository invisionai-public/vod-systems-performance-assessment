HELP = """ compares two audit gt CSV files against each other and generates a diff (mismatch). The
first CSV file is assumed to be the reference (the annotation project supervisor).

Example:
python diff_audits.py ../../sample-data/audit_supervisor.csv  ../../sample-data/audit_annotator.csv
"""

import argparse
from pathlib import Path
from typing import Optional, List
import pandas as pd
from vod_analytics.audit_utils import Headers, Labels, read_gt, assert_tables_datetime_match
 

def write_count_diff_to_csv(
        supervisor_gt: Path,
        annotator_gt: Path,
        diff_csv: Optional[Path] = None,
        ignore_plus: bool = False,
        ignore_supervisor_skipped: bool = False) -> None:
    """ compares the count column of the annotator_gt CSV file against that from supervisor_gt and
    writes all the mismatching rows to a diff file (annotator_gt + count and countability sugge-
    stions from the supervisor csv)"""

    table_supervisor = read_gt(supervisor_gt)
    table_annotator = read_gt(annotator_gt)
    assert_tables_datetime_match(table_supervisor, table_annotator)

    char_to_strip = '+' if ignore_plus else ''  # strip this character from the count comparison
    mismatch_indices = table_annotator[Headers.gt_count].str.strip(char_to_strip) \
        != table_supervisor[Headers.gt_count].str.strip(char_to_strip)
    table_mismatch = table_annotator.loc[mismatch_indices, :]
    feedback_cols = [f'{Headers.gt_count}_suggested', f'{Headers.gt_countability}_suggested']
    table_mismatch[feedback_cols[0]] = table_supervisor.loc[mismatch_indices, Headers.gt_count]
    table_mismatch[feedback_cols[1]] = table_supervisor.loc[mismatch_indices,
                                                               Headers.gt_countability]

    if ignore_supervisor_skipped:  # ignore mismatches in which the supervisor count is 'skip'
        table_mismatch = table_mismatch[table_mismatch[feedback_cols[0]] != 'skip']
    
    if diff_csv is None:
        diff_csv = annotator_gt.parent.joinpath(f'{annotator_gt.stem}_to_revisit.csv')
        print(f'Wrote diff in {diff_csv}.')
    table_mismatch.to_csv(diff_csv, index=False, columns=Headers.gt_headers + feedback_cols)


def main(args: Optional[List[str]] = None) -> None:
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('files', type=Path, nargs=2)
    parser.add_argument('-m', '--mode', type=str,
                        choices=['strong', 'medium', 'soft'], default='medium')

    args = parser.parse_args(args)
    supervisor_csv = args.files[0].resolve()
    annotator_csv = args.files[1].resolve()
    ignore = {'strong': {'ignore_plus': False, 'ignore_supervisor_skipped': False},
              'medium': {'ignore_plus': True, 'ignore_supervisor_skipped': False},
              'soft': {'ignore_plus': True, 'ignore_supervisor_skipped': True}}
    
    write_count_diff_to_csv(supervisor_csv, annotator_csv, **ignore[args.mode])


if __name__ == "__main__":
    main()    