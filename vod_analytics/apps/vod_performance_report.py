#!/usr/bin/env python3
HELP = """VOD_Performance_Report.py
There are 5 steps to using this script:
1. Clone [Invision's Public Repository](https://gitlab.com/invisionai-public/vod-systems-performance-assessment.git).
2. Edit any necessary configuration parameters in `vod_performance_report_configs.yml`.
   Make sure you change the `system_includes_lpr` parameter to `True` or `False` to include or exclude LPR-related code respectively.
3. If you want plots to persist, change the plt.ion() on line 35 to plt.ioff() to turn off interactive mode.
4. You can import `run_performance_notebook` to run everything from another script.
5. A script is given at the end of this file that can be run with 
   `./vod_performance_report.py -i [report-identifier] -c [report-configs] -g [groundtruth-file] -p [prediction-file]`

This script will run through all code, analyze and visualize the data and produce a PDF summary report for you.
You can find the automatically produced report, `Report_MM-DD-YYYY_[report-identifier].pdf` in the created `Results` folder.

"""

import argparse
from itertools import product
from pathlib import Path
from typing import Tuple, Dict, List, Union, Optional

from fpdf import FPDF
from fpdf.enums import XPos, YPos
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import register_matplotlib_converters

from vod_analytics.configs import (load_yaml_to_dict,
                                   VODPerformanceReportConfigs)
from vod_analytics.data_cleaning import removeRowsWithNanColValues
from vod_analytics.matching import Match_Pred_and_Gt_With
from vod_analytics.metrics import (reportWindowDetectionAccuracy,
                                   reportOccupancyMetrics,
                                   reportCountsTprAndFpr, CountType,
                                   groupProbabilitiesByErrorType)
from vod_analytics.pdf import (imgFromMemoryInPDF, addHeader,
                               addPage_ifNecessary)
from vod_analytics.plotting import (plotPieceWiseWithLegend,
                                    plotLayeredHistogram, plotStackedBars,
                                    radar_factory, plotTwoColumnTable,
                                    plotOccupancyDetectionResults,
                                    saveFigToMem)

register_matplotlib_converters()
plt.ion()  # turns on interactive mode, closes all plt.show() automatically


def validate_and_format_pred_and_gt_dataframes(
        table_pred: pd.DataFrame, table_gt: pd.DataFrame,
        configs: VODPerformanceReportConfigs
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    REQUIRED_PRED_COLS = [
        configs.prediction_params.prediction_col_names.date,
        configs.prediction_params.prediction_col_names.time,
        configs.prediction_params.prediction_col_names.front_passenger_count,
        configs.prediction_params.prediction_col_names.rear_passenger_count,
        configs.prediction_params.prediction_col_names.total_passenger_count,
        configs.prediction_params.prediction_col_names.
        total_passenger_confidence,
        configs.prediction_params.prediction_col_names.vehicle_image_link
    ]
    if configs.system_includes_lpr:
        assert configs.prediction_params.prediction_col_names.license_plate is not None,\
               "'license_plate' must exist with the corresponding pred column name."
        assert configs.prediction_params.prediction_col_names.license_plate_image_link is not None,\
               "'license_plate_image_link' must exist with the corresponding pred column name."
        REQUIRED_PRED_COLS.extend([
            configs.prediction_params.prediction_col_names.license_plate,
            configs.prediction_params.prediction_col_names.
            license_plate_image_link
        ])

    NUMERIC_PRED_COLS = [
        configs.prediction_params.prediction_col_names.front_passenger_count,
        configs.prediction_params.prediction_col_names.rear_passenger_count,
        configs.prediction_params.prediction_col_names.total_passenger_count,
        configs.prediction_params.prediction_col_names.
        total_passenger_confidence
    ]
    table_pred.fillna('nan', inplace=True)  # np.nan => 'nan'
    table_gt.fillna('nan', inplace=True)  # np.nan => 'nan'

    # verify predictions CSV file format
    for col in REQUIRED_PRED_COLS:
        assert col in table_pred, f"pred file is missing column {col}."

    # if % is included in the values for the confidence column, remove it so it can be converted to float
    table_pred[configs.prediction_params.prediction_col_names.
               total_passenger_confidence] = table_pred[
                   configs.prediction_params.prediction_col_names.
                   total_passenger_confidence].str.replace('%', '')

    table_pred[
        NUMERIC_PRED_COLS] = table_pred.loc[:, NUMERIC_PRED_COLS].astype(
            float)  # integers or nan, thus float
    # change all confidences to be between [0, 100] if necessary
    if table_pred[configs.prediction_params.prediction_col_names.
                  total_passenger_confidence].max() <= 1:
        table_pred[configs.prediction_params.prediction_col_names.
                   total_passenger_confidence] = table_pred[
                       configs.prediction_params.prediction_col_names.
                       total_passenger_confidence].multiple(100)

    # verify gt CSV file format
    REQUIRED_GT_COLS = [
        configs.groundtruth_params.groundtruth_col_names.date,
        configs.groundtruth_params.groundtruth_col_names.time,
        configs.groundtruth_params.groundtruth_col_names.front_passenger_count,
        configs.groundtruth_params.groundtruth_col_names.rear_passenger_count
    ]
    if configs.groundtruth_params.groundtruth_col_names.third_row_passenger_count is not None:
        REQUIRED_GT_COLS.extend([
            configs.groundtruth_params.groundtruth_col_names.
            third_row_passenger_count
        ])
    if configs.system_includes_lpr:
        assert configs.groundtruth_params.groundtruth_col_names.license_plate is not None,\
               "'license_plate' must exist with the corresponding gt column name."
        REQUIRED_GT_COLS.extend(
            [configs.groundtruth_params.groundtruth_col_names.license_plate])

    for col in REQUIRED_GT_COLS:
        assert col in table_gt, f"pred file is missing column {col}."

    table_gt[configs.groundtruth_params.groundtruth_col_names.
             front_passenger_count] = table_gt[
                 configs.groundtruth_params.groundtruth_col_names.
                 front_passenger_count].str.strip(
                 )  # remove trailing whitespaces if any
    table_gt[configs.groundtruth_params.groundtruth_col_names.
             rear_passenger_count] = table_gt[
                 configs.groundtruth_params.groundtruth_col_names.
                 rear_passenger_count].str.strip(
                 )  # remove trailing whitespaces if any
    gt_frontcounts_validity = [
        count in configs.groundtruth_params.allowed_frontcounts
        for count in table_gt.frontcount_gt
    ]
    gt_rearcounts_validity = [
        count in configs.groundtruth_params.allowed_rearcounts
        for count in table_gt.rearcount_gt
    ]
    gt_counts_invalidity = np.logical_or(
        np.logical_not(gt_frontcounts_validity),
        np.logical_not(gt_rearcounts_validity))
    n_invalid_gt_counts = np.sum(gt_counts_invalidity)
    table_gt['gt_case_ID'] = range(
        2, 2 + len(table_gt)
    )  # unique gt case identifier (starting from 2 to match csv row number in editor)
    if n_invalid_gt_counts > 0:
        if configs.groundtruth_params.gt_counts_validation == 'remove-rows':
            table_gt.drop(table_gt[gt_counts_invalidity].index, inplace=True)
            print(
                f"WARNING: removed {n_invalid_gt_counts} rows with invalid counts from GT file."
            )
        elif configs.groundtruth_params.gt_counts_validation == 'produce-error':
            assert all(gt_frontcounts_validity), (
                "GT file contains bad counts under the frontcount_gt column. "
                f"Only {configs.groundtruth_params.allowed_frontcounts} are allowable."
            )
            assert all(gt_rearcounts_validity), (
                "GT file contains bad counts under the rearcount_gt column. "
                f"Only {configs.groundtruth_params.allowed_rearcounts} are allowable."
            )
        else:
            raise ValueError(
                f"Unrecognized gt_counts_validation mode of {configs.groundtruth_params.gt_counts_validation}."
            )

    # unify gt nan values to assure smooth conversion from str to float later
    for column, symbol in product(['frontcount_gt', 'rearcount_gt'],
                                  ['N/A', 'n/a']):
        table_gt[column] = table_gt.loc[:, column].str.replace(symbol, 'nan')

    # convert time strings to datetime objects
    table_pred['Datetime'] = pd.to_datetime(
        table_pred[configs.prediction_params.prediction_col_names.date] + ' ' +
        table_pred[configs.prediction_params.prediction_col_names.time],
        format=configs.prediction_params.datetime_format)
    table_gt['Datetime_gt'] = pd.to_datetime(
        table_gt[configs.groundtruth_params.groundtruth_col_names.date] + ' ' +
        table_gt[configs.groundtruth_params.groundtruth_col_names.time],
        format=configs.groundtruth_params.datetime_format)

    if configs.system_includes_lpr:
        # strip the '-XYZ' country suffix in case it exists in the plate numbers
        remove_suffix = lambda x: x.split('-')[0]
        table_gt[configs.groundtruth_params.groundtruth_col_names.
                 license_plate] = table_gt[
                     configs.groundtruth_params.groundtruth_col_names.
                     license_plate].apply(remove_suffix)
        table_pred[configs.prediction_params.prediction_col_names.
                   license_plate] = table_pred[
                       configs.prediction_params.prediction_col_names.
                       license_plate].apply(remove_suffix)

    # keep and rename only required pred and gt cols to standardize naming for rest of analysis
    RENAME_PRED_COLS = {
        'Datetime':
        'Datetime_pred',
        configs.prediction_params.prediction_col_names.license_plate:
        'LPR_pred',
        configs.prediction_params.prediction_col_names.front_passenger_count:
        'frontcount_pred',
        configs.prediction_params.prediction_col_names.rear_passenger_count:
        'rearcount_pred',
        configs.prediction_params.prediction_col_names.total_passenger_count:
        'total_count_pred',
        configs.prediction_params.prediction_col_names.total_passenger_confidence:
        'total_count_confidence_pred',
        configs.prediction_params.prediction_col_names.vehicle_image_link:
        'VOD_image_link',
        configs.prediction_params.prediction_col_names.license_plate_image_link:
        'LPR_image_link'
    }

    RENAME_GT_COLS = {
        configs.groundtruth_params.groundtruth_col_names.date:
        'Date_gt',
        configs.groundtruth_params.groundtruth_col_names.time:
        'Time_gt',
        'Datetime_gt':
        'Datetime_gt',
        configs.groundtruth_params.groundtruth_col_names.front_passenger_count:
        'frontcount_gt',
        configs.groundtruth_params.groundtruth_col_names.rear_passenger_count:
        'rearcount_gt',
        configs.groundtruth_params.groundtruth_col_names.license_plate:
        'LPR_gt',
        configs.groundtruth_params.groundtruth_col_names.third_row_passenger_count:
        'thirdrowcount_gt'
    }

    if configs.groundtruth_params.groundtruth_col_names.third_row_passenger_count is None:
        RENAME_GT_COLS.pop(
            configs.groundtruth_params.groundtruth_col_names.
            third_row_passenger_count, None)
    if not configs.system_includes_lpr:
        RENAME_PRED_COLS.pop(
            configs.prediction_params.prediction_col_names.license_plate, None)
        RENAME_PRED_COLS.pop(
            configs.prediction_params.prediction_col_names.
            license_plate_image_link, None)
        RENAME_GT_COLS.pop(
            configs.groundtruth_params.groundtruth_col_names.license_plate,
            None)

    # rename prediction columns
    table_temp = table_pred.loc[:, list(RENAME_PRED_COLS.keys())]
    table_pred_renamed = table_temp.rename(columns=RENAME_PRED_COLS)

    # fix space in links and change to unblurred VOD image link for better later use
    table_pred_renamed['VOD_image_link'] = table_pred_renamed[
        'VOD_image_link'].str.replace('row_image/1/',
                                      'row_image/0/').str.replace(' ', '%20')
    # rename groundtruth columns
    table_temp = table_gt.loc[:, list(RENAME_GT_COLS.keys())]
    table_gt_renamed = table_temp.rename(columns=RENAME_GT_COLS)

    if configs.system_includes_lpr:
        table_pred_renamed['LPR_image_link'] = table_pred_renamed[
            'LPR_image_link'].str.replace(' ', '%20')

    return table_pred_renamed, table_gt_renamed


def pred_to_gt_association(
        table_pred: pd.DataFrame, table_gt: pd.DataFrame,
        configs: VODPerformanceReportConfigs) -> Dict[str, pd.DataFrame]:

    if configs.system_includes_lpr:
        table_matched, table_unmatched = Match_Pred_and_Gt_With.STR(
            table_pred, table_gt, configs.matching_gt_pred_params.timediff_tol,
            configs.matching_gt_pred_params.ocr_max_dist)
    else:
        table_matched, table_unmatched = Match_Pred_and_Gt_With.TIMESTAMP(
            table_pred, table_gt, configs.matching_gt_pred_params.timediff_tol)

    # separate human unreadable (unintelligible) gt rows
    GT_COL_USED_FOR_QUALITY = [
        configs.groundtruth_params.groundtruth_col_names.front_passenger_count,
        configs.groundtruth_params.groundtruth_col_names.rear_passenger_count
    ]
    tagged_rows = table_matched[GT_COL_USED_FOR_QUALITY].isin(
        configs.groundtruth_params.unreadable_gt_counts).any(axis=1)
    table_matched_unreadable = table_matched.loc[tagged_rows, :]

    # keep human readable gt rows in a separate table
    table_matched_readable = table_matched.loc[~tagged_rows, :]

    # table_matched_readable should contain numeric (integers and np.nan, thus 'float') values under gt counts
    table_matched_readable = table_matched_readable.astype({
        'frontcount_gt':
        float,
        'rearcount_gt':
        float
    })

    return {
        'table_matched': table_matched,
        'table_matched_readable': table_matched_readable,
        'table_matched_unreadable': table_matched_unreadable,
        'table_unmatched': table_unmatched
    }


def capture_success_metrics(
        table_gt: pd.DataFrame,
        table_matched: pd.DataFrame,
        table_matched_readable: pd.DataFrame,
        system_includes_lpr: bool = True) -> Dict[str, Union[float, int]]:
    # combined LPR/Triggering Success Rate
    N, n = len(table_gt), len(table_matched)
    print(
        f'Out of {N} total ground truth vehicles, {n} are correctly detected/matched.'
    )
    if system_includes_lpr:
        rate_LPR_trg = n / N
        print(
            f'The combined LPR and triggering success rate is: {rate_LPR_trg*100:.1f}%.'
        )

    # Image Quality Score
    img_quality_score = len(table_matched_readable) / n
    print(
        f'Out of {n} matched vehicles, {len(table_matched_readable)} are perfectly readable based on the ground truth.'
    )
    print(
        'The image quality score - assuming the gt counts originate from manual review '
        f'(not prescribed tests) - is: {img_quality_score*100:.1f}%.')
    results = {
        'image_quality_score': img_quality_score,
        'num_gt_rows': N,
        'num_matched_rows': n,
        'num_readable_matched_rows': len(table_matched_readable)
    }
    if system_includes_lpr:
        results['rate_LPR_trigger'] = rate_LPR_trg

    return results


def occupancy_detection_metrics(
    table_matched_readable: pd.DataFrame,
    table_matched_unreadable: pd.DataFrame,
    count_thresholds: List[int] = [2, 3]
) -> Dict[str, Union[float, pd.DataFrame, Dict]]:
    frontwindow_det_accu = reportWindowDetectionAccuracy(
        table_matched_readable, 'front')
    rearwindow_det_accu = reportWindowDetectionAccuracy(
        table_matched_readable, 'rear')

    # deal with nans (non-existing windows)
    for index, row in table_matched_readable.iterrows():
        if pd.isna(row['rearcount_gt']) and pd.isna(row['rearcount_pred']):
            table_matched_readable.loc[index, 'rearcount_gt'] = 0
            table_matched_readable.loc[index, 'rearcount_pred'] = 0
    table_occupancy_metrics = table_matched_readable.dropna()

    # report occupancy detection metrics for total count (1, 2, 3+) classes
    occupancy_results = reportOccupancyMetrics(
        count_truth=table_occupancy_metrics['frontcount_gt'] +
        table_occupancy_metrics['rearcount_gt'],
        count_pred=table_occupancy_metrics['frontcount_pred'] +
        table_occupancy_metrics['rearcount_pred'],
        labels=[1, 2, 3],
        title='total count')

    # remove rows that have nan or empty values in the specified columns
    table_matched_readable_filtered = removeRowsWithNanColValues(
        table_matched_readable,
        ['total_count_confidence_pred', 'rearcount_gt'])

    plots_in_mem = {}
    for count_threshold in count_thresholds:
        tpr_fpr_report = reportCountsTprAndFpr(
            CountType.FRONT_REAR_COUNTS, table_matched_readable_filtered,
            count_threshold)
        # plot ROC curves
        plots_in_mem['roc_threshold_' +
                     str(count_threshold)] = plotPieceWiseWithLegend(
                         tpr_fpr_report['fpr_allcases'], tpr_fpr_report['tpr_allcases'],
                         tpr_fpr_report['decision_types'], (0.0, 1.0),
                         (0.0, 1.05),
                         f"ROC for count threshold {count_threshold}",
                         "False Positive Rate", "True Positive Rate",
                         ([0, 1], [0, 1]))

    pred_error_stats = groupProbabilitiesByErrorType(
        CountType.FRONT_REAR_COUNTS, table_matched_readable,
        table_matched_unreadable)

    plots_in_mem['pred_conf_hist'] = plotLayeredHistogram(
        [
            pred_error_stats['correct'], pred_error_stats['incorrect'],
            pred_error_stats['low quality']
        ], ["Correct", "Incorrect", "Low Quality"],
        "Histogram of Prediction Confidences", "Confidence (%)", "Counts", 50,
        False, (24 / 2.54, 16 / 2.54))

    # return the bins here so they are consistent across the grouping
    num_bins = 50
    total_cut, edges = pd.cut(pred_error_stats['total readable'],
                              num_bins,
                              retbins=True)
    total = pd.Series(total_cut).value_counts().sort_index()
    undercounted = pd.Series(pd.cut(pred_error_stats['undercounted'],
                                    edges)).value_counts().sort_index()
    overcounted = pd.Series(pd.cut(pred_error_stats['overcounted'],
                                   edges)).value_counts().sort_index()
    labels = [index.left for index in total.index.values]
    plots_in_mem['error_prob'] = plotStackedBars(
        labels, [
            undercounted.divide(total, fill_value=0),
            overcounted.divide(total, fill_value=0)
        ], ["Undercounted", "Overcounted"], "Error Probability vs Confidence",
        "Confidence (%)", "Probability", (24 / 2.54, 16 / 2.54), 'edge')
    return {
        'frontwindow_det_accuracy': frontwindow_det_accu,
        'rearwindow_det_accuracy': rearwindow_det_accu,
        'occupancy_metrics': occupancy_results,
        'table_occupancy_metrics': table_occupancy_metrics,
        'occupancy_plots': plots_in_mem
    }


def print_overcounted_and_undercounted_misclassifications(
        table_occupancy_metrics: pd.DataFrame,
        system_includes_lpr: bool = True) -> None:
    pd.set_option('display.max_colwidth', 5000)
    totcount_clipped_pred = np.clip(table_occupancy_metrics.frontcount_pred +
                                    table_occupancy_metrics.rearcount_pred, 0,
                                    3)  # clip to make 3 represent 3+
    totcount_clipped_gt = np.clip(
        table_occupancy_metrics.frontcount_gt +
        table_occupancy_metrics.rearcount_gt, 0, 3)
    condition = totcount_clipped_pred > totcount_clipped_gt
    print(
        'This is a list of all allegedly overcounted occupancy misclassifications:'
    )
    print(table_occupancy_metrics.loc[condition, [
        'VOD_image_link', 'frontcount_pred', 'frontcount_gt', 'rearcount_pred',
        'rearcount_gt'
    ]].to_string(index=False))

    condition = totcount_clipped_pred < totcount_clipped_gt
    print(
        'This is a list of all allegedly undercounted occupancy misclassifications:'
    )
    cols_to_print = [
        'VOD_image_link', 'frontcount_pred', 'frontcount_gt', 'rearcount_pred',
        'rearcount_gt'
    ]
    if system_includes_lpr:
        cols_to_print.insert(0, 'LPR_gt')

    print(table_occupancy_metrics.loc[condition,
                                      cols_to_print].to_string(index=False))


def pdf_report_generator(img_quality_score: float,
                         frontwindow_det_accu: float,
                         rearwindow_det_accu: float,
                         occupancy_results: pd.DataFrame,
                         table_occupancy_metrics: pd.DataFrame,
                         occupancy_plots: dict,
                         table_matched_readable: pd.DataFrame,
                         table_matched_unreadable: pd.DataFrame,
                         table_unmatched: pd.DataFrame,
                         configs: VODPerformanceReportConfigs,
                         results_dir: Path,
                         rate_LPR_trg: Optional[float] = None,
                         logo_img_path: Optional[Path] = None) -> None:

    # Generate First Page
    pdf = FPDF()
    pdf.add_page()
    addHeader(
        pdf,
        f'{configs.report_gen_params.report_timespan}, {configs.report_gen_params.vod_system_tag}',
        logo_img_path)

    # summarize all performance metrics in a radar chart or table
    metrics = [
        'Img Quality', 'Front Window Detection', 'Rear Window Detection',
        'Occupancy Detection Accuracy', 'Over-counting', 'Under-counting',
        'TPR2+', 'FPR2+', 'TPR3+', 'FPR3+'
    ]

    CM = occupancy_results['CM']
    CM_2plus = np.array([[CM[0, 0], CM[0, 1] + CM[0, 2]],
                         [CM[1, 0] + CM[2, 0],
                          np.sum(CM[1:, 1:])]])
    CM_3plus = np.array([[np.sum(CM[0:2, 0:2]), CM[0, 2] + CM[1, 2]],
                         [CM[2, 0] + CM[2, 1], CM[2, 2]]])
    TPR_FPR = lambda M: (M[0, 0] / np.sum(M[:, 0]), M[0, 1] / np.sum(M[:, 1]))
    TPR_2plus, FPR_2plus = TPR_FPR(CM_2plus)
    TPR_3plus, FPR_3plus = TPR_FPR(CM_3plus)

    metrics_vals = [
        img_quality_score, frontwindow_det_accu, rearwindow_det_accu,
        occupancy_results['accuracy'], occupancy_results['overcounting'],
        occupancy_results['undercounting'], TPR_2plus, FPR_2plus, TPR_3plus,
        FPR_3plus
    ]
    plot_title = 'Performance Metrics Summary'
    if configs.system_includes_lpr:
        metrics.insert(0, 'LPR-Trigger Success')
        metrics_vals.insert(0, rate_LPR_trg)

    if configs.report_gen_params.metrics_summary_style == 'radar':
        metrics = [
            metric.replace(' ', '\n').replace('-', '\n') for metric in metrics
        ]
        theta = radar_factory(len(metrics), frame='polygon')
        fig, ax = plt.subplots(figsize=(9 / 2.54, 9 / 2.54),
                               subplot_kw=dict(projection='radar'))
        ax.set_title(plot_title,
                     weight='bold',
                     size='medium',
                     position=(0.5, 1.2),
                     horizontalalignment='center',
                     verticalalignment='center')
        ax.set_rgrids([0.05, 0.25, 0.5, 0.75, 0.95])
        ax.plot(theta, metrics_vals, color='y')
        ax.fill(theta, metrics_vals, facecolor='y', alpha=0.25)
        ax.set_varlabels(metrics)
        perf_metrics_summary = saveFigToMem(fig)
    else:
        vals = [f"{metric * 100 : .1f}%" for metric in metrics_vals]
        perf_metrics_summary = plotTwoColumnTable(
            metrics, vals, 'Performance Metrics Summary',
            ('Metrics', 'Metric Values'))

    # add the radar chart or table to the pdf report
    pdf.cell(190, 10, 'VOD System Performance Statistics', align='C')
    imgFromMemoryInPDF(perf_metrics_summary, pdf, x=20, y=35, h=80)

    # generate the confusion matrix figure
    conf_matrix = plotOccupancyDetectionResults(occupancy_results)

    # add the CM plot to the report
    imgFromMemoryInPDF(conf_matrix, pdf, x=120, y=35, h=80)

    # add the Prediction Confidence plot to the report
    if configs.report_gen_params.plot_pred_confidences_hist:
        imgFromMemoryInPDF(occupancy_plots['pred_conf_hist'],
                           pdf,
                           x=10,
                           y=120,
                           h=60)
    if configs.report_gen_params.plot_error_prob_vs_confidence:
        imgFromMemoryInPDF(occupancy_plots['error_prob'],
                           pdf,
                           x=110,
                           y=120,
                           h=60)

    # add ROC curves from Section 3 to the report
    for count_threshold in configs.count_thresholds:
        imgFromMemoryInPDF(occupancy_plots['roc_threshold_' +
                                           str(count_threshold)],
                           pdf,
                           x=10 + 100 * (count_threshold - 2),
                           y=190,
                           h=60)

    # add second (and more if necessary) page with table of errors and links to images
    pdf.add_page()
    addHeader(
        pdf,
        f'{configs.report_gen_params.report_timespan}, {configs.report_gen_params.vod_system_tag}',
        logo_img_path)

    LPR_TRIGGER_TABLE_TITLE = 'Triggering Errors'
    LPR_TRIGGER_TABLE_GT_HEADER = 'Datetime'
    LPR_TRIGGER_TABLE_APPROX_HEADER = 'Approx Datetime Match'
    if configs.system_includes_lpr:
        LPR_TRIGGER_TABLE_TITLE = f'LPR/{LPR_TRIGGER_TABLE_TITLE}'
        LPR_TRIGGER_TABLE_GT_HEADER = f'License Plate / {LPR_TRIGGER_TABLE_GT_HEADER}'
        LPR_TRIGGER_TABLE_APPROX_HEADER = f'Tentative Match / {LPR_TRIGGER_TABLE_APPROX_HEADER}'

    # unmatched vehicles (license plate errors table)
    pdf.cell(190,
             10,
             LPR_TRIGGER_TABLE_TITLE,
             align='C',
             new_x=XPos.LMARGIN,
             new_y=YPos.NEXT)
    pdf.set_font('helvetica', '', size=8)
    pdf.cell(25, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)

    printed_entries = 0
    if table_unmatched.empty:
        pdf.cell(55,
                 6,
                 txt='There is no error of this type.',
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP)
        printed_entries += 1
    else:
        pdf.cell(55,
                 6,
                 txt=LPR_TRIGGER_TABLE_GT_HEADER,
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP,
                 border=1,
                 align='C')
        pdf.cell(55,
                 6,
                 txt=LPR_TRIGGER_TABLE_APPROX_HEADER,
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP,
                 border=1,
                 align='C')
        if configs.system_includes_lpr:
            pdf.cell(30,
                     6,
                     'LPR Image',
                     new_x=XPos.LMARGIN,
                     new_y=YPos.NEXT,
                     border=1,
                     align='C')

        for _, testcase in table_unmatched.head(
                configs.report_gen_params.lpr_trigger_table_max_entries
        ).iterrows():
            printed_entries += 1
            pdf.cell(25, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
            lpr_trigger_table_gt_val = f'{str(testcase.Datetime_gt).split(".")[0]}'
            lpr_trigger_table_approx_val = f'{str(testcase.Datetime_pred).split(".")[0]}'
            if configs.system_includes_lpr:
                lpr_trigger_table_gt_val = f'{testcase.LPR_gt} / {lpr_trigger_table_gt_val}'
                lpr_trigger_table_approx_val = f'{testcase.LPR_pred} / {lpr_trigger_table_approx_val}'
            pdf.cell(55,
                     6,
                     txt=lpr_trigger_table_gt_val,
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(55,
                     6,
                     txt=lpr_trigger_table_approx_val,
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')

            if configs.system_includes_lpr:
                # add LPR image link
                link_URL = str(testcase['LPR_image_link'])
                if link_URL == 'N/A':
                    pdf.cell(30,
                             6,
                             txt='N/A',
                             link='',
                             new_x=XPos.LMARGIN,
                             new_y=YPos.NEXT,
                             border=1,
                             align='C')
                else:
                    pdf.set_text_color(0, 0, 255)
                    pdf.set_font('helvetica', 'U', size=8)
                    pdf.cell(30,
                             6,
                             txt='Link',
                             link=link_URL,
                             new_x=XPos.LMARGIN,
                             new_y=YPos.NEXT,
                             border=1,
                             align='C')
                    pdf.set_text_color(0, 0, 0)
                    pdf.set_font('helvetica', '', size=8)

        if len(table_unmatched
               ) > configs.report_gen_params.lpr_trigger_table_max_entries:
            printed_entries += 1
            pdf.cell(25, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
            pdf.cell(55,
                     6,
                     txt=str("..."),
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(55,
                     6,
                     txt=str("..."),
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(30,
                     6,
                     txt=str("..."),
                     new_x=XPos.LMARGIN,
                     new_y=YPos.NEXT,
                     border=1,
                     align='C')
            pdf.set_text_color(0, 0, 0)
            pdf.set_font('helvetica', '', size=8)

        table_unmatched.to_csv(results_dir / 'unmatched_cases.csv',
                               index=False)

    # helper function for printing and saving occupancy misclassification cases
    def print_and_save_MisclassificationTable(
        pdf,
        table,
        pred_vs_gt_relation,
        output_csv_name,
        printed_entries=0,
        occu_misclass_table_cap=configs.report_gen_params.
        occu_misclass_table_max_entries):
        # parse the misclassification
        totcount_clipped_pred = np.clip(
            table.frontcount_pred + table.rearcount_pred, 0, 3).astype('int')
        totcount_clipped_gt = np.clip(table.frontcount_gt + table.rearcount_gt,
                                      0, 3).astype('int')
        condition = pred_vs_gt_relation(totcount_clipped_pred,
                                        totcount_clipped_gt)
        count2class_dict = {1: '1', 2: '2', 3: '3+'}

        # print the table
        pdf.set_font('helvetica', '', size=8)
        pdf.cell(12, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
        if np.sum(condition) == 0:
            pdf.cell(80,
                     6,
                     txt='There is no classification error of this type.',
                     new_x=XPos.LMARGIN,
                     new_y=YPos.NEXT)
            printed_entries += 1
            return printed_entries

        MISCLASS_PLATE_TIME_HEADER = 'Datetime'
        if configs.system_includes_lpr:
            MISCLASS_PLATE_TIME_HEADER = f'License Plate / {MISCLASS_PLATE_TIME_HEADER}'

        pdf.cell(55,
                 6,
                 txt=MISCLASS_PLATE_TIME_HEADER,
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP,
                 border=1,
                 align='C')
        pdf.cell(40,
                 6,
                 txt='Predicted Occupancy',
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP,
                 border=1,
                 align='C')
        pdf.cell(40,
                 6,
                 txt='True Occupancy',
                 new_x=XPos.RIGHT,
                 new_y=YPos.TOP,
                 border=1,
                 align='C')
        pdf.cell(30,
                 6,
                 txt='Occupancy Image',
                 new_x=XPos.LMARGIN,
                 new_y=YPos.NEXT,
                 border=1,
                 align='C')

        for idx, testcase in table[condition].head(
                occu_misclass_table_cap).iterrows():
            printed_entries += 1
            pdf.cell(12, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
            misclass_plate_time_val = f'{str(testcase.Datetime_pred).split(".")[0]}'
            if configs.system_includes_lpr:
                misclass_plate_time_val = f'{testcase.LPR_pred} / {misclass_plate_time_val}'
            pdf.cell(55,
                     6,
                     txt=misclass_plate_time_val,
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')

            # print occupancy classes and row count details
            r1, r2 = int(testcase.frontcount_pred), int(
                testcase.rearcount_pred)  # predicted row counts
            pdf.cell(
                40,
                6,
                txt=
                f'{count2class_dict[totcount_clipped_pred[idx]]}  ({r1} + {r2})',
                new_x=XPos.RIGHT,
                new_y=YPos.TOP,
                border=1,
                align='C')
            if configs.groundtruth_params.groundtruth_col_names.third_row_passenger_count is not None:
                R1, R2, R3 = int(
                    testcase.frontcount_gt), int(testcase.rearcount_gt) - int(
                        testcase.thirdrowcount_gt), int(
                            testcase.thirdrowcount_gt)  # gt row counts
                pdf.cell(
                    40,
                    6,
                    txt=
                    f'{count2class_dict[totcount_clipped_gt[idx]]}  ({R1} + {R2} + {R3})',
                    new_x=XPos.RIGHT,
                    new_y=YPos.TOP,
                    border=1,
                    align='C')
            else:
                R1, R2 = int(testcase.frontcount_gt), int(
                    testcase.rearcount_gt)  # gt row counts
                pdf.cell(
                    40,
                    6,
                    txt=
                    f'{count2class_dict[totcount_clipped_gt[idx]]}  ({R1} + {R2})',
                    new_x=XPos.RIGHT,
                    new_y=YPos.TOP,
                    border=1,
                    align='C')

            pdf.set_text_color(0, 0, 255)
            pdf.set_font('helvetica', 'U', size=8)
            pdf.cell(30,
                     6,
                     txt='Link',
                     link=str(testcase['VOD_image_link']),
                     new_x=XPos.LMARGIN,
                     new_y=YPos.NEXT,
                     border=1,
                     align='C')
            pdf.set_text_color(0, 0, 0)
            pdf.set_font('helvetica', '', size=8)

        if len(table[condition]) > occu_misclass_table_cap:
            printed_entries += 1
            pdf.cell(12, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
            pdf.cell(55,
                     6,
                     txt='...',
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(40,
                     6,
                     txt='...',
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(40,
                     6,
                     txt='...',
                     new_x=XPos.RIGHT,
                     new_y=YPos.TOP,
                     border=1,
                     align='C')
            pdf.cell(30,
                     6,
                     txt='...',
                     new_x=XPos.LMARGIN,
                     new_y=YPos.NEXT,
                     border=1,
                     align='C')
            pdf.set_text_color(0, 0, 0)
            pdf.set_font('helvetica', '', size=8)

        # save table into a CSV file and print it in the report
        table[condition].to_csv(results_dir / output_csv_name, index=False)
        return printed_entries

    # overcounted classification errors
    printed_entries = addPage_ifNecessary(
        pdf, printed_entries, configs.report_gen_params.report_timespan,
        configs.report_gen_params.vod_system_tag)
    pdf.set_font('helvetica', 'B', size=12)
    pdf.ln(10)
    pdf.cell(190,
             10,
             'Occupancy Overcounting Errors',
             align='C',
             new_x=XPos.LMARGIN,
             new_y=YPos.NEXT)
    printed_entries = print_and_save_MisclassificationTable(
        pdf,
        table_occupancy_metrics,
        pred_vs_gt_relation=np.greater,
        output_csv_name='overcounting_or_underlabeling.csv',
        printed_entries=printed_entries + 2)

    # undercounted classification errors
    printed_entries = addPage_ifNecessary(
        pdf, printed_entries, configs.report_gen_params.report_timespan,
        configs.report_gen_params.vod_system_tag)
    pdf.set_font('helvetica', 'B', size=12)
    pdf.ln(10)
    pdf.cell(190,
             10,
             'Occupancy Undercounting Errors',
             align='C',
             new_x=XPos.LMARGIN,
             new_y=YPos.NEXT)
    printed_entries = print_and_save_MisclassificationTable(
        pdf,
        table_occupancy_metrics,
        pred_vs_gt_relation=np.less,
        output_csv_name='undercounting_or_overlabeling.csv',
        printed_entries=printed_entries + 2)

    # save low quality image cases and all cases in Results folder
    table_matched_unreadable.to_csv(results_dir / 'low_quality.csv',
                                    index=False)
    table_matched_readable.to_csv(results_dir / 'all_matched_cases.csv',
                                  index=False)

    pdf_file = results_dir / (
        f'Report_{configs.report_gen_params.report_timespan}_'
        f'{configs.report_gen_params.vod_system_tag}.pdf')
    pdf.output(pdf_file)

    print(f'Report successfully saved to {pdf_file}.')


def generate_performance_report(
        table_pred: pd.DataFrame,
        table_gt: pd.DataFrame,
        report_identifier: Optional[str] = None,
        configs_abs_path: Path = 'vod_performance_report_configs.yml',
        logo_image_path: Optional[Path] = None):

    report_configs = load_yaml_to_dict(VODPerformanceReportConfigs,
                                       configs_abs_path)
    # replace report_timespan with report_identifier if given so won't have to edit yaml each time.
    if report_identifier is not None:
        report_configs.report_gen_params.report_timespan = report_identifier

    # create Figures and Results directories
    repo_path = Path.cwd().resolve()
    figures_path = repo_path.joinpath('Figures')
    figures_path.mkdir(parents=True, exist_ok=True)
    results_path = repo_path.joinpath('Results')
    results_path.mkdir(parents=True, exist_ok=True)

    # add Invision Logo to Figures folder
    if logo_image_path:
        assert logo_image_path.is_file(), f"Can't find: {logo_image_path}"

    # check to make sure required columns (as set in vod_performance_report_configs.yml)
    # exist and are formatted correctly
    table_pred_validated, table_gt_validated = validate_and_format_pred_and_gt_dataframes(
        table_pred, table_gt, report_configs)

    # If the system includes an LPR, for each row, we try to find a plate and timestamp match.
    # If an exact plate match and an approximate timestamp match is found,
    # both the matching pred and gt cols are appended to `table_matched`
    # Otherwise an entry associated with that groundtruth testcase is appended to `table_unmatched`.

    # If the system does not include an LPR, only the approximate timestamp match is used.
    # In this case, if a timestamp falls within the tolerance set with `timediff_tol`,
    # that entry is appended to `table_matched`.
    # Otherwise, it will be appended to `table_unmatched`.

    # `table_matched` is further subdivided into two tables,
    # i.e. `table_matched_readable` and `table_matched_unreadable`.
    # The unreadable subdivision is created from the collection of any `table_matched` rows
    # that contain an allowable unreadable tag, `DARK` or `GLARE`.
    associated_tables = pred_to_gt_association(table_pred_validated,
                                               table_gt_validated,
                                               report_configs)

    # calculate high-level LPR/triggering success rate and high-level image quality score
    cap_success_metrics = capture_success_metrics(
        table_gt, associated_tables['table_matched'],
        associated_tables['table_matched_readable'],
        report_configs.system_includes_lpr)

    # calculate front and rear window detection accuracies,
    # occupancy count classification metrics
    # (1, 2 and 3+ confusion matrix, accuracy, undercounting and overcounting rates),
    # and binary classification based on occupancy thresholds of 2+ and 3+ (ROC Curves)
    occupancy_metrics = occupancy_detection_metrics(
        associated_tables['table_matched_readable'],
        associated_tables['table_matched_unreadable'],
        report_configs.count_thresholds)

    print_overcounted_and_undercounted_misclassifications(
        occupancy_metrics['table_occupancy_metrics'],
        report_configs.system_includes_lpr)

    rate_LPR_trigger = None
    if report_configs.system_includes_lpr:
        rate_LPR_trigger = cap_success_metrics['rate_LPR_trigger']
    print('Generating Report')
    pdf_report_generator(cap_success_metrics['image_quality_score'],
                         occupancy_metrics['frontwindow_det_accuracy'],
                         occupancy_metrics['rearwindow_det_accuracy'],
                         occupancy_metrics['occupancy_metrics'],
                         occupancy_metrics['table_occupancy_metrics'],
                         occupancy_metrics['occupancy_plots'],
                         associated_tables['table_matched_readable'],
                         associated_tables['table_matched_unreadable'],
                         associated_tables['table_unmatched'], report_configs,
                         results_path, rate_LPR_trigger, logo_img)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument(
        '-i',
        '--identifier',
        type=str,
        default=None,
        help='str to help identify the specific generated report ie the date.')
    parser.add_argument(
        '-c',
        '--configs',
        type=Path,
        default=Path(__file__).parent.joinpath(
            'vod_performance_report_configs.yml'),
        help=('path to the configs.yml file.'
              'Defaults to ./vod_performance_report_configs.yml'))
    parser.add_argument(
        '-g',
        '--groundtruth',
        type=Path,
        default=Path(__file__).parents[2].joinpath(
            'sample-data/groundtruth_07-08-2021_anonymized.csv'),
        help='path to the groundtruth csv file. Defaults to sample data.')
    parser.add_argument(
        '-p',
        '--prediction',
        type=Path,
        default=Path(__file__).parents[2].joinpath(
            'sample-data/vehicles_FROM_07-08-2021_00-'
            '00_TO_07-08-2021_23-59_anonymized.csv'),
        help='path to the prediction csv file. Defaults to sample data.')
    args = parser.parse_args()

    table_pred = pd.read_csv(args.prediction.resolve(), dtype='str')
    table_gt = pd.read_csv(args.groundtruth.resolve(), dtype='str')

    logo_img = Path(__file__).resolve().parents[2].joinpath(
        'Invision_Logo.png')

    configs = Path(args.configs).resolve()
    assert configs.is_file(), f'Configuration file {configs} does not exist.'

    generate_performance_report(table_pred, table_gt, args.identifier, configs,
                                logo_img)
