#!/usr/bin/env python3
HELP = """Traffic_Statics_Report.py
There are 5 steps to using this script:
1. Clone [Invision's Public Repository](https://gitlab.com/invisionai-public/vod-systems-performance-assessment.git).
2. You can import `generate_traffic_statistics_report` to run everything from another script.
5. A script is given at the end of this file that can be run with 
`./traffic_stats_report.py -i [report-identifier] -p [prediction-file] -t [timezone]`

This script will run through all code, analyze and visualize the data and produce a pdf Traffic Statistics report for you.
You can find the automatically produced report, `Report_MM-DD-YYYY_[report-identifier].pdf` in the created `Results` folder.
"""

import argparse
import io
from pathlib import Path
from typing import Tuple, Optional

from fpdf import FPDF
import pandas as pd

from vod_analytics.configs import TrafficStatsReportConfigs, load_yaml_to_dict
from vod_analytics.pdf import imgFromMemoryInPDF, addHeader
from vod_analytics.plotting import (plotTimeHistoryCounts, plotPieChart,
                                    plotTimeHistoryStackedBars)


def generate_traffic_statistics_report(
        traffic_data: pd.DataFrame,
        vod_system_tag: str,
        configs: TrafficStatsReportConfigs,
        logo_file_path: Optional[str] = None) -> Tuple[io.BytesIO, str]:
    # set up pdf
    pdf = FPDF()
    pdf.add_page()

    # validate and format inputs
    # requirements on the prediction file
    required_pred_cols = []
    required_pred_cols.extend(configs.stats_col_names.datetime)
    required_pred_cols.extend([
        configs.stats_col_names.front_passenger_count,
        configs.stats_col_names.rear_passenger_count
    ])
    numeric_preds = [
        configs.stats_col_names.front_passenger_count,
        configs.stats_col_names.rear_passenger_count
    ]

    # verify predictions CSV file format and return error pdf if missing any
    missing_cols = [
        col for col in required_pred_cols if col not in traffic_data.columns
    ]
    if len(missing_cols) > 0:
        pdf.set_font('Times', '', 12)
        pdf.cell(
            0,
            10,
            f'Missing {missing_cols} columns required to calculate traffic statistics.',
            new_x="LMARGIN",
            new_y="NEXT",
            align='C')
        pdf.cell(0,
                 10,
                 'There may not be any vehicle data in this time range.',
                 align='C')
        return io.BytesIO(
            pdf.output()), 'Traffic_Statistics_Missing_Columns.pdf'

    # format data
    if len(configs.stats_col_names.datetime) == 1:
        traffic_data['Datetime'] = pd.to_datetime(
            traffic_data[configs.stats_col_names.datetime[0]],
            format=configs.datetime_format)
    else:
        traffic_data['Datetime'] = pd.to_datetime(
            traffic_data[configs.stats_col_names.datetime].agg(' '.join,
                                                               axis=1),
            format=configs.datetime_format)
        traffic_data['Datetime'] = traffic_data['Datetime'].dt.tz_localize(
            configs.input_timezone)

    traffic_data['Datetime'] = traffic_data['Datetime'].dt.tz_convert(
        configs.output_timezone)
    traffic_data[numeric_preds] = traffic_data.loc[:, numeric_preds].astype(
        float)  # integers or nan, thus float
    traffic_data['total_passenger_count'] = (traffic_data[configs.stats_col_names.front_passenger_count] +\
                                             traffic_data[configs.stats_col_names.rear_passenger_count]).astype(float)

    # Report Generation Settings
    report_timespan = (
        f"{traffic_data['Datetime'].min().strftime('%m-%d-%Y_%H-%M')}"
        f"_to_{traffic_data['Datetime'].max().strftime('%m-%d-%Y_%H-%M')}")
    vod_system_tag = vod_system_tag

    pdf.set_font('Times', 'B', 20)
    logo_path = None
    if logo_file_path:
        logo_path = Path(logo_file_path).resolve()
    addHeader(pdf, f'{report_timespan}, {vod_system_tag}', logo_file_path)

    total_hours = ((traffic_data['Datetime'].max() -
                    traffic_data['Datetime'].min()).total_seconds() // 3600)
    # where 575000 is a tested max number of vehicle entries that can be analyzed within a one minute timeout.
    if len(traffic_data.index) > 100 and len(traffic_data.index) < 575000 and\
       total_hours > 6 and total_hours < 24 * 31:
        # get traffic_plot_hours based on the latest and earliest timestamps
        traffic_plot_hours = 24 if (
            (traffic_data['Datetime'].max() -
             traffic_data['Datetime'].min()).total_seconds() //
            3600) > 24 else 1
        timely_unit_title = "Hourly "
        if traffic_plot_hours > 24:
            timely_unit_title = "Daily "

        # Rush Hour Estimation
        traffic_flux = plotTimeHistoryCounts(traffic_data['Datetime'],
                                             traffic_plot_hours,
                                             'Traffic Flux',
                                             'Number of Vehicles',
                                             timely_unit_title == 'Hourly',
                                             configs.output_timezone)

        # Overall Vehicles Occupancy Distribution
        # get bucketed counts of occupants
        traffic_data['occu_class'] = traffic_data['total_passenger_count'].\
            where(cond=traffic_data['total_passenger_count'] > 0)
        occupancy_count = plotPieChart(traffic_data['occu_class'],
                                       "Vehicle Occupancy Counts")
        timely_occupancy_count = plotTimeHistoryStackedBars(
            traffic_data['Datetime'], traffic_data['occu_class'],
            traffic_plot_hours, timely_unit_title + "Vehicle Occupancy Counts",
            'Number of Vehicles')

        # Vehicle Class pie chart
        vehicle_class_exists = configs.stats_col_names.vehicle_class in traffic_data.columns
        if vehicle_class_exists:
            traffic_data[configs.stats_col_names.vehicle_class].replace(
                {'HEAVY TRUCK': 'TRUCK'},
                inplace=True)  # unify Heavy Truck and Truck
            traffic_data[configs.stats_col_names.vehicle_class].replace(
                {
                    'BUS': 'OTHER',
                    'UNKNOWN': 'OTHER',
                    'nan': 'OTHER'
                },
                inplace=True)  # unify Bus and Unknown into Other
            observed_classes = plotPieChart(
                traffic_data[configs.stats_col_names.vehicle_class],
                'Observed Vehicle Classes')
            timely_observed_classes = plotTimeHistoryStackedBars(
                traffic_data['Datetime'],
                traffic_data[configs.stats_col_names.vehicle_class],
                traffic_plot_hours,
                timely_unit_title + "Observed Vehicle Classes",
                'Number of Vehicles')
        lpr_data_exists = configs.stats_col_names.license_plate in traffic_data.columns
        if lpr_data_exists:
            # LPR reading class pie chart
            unread = pd.Series(['NOPLATE', 'NOTREAD'])
            traffic_data['lpr_class'] = traffic_data[configs.stats_col_names.license_plate].\
                where(cond=traffic_data[configs.stats_col_names.license_plate].isin(unread), other='READ')
            lpr_class = plotPieChart(traffic_data['lpr_class'],
                                     'LPR Read Status')
            timely_lpr_class = plotTimeHistoryStackedBars(
                traffic_data['Datetime'], traffic_data['lpr_class'],
                traffic_plot_hours, timely_unit_title + "LPR Read Status",
                'Number of Vehicles')

        # Start building the pdf
        # traffic stats plots
        pdf.cell(0, 10, 'Traffic Statistics', align='C')
        imgFromMemoryInPDF(traffic_flux, pdf, x=30, y=35, h=60)
        imgFromMemoryInPDF(timely_occupancy_count, pdf, x=30, y=105, h=80)
        imgFromMemoryInPDF(occupancy_count, pdf, x=80, y=195, h=60)

        pdf.add_page()
        addHeader(pdf, f'{report_timespan}, {vod_system_tag}', logo_file_path)

        if vehicle_class_exists:
            imgFromMemoryInPDF(timely_observed_classes, pdf, x=30, y=35, h=80)
            imgFromMemoryInPDF(observed_classes, pdf, x=80, y=125, h=60)
        if lpr_data_exists:
            pdf.add_page()
            addHeader(pdf, f'{report_timespan}, {vod_system_tag}',
                      logo_file_path)
            imgFromMemoryInPDF(timely_lpr_class, pdf, x=30, y=35, h=80)
            imgFromMemoryInPDF(lpr_class, pdf, x=80, y=125, h=60)
    else:
        pdf.set_font('Times', 'I', 12)
        pdf.cell(
            0,
            10,
            'Need at least 100 vehicle entries to create a meaningful traffic statistics report.',
            new_x="LMARGIN",
            new_y="NEXT",
            align='C')
        pdf.cell(
            0,
            10,
            f'Currently have {len(traffic_data.index)} for timespan {report_timespan}',
            align='C')

    return io.BytesIO(pdf.output(
    )), f'Traffic_Statistics_{report_timespan}_{vod_system_tag}.pdf'


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument(
        '-i',
        '--identifier',
        type=str,
        default=None,
        help='str to help identify the specific generated report ie the date.')
    parser.add_argument(
        '-c',
        '--configs',
        type=str,
        default='traffic_stats_configs.yml',
        help=
        ('path to the configs.yml file. Defaults to ./traffic_stats_configs.yml'
         ))
    parser.add_argument(
        '-p',
        '--prediction',
        type=str,
        default=('../../sample-data/vehicles_FROM_07-08-2021_00-'
                 '00_TO_07-08-2021_23-59_anonymized.csv'),
        help='path to the prediction csv file. Defaults to sample data.')
    args = parser.parse_args()

    table_pred = pd.read_csv(Path(args.prediction).resolve(), dtype='str')

    configs_path = Path(args.configs).resolve()
    assert configs_path.is_file(
    ), f'Configuration file {configs_path} does not exist.'
    configs = load_yaml_to_dict(TrafficStatsReportConfigs, configs_path)

    results_path = Path(__file__).parent.parent.parent.joinpath('Results')
    results_path.mkdir(parents=True, exist_ok=True)

    logo_img = Path(__file__).resolve().parents[2].joinpath(
        'Invision_Logo.png')

    print('Generating Traffic Statistics Report.')
    report_bytes, report_name = generate_traffic_statistics_report(
        table_pred, args.identifier, configs, logo_img)
    with open(Path.cwd().joinpath('Results', report_name), 'wb') as f:
        f.write(report_bytes.read())

    print(f'{report_name} generated.')
