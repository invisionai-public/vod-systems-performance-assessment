HELP = """ audits the sanity of ground truth CSV files by checking the validity of the count values
(allowability of counts and their consistency with the respective  chosen countabilities). Invalid
rows of the CSV files are written to new CSV files with a '_invalid_count_contability' suffix.

Example:
python check_audits_validity.py ../../sample-data/audit_annotator.csv
"""

import argparse
from pathlib import Path
from typing import Optional, List
from vod_analytics.audit_utils import Headers, Labels, read_gt 


def write_invalid_count_countability_to_csv(csv_gt: Path, csv_invalid: Optional[Path] = None
                                            ) -> None:
    """ checks the validity (allowability of count and its consistency with the chosen countability)
    of a groundtruth CSV file and writes the invalid rows to a CSV file. """
    
    table_gt = read_gt(csv_gt)
    invalid_indices = table_gt[[Headers.gt_count, Headers.gt_countability]].apply(
                                               lambda x: not Labels.is_label_valid(*x), axis=1)
    table_invalid = table_gt.loc[invalid_indices, :]
    allowed = Labels.allowed_count_countability
    feedback_col = 'inconsistency_explained'
    table_invalid[feedback_col] = table_invalid[Headers.gt_count].apply(
        lambda c: f"Only countabilities {' & '.join(allowed[c])} are allowed when count = {c}.")
    
    if csv_invalid is None:
        csv_invalid = csv_gt.parent.joinpath(f'{csv_gt.stem}_invalid_count_countability.csv')
        print(f'Writing invalid rows in {csv_invalid}.')
    table_invalid.to_csv(csv_invalid, index=False, columns=[*Headers.gt_headers, feedback_col])
 

def main(args: Optional[List[str]] = None) -> None:
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument('CSVs', type=Path, nargs='+')
    args = parser.parse_args(args)
                        
    for csv in args.CSVs:
        invalid_rows = write_invalid_count_countability_to_csv(csv)


if __name__ == "__main__":
    main()        