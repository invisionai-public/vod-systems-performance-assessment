from enum import Enum
from functools import wraps
import logging
from typing import Tuple, Callable

from distance import levenshtein
import numpy as np
import pandas as pd


def print_matched_and_unmatched_len(func: Callable) -> Callable:

    @wraps(func)
    def wrapper(*args, **kwargs) -> Tuple[pd.DataFrame, pd.DataFrame]:
        table_matched, table_unmatched = func(*args, **kwargs)
        logging.info(
            f"{len(table_matched)} groundtruth and prediction cases were matched."
        )
        logging.warning(
            f"{len(table_unmatched)} groundtruth cases were not matched.")
        return table_matched, table_unmatched

    return wrapper


def _match_pred_and_gt_using_str(
    table_pred: pd.DataFrame,
    table_gt: pd.DataFrame,
    timediff_tol: int,
    OCR_distance_acceptable: int,
    ts_column_names: Tuple[str, str] = ('Datetime_pred', 'Datetime_gt'),
    str_column_names: Tuple[str, str] = ('LPR_pred', 'LPR_gt')
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """ Tries to match each testcase in table_gt to an entry in table_pred using exact str matching
        and approximate timestamp matching (within timediff_tol). Note that approximate plate matching
        within an OCR Levenshtein distance is used to create association suggestions in the unmatched cases."""
    # keep a copy of old table's dtypes so we can restore them
    old_dtypes = {**dict(table_pred.dtypes), **dict(table_gt.dtypes)}

    # add column names to table
    table_matched = pd.DataFrame(
        columns=[*list(table_gt.columns), *list(table_pred.columns)])
    table_unmatched = pd.DataFrame(
        columns=[*list(table_gt.columns), *list(table_pred.columns)])
    ts_col_pred, ts_col_gt = ts_column_names
    str_col_pred, str_col_gt = str_column_names
    for _, testcase_gt in table_gt.iterrows():
        # get entries where difference between pred and gt Datetimes is within the time diff tolerance
        is_near_t_gt = (table_pred[ts_col_pred] -
                        testcase_gt[ts_col_gt]).dt.total_seconds().between(
                            -timediff_tol, timediff_tol)
        indices_near_t_gt = is_near_t_gt[is_near_t_gt].index
        candidates_match = table_pred.loc[indices_near_t_gt, :]
        # write OCR error and time_diff to candidates_match
        candidates_match['OCR_errors'] = [
            levenshtein(testcase_gt[str_col_gt], row[str_col_pred])
            for _, row in candidates_match.iterrows()
        ]
        candidates_match['time_diff'] = np.abs(
            (candidates_match[ts_col_pred] -
             testcase_gt[ts_col_gt]).dt.total_seconds())
        # sort so that most likely match is first if no exact match found
        sorted_matches = candidates_match.sort_values(
            by=['OCR_errors', 'time_diff'])
        if sorted_matches.empty:
            table_unmatched = pd.concat(
                [table_unmatched, pd.DataFrame(testcase_gt)],
                ignore_index=True)
            continue

        OCR_distance = sorted_matches.iloc[0].OCR_errors
        sorted_matches.drop(['time_diff', 'OCR_errors'], axis=1, inplace=True)
        # set up matched groundtruth and prediction data for table entry with most likely/exact match
        joined_table_data = pd.concat(
            [testcase_gt, sorted_matches.iloc[0]],
            axis=0)  # TODO: head is not returning the head only! (only a view)
        # convert to a DataFrame row
        joined_table_data = pd.DataFrame([joined_table_data.tolist()],
                                         columns=joined_table_data.index)
        # if the difference between strings is 0, gt and pred are added to table_matched
        if OCR_distance == 0:
            table_matched = pd.concat([table_matched, joined_table_data],
                                      ignore_index=True)
        # if the difference is within the tolerance, add the gt and pred data that is the most likely match
        elif OCR_distance <= OCR_distance_acceptable:
            table_unmatched = pd.concat([table_unmatched, joined_table_data],
                                        ignore_index=True)
        # otherwise, there is no match so only gt values are added to table_unmatched
        else:
            table_unmatched = pd.concat(
                [table_unmatched, testcase_gt.to_frame().T], ignore_index=True)

    # restore the original dtypes (they get lost when creating the matched/unmatched tables)
    table_matched = table_matched.astype({
        key: value
        for key, value in old_dtypes.items() if key in table_matched.columns
    })
    table_unmatched = table_unmatched.astype({
        key: value
        for key, value in old_dtypes.items() if key in table_unmatched.columns
    })

    return table_matched, table_unmatched


def _match_pred_and_gt_using_timestamp(
    table_pred: pd.DataFrame,
    table_gt: pd.DataFrame,
    timediff_tol: int,
    ts_column_names: Tuple[str, str] = ('Datetime_pred', 'Datetime_gt')
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """ Tries to match each testcase in table_gt to an entry in table_pred using approximate timestamp matching
        (within timediff_tol).
    """
    # keep a copy of old table's dtypes so we can restore them
    old_dtypes = {**dict(table_pred.dtypes), **dict(table_gt.dtypes)}
    # add column names
    table_matched = pd.DataFrame(
        columns=[*list(table_gt.columns), *list(table_pred.columns)])
    table_unmatched = pd.DataFrame(
        columns=[*list(table_gt.columns), *list(table_pred.columns)])
    ts_col_pred, ts_col_gt = ts_column_names
    for _, testcase_gt in table_gt.iterrows():
        # get entry where difference between pred and gt Datetimes is between the time diff tolerance
        is_near_t_gt = (table_pred[ts_col_pred] -
                        testcase_gt[ts_col_gt]).dt.total_seconds().between(
                            -timediff_tol, timediff_tol)
        indices_near_t_gt = is_near_t_gt[is_near_t_gt].index
        candidates_match = table_pred.loc[indices_near_t_gt, :]
        # write time_diff to candidates_match
        candidates_match['time_diff'] = np.abs(
            (candidates_match[ts_col_pred] -
             testcase_gt[ts_col_gt]).dt.total_seconds())
        # sort so that closest match if no exact match found is first
        sorted_matches = candidates_match.sort_values(by=['time_diff'])
        if sorted_matches.empty or abs(
                sorted_matches['time_diff'].iloc[0]) > timediff_tol:
            table_unmatched = pd.concat(
                [table_unmatched, testcase_gt.to_frame().T], ignore_index=True)
            # convert to datetime to be consistent with how this col is converted in Jupyter notebook formatting
            table_unmatched[ts_col_gt] = pd.to_datetime(
                table_unmatched[ts_col_gt])
            table_unmatched[ts_col_pred] = pd.to_datetime(
                table_unmatched[ts_col_pred])
        else:
            sorted_matches.drop(['time_diff'], axis=1, inplace=True)
            joined_table_data = pd.concat(
                [testcase_gt, sorted_matches.iloc[0]], axis=0
            )  # TODO: head is not returning the head only! (only a view)
            table_matched = pd.concat([
                table_matched,
                pd.DataFrame([joined_table_data.tolist()],
                             columns=joined_table_data.index)
            ],
                                      ignore_index=True)

    # restore the original dtypes (they get lost when creating the matched/unmatched tables)
    table_matched = table_matched.astype({
        key: value
        for key, value in old_dtypes.items() if key in table_matched.columns
    })
    table_unmatched = table_unmatched.astype({
        key: value
        for key, value in old_dtypes.items() if key in table_unmatched.columns
    })

    return table_matched, table_unmatched


def _match_pred_and_gt_using_id(
    table_pred: pd.DataFrame,
    table_gt: pd.DataFrame,
    ts_column_names: Tuple[str, str] = ('timestamp_pred', 'timestamp_gt'),
    id_column_names: Tuple[str, str] = ('vehicle_id_pred', 'vehicle_id_gt')
) -> Tuple[pd.DataFrame, pd.DataFrame]:
    """
    Given the formatted table_pred and table_gt, match the entries between tables using vehicle_id.
    Returns table_matched and table_unmatched
    with columns: ['vehicle_id_gt', 'Timestamp_gt', 'total_count_gt', 'countability_gt', 'vod_image_link_gt', 'hov_status_gt', 'readability_gt',
                   'vehicle_id_pred', 'Timestamp_pred', 'vehicle_class_pred', 'total_count_pred', 'total_count_confidence_pred',
                   'vod_image_link_pred', 'hov_status_pred']
    """
    return _match_pred_and_gt_using_str(table_pred,
                                        table_gt,
                                        timediff_tol=0,
                                        OCR_distance_acceptable=0,
                                        ts_column_names=ts_column_names,
                                        str_column_names=id_column_names)


class Match_Pred_and_Gt_With(Enum):
    STR = _match_pred_and_gt_using_str
    TIMESTAMP = _match_pred_and_gt_using_timestamp
    ID = _match_pred_and_gt_using_id
