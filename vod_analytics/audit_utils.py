# to be DEPRECATED (to be completely replaced by audit_helpers and auditing)

from enum import Enum
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path
from typing import Tuple, Dict

from fpdf import FPDF
from fpdf.enums import XPos, YPos
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from ipywidgets import interactive, SelectionSlider, SelectionRangeSlider
from IPython.display import display

from vod_analytics.plotting import plotTwoColumnTable, plotConfusionMatrix
from vod_analytics.pdf import addHeader, imgFromMemoryInPDF


class Headers:
    date, date_format = 'Date', "%Y-%m-%d"
    time, time_format = 'Time', "%H:%M:%S.%f"
    datetime = 'Datetime'

    gt_count = 'cerema_count'
    gt_countability = 'countability'
    gt_extras = ['Description', 'Link', 'User']
    gt_headers = [date, time, gt_count, gt_countability, *gt_extras]

    pred_count = 'Fused Count'
    pred_quality_score = 'Quality Score'
    pred_LOV_confidence = 'LOV Confidence'
    pred_headers = [date, time, pred_count, pred_quality_score, pred_LOV_confidence]

    HOV_State = 'HOV State'


class Labels:
    uncountables = ['unreadable', 'undeterminable']
    countables = ['min_viable', 'good', 'excellent']
    allowed_count_countability = {'0+': uncountables,
                                  '1': countables,
                                  '1+': uncountables,
                                  '2': countables,
                                  '2+': uncountables,
                                  '3': countables,
                                  '3+': uncountables,
                                  'more': countables + uncountables,
                                  'skip': ['skip']}
    allowed_count_countability = defaultdict(lambda: 'invalid_label', allowed_count_countability)
    allowed_counts = list(allowed_count_countability.keys())
    allowed_countabilities = [*uncountables, *countables, 'skip']  # ascending order then skip

    @staticmethod
    def is_label_valid(count: str, countability: str) -> bool:
        return countability in Labels.allowed_count_countability[count]

        
class HOV_State(Enum):
    LOV = 0
    LOV_Review = 1  # only for predictions
    HOV = 2
    LQ = 3  # Low Quality (thus unprovable or unverifiable state)


class EnforcementType(Enum):
    """ HOV2 and HOV3 Enforcement types and their associated relationship to occupancy counts """
    
    HOV2 = {
        '0+': HOV_State.LQ,
        '1': HOV_State.LOV,
        '1+': HOV_State.LQ,
        '2': HOV_State.HOV,
        '2+': HOV_State.HOV,
        '3': HOV_State.HOV,
        '3+': HOV_State.HOV,
        'more': HOV_State.HOV,
        'skip': None,
    }

    HOV3 = {
        '0+': HOV_State.LQ,
        '1': HOV_State.LOV,
        '1+': HOV_State.LQ,
        '2': HOV_State.LOV,
        '2+': HOV_State.LQ,
        '3': HOV_State.HOV,
        '3+': HOV_State.HOV,
        'more': HOV_State.HOV,
        'skip': None,
    }

    def count_to_enforcement(self, count: str) -> str:
        return 'skip' if count == 'skip' else self.value[count].name
    

def read_gt(gt_csv: Path) -> pd.DataFrame:
    df = pd.read_csv(gt_csv, dtype=str, usecols=Headers.gt_headers)
    add_datetime_col(df)
    df = df.sort_values(by=Headers.datetime, ascending=True)
    return df


def read_pred(pred_csv: Path) -> pd.DataFrame:
    df = pd.read_csv(pred_csv, dtype=str, usecols=Headers.pred_headers)
    add_datetime_col(df)
    df = df.sort_values(by=Headers.datetime, ascending=True)
    percentage_cols = [Headers.pred_quality_score, Headers.pred_LOV_confidence]
    for col in percentage_cols:
        df[col] = df[col].str.strip('%').astype(float) / 100.0
    df[Headers.pred_count] = df[Headers.pred_count].astype(float)
    return df


def add_datetime_col(table: pd.DataFrame) -> None:
    table[Headers.datetime] = pd.to_datetime(table[Headers.date] + '_' + table[Headers.time],
        format=Headers.date_format + '_' + Headers.time_format)


def add_enforcement_recommendation_col(
        df_pred: pd.DataFrame,
        confidence_threshold: float,
        quality_thresholds: Tuple[float, float]) -> None:
    """ adds an enforcement recommendation column to the dataframe in place"""

    assert 0 <= confidence_threshold <= 1, "Confidence threshold must be between 0 and 1."
    assert 0 <= quality_thresholds[0] <= quality_thresholds[1] <= 1, \
        "The two quality thresholds must be between 0 and 1, and the first one should be smaller."
    
    def assign_HOV_state(LOV_confidence: float, quality: float) -> str:
        
        if np.isnan(LOV_confidence) or np.isnan(quality):
            return 'skip'
        elif quality < quality_thresholds[0]:
            return HOV_State.LQ.name
        
        if LOV_confidence <= 0.5:
            return HOV_State.HOV.name
        elif LOV_confidence > confidence_threshold and quality > quality_thresholds[1]:
            return HOV_State.LOV.name
        else:
            return HOV_State.LOV_Review.name

    conf_quality_cols = [Headers.pred_LOV_confidence, Headers.pred_quality_score]
    df_pred[Headers.HOV_State] = df_pred[conf_quality_cols].apply(
        lambda x: assign_HOV_state(*x), axis=1)
    

def assert_tables_datetime_match(table_a: pd.DataFrame, table_b: pd.DataFrame) -> None:
    assert len(table_a) == len(table_b), \
        "Tables to be compared must have equal number of rows."
    assert (table_a[Headers.datetime] == table_b[Headers.datetime]).all(), \
        "Tables to be compared must have identical Date and Time cols."


@dataclass
class EnforcementPerformance:
    audit_table: pd.DataFrame  # df_pred with added gt columns, defining the audit dataset
    CM_array: np.ndarray
    cm_table: pd.DataFrame  # confusion matrix table
    metrics_table: pd.DataFrame
    wrong_pred_tables: Dict[str, pd.DataFrame]  # eg. wrong_pred_tables['LOV'] has all audit_table 
                                                #rows whose HOV_State is wrongly predicted to be LOV

    def save_to_folder(self, folder: Path) -> None:
        folder.mkdir(parents=True, exist_ok=True)
        for attr in ['audit_table', 'cm_table', 'metrics_table']:
            getattr(self, attr).to_csv(folder / f'{attr}.csv', index=False)   
        for error_label, df_error in self.wrong_pred_tables.items():
            df_error.to_csv(folder / f'wrong_{error_label}_predictions.csv', index=False)
    
    def print_cm_and_metrics(self) -> None:
        print("-" * 50)
        print(' Confusion Matrix:\n', self.cm_table)
        print("-" * 50)
        print(' Table of Metrics:\n', self.metrics_table)
        print("-" * 50)


def assess_enforcement_classification(
        df_gt: pd.DataFrame,
        df_pred: pd.DataFrame,
        confidence_threshold: float,
        quality_thresholds: Tuple[float, float],
        enforcement_type: EnforcementType) -> EnforcementPerformance:    
    """ assess the enforcement success as a multiclass classification problem. Like any classificat-
    ion problem, the formulation is described by a confusion matrix (CM). Here CM is a 5-by-5 matrix
    with truth column and prediction rows with 5 classes (LOV, LOV_review, HOV, LQ, skip). We
    also prune CM down into a smaller 4-by-3 matrix cm by removing the all-zero LOV_review truth
    column and the skip row and column."""
    
    assert_tables_datetime_match(df_gt, df_pred)
    add_enforcement_recommendation_col(df_pred, confidence_threshold, quality_thresholds)
    df_gt[Headers.HOV_State] = df_gt[Headers.gt_count].apply(enforcement_type.count_to_enforcement)
    labels = [e.name for e in HOV_State]
    labels.append('skip')
    CM = confusion_matrix(df_gt[Headers.HOV_State], df_pred[Headers.HOV_State], labels=labels).T
    cm = np.empty((4, 3), dtype=int)
    cm = CM[0:4, [0, 2, 3]] 
    cm_table = pd.DataFrame({'True LOV': cm[:, 0], 'True HOV': cm[:, 1], 'True LQ': cm[:, 2]},
                            index = ['Pred LOV_auto', 'Pred LOV_rev', 'Pred HOV', 'Pred LQ'])
    
    metrics = {
        'Auto-enforce Bucket apparent FPR': f'{cm[0, 1]/ cm[:, 1:].sum() * 100:.1f}%',
        'Auto-enforce Bucket hidden FPR': f'{cm[0, 2]/ cm[:, 1:].sum() * 100:.1f}%',
        'Auto-enforce Bucket Violation Coverage': f'{cm[0, 0] / cm[:, 0].sum() * 100:.1f}%',
        'LOV Review Bucket: Precision (Review ROI)': f'{cm[1, 0] / cm[1, :].sum() * 100:.1f}%',
        'Overall missrate': f'{cm[2:, 0].sum() /cm[:, 0].sum() * 100:.1f}%',
        'Quality TPR':  f'{cm[3, 2] / cm[:, 2].sum() * 100:.1f}%',
    }
    metrics_table = pd.DataFrame(metrics, index = ['value']).transpose()

    wrong_predictions = {}  # a dict or wrong prediction tables
    gt_cols_to_add = [Headers.gt_count, Headers.gt_countability, *Headers.gt_extras,
                      Headers.HOV_State]
    for col in gt_cols_to_add:
        df_pred[col + '_gt'] = df_gt[col]
    
    nonskipped_gt_mask = df_gt[Headers.HOV_State] != 'skip'
    for label in labels:
        error_mask = np.logical_and(df_pred[Headers.HOV_State] == label,
            df_gt[Headers.HOV_State] != label.strip('_Review'))
        wrong_predictions[label] = df_pred[np.logical_and(error_mask, nonskipped_gt_mask)]
    return EnforcementPerformance(df_pred, CM, cm_table, metrics_table, wrong_predictions)


class ThresholdsTuningApp:
    """ Interactive app with two sliders for tuning the confidence threshold and the two quality
    thresholds to achieve desired audit performance metrics. The app gets initialized with ground-
    truth and prediction data."""

    # slider for tuning LOV confidence in 50% to 100% interval (progressively higher resolution)
    confidence_selections = [float(c) for c in range(50, 90, 2)]
    confidence_selections.extend([float(c) for c in range(90, 95, 1)])
    confidence_selections.extend([c / 10.0 for c in range(950, 1001)])
    confidence_sldr = SelectionSlider(options=confidence_selections, value=97.0, 
        description='confidence %', continuous_update=False, readout=True)

    # slider for tuning low/high qualities in 0 to 100% interval (progressively higher resolution)
    quality_selections = [float(c) for c in range(0, 50, 5)]
    quality_selections.extend(confidence_selections)
    quality_sldr = SelectionRangeSlider(options=quality_selections, value=(60., 98.5,),
        description='quality %', continuous_update=False, readout=True)

    def __init__(self, df_gt: pd.DataFrame, df_pred: pd.DataFrame) -> None:
        def func_to_interactive(confidence_thr_percentage, quality_thr_percentages):
            results = assess_enforcement_classification(df_gt=df_gt, df_pred=df_pred, 
                confidence_threshold=confidence_thr_percentage / 100.0,
                quality_thresholds=tuple(thr / 100.0 for thr in quality_thr_percentages),
                enforcement_type=EnforcementType.HOV3)
            display(results.metrics_table)
            return results
        self.sliders_app = interactive(func_to_interactive,
            confidence_thr_percentage=ThresholdsTuningApp.confidence_sldr,
            quality_thr_percentages=ThresholdsTuningApp.quality_sldr)
    
    def display(self) -> None:
        display(self.sliders_app)

    @property
    def confidence_threshold(self) -> float:
        return self.sliders_app.kwargs['confidence_thr_percentage'] / 100.0
    
    @property
    def quality_thresholds(self) -> Tuple[float, float]:   
        return tuple([thr / 100.0 for thr in self.sliders_app.kwargs['quality_thr_percentages']])
    
    @property
    def result(self) -> EnforcementPerformance:
        return self.sliders_app.result


def _add_wrong_preds_table(pdf: FPDF, wrong_preds_df: pd.DataFrame, title: str,
        printed_entries: int=0) -> int:
    """ sub-utility for generate_audit_report_pdf """
    
    # add title
    pdf.set_font('helvetica', 'B', size=10)
    pdf.cell(190, 10, title, align='C', new_x=XPos.LMARGIN, new_y=YPos.NEXT)
    
    # print the table
    pdf.set_font('helvetica', '', size=8)
    pdf.cell(12, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
    if len(wrong_preds_df.index) == 0:
        pdf.cell(80, 6, txt='There is no prediction error of this type.',
                 new_x=XPos.LMARGIN, new_y=YPos.NEXT)
        printed_entries += 1
        return printed_entries
    
    # table headers
    pdf.cell(55, 6, txt='GT HOV Status', new_x=XPos.RIGHT, new_y=YPos.TOP, border=1, align='C')
    pdf.cell(40, 6, txt='Predicted LOV Confidence', new_x=XPos.RIGHT, new_y=YPos.TOP, border=1,
        align='C')
    pdf.cell(40, 6, txt='Predicted Quality', new_x=XPos.RIGHT, new_y=YPos.TOP, border=1, align='C')
    pdf.cell(30, 6, txt='Vehicle Image Link', new_x=XPos.LMARGIN, new_y=YPos.NEXT, border=1,
        align='C')
    
    # add the rows
    for _, df in wrong_preds_df.iterrows():
        printed_entries += 1
        pdf.cell(12, 6, txt='', new_x=XPos.RIGHT, new_y=YPos.TOP)
        pdf.cell(55, 6, txt=str(df['HOV State_gt']), new_x=XPos.RIGHT, new_y=YPos.TOP, border=1,
            align='C')
        pdf.cell(40, 6, txt=f"{df['LOV Confidence']*100:.1f}%", new_x=XPos.RIGHT, new_y=YPos.TOP,
            border=1, align='C')
        pdf.cell(40, 6, txt=f"{df['Quality Score']*100:.1f}%", new_x=XPos.RIGHT, new_y=YPos.TOP,
            border=1, align='C')

        if str(df['Link_gt']) == 'nan' or str(df['Link_gt']) == 'None' \
            or str(df['Link_gt']) == "":
            pdf.cell(30, 6, txt='N/A', new_x=XPos.LMARGIN, new_y=YPos.NEXT, border=1, align='C')
        else:
            pdf.set_text_color(0, 0, 255)
            pdf.set_font('helvetica', 'U', size=8)
            pdf.cell(30, 6, txt='Link', link=str(df['Link_gt']), new_x=XPos.LMARGIN,
                new_y=YPos.NEXT, border=1, align='C')

        pdf.set_text_color(0, 0, 0)
        pdf.set_font('helvetica', '', size=8)
    return printed_entries


def generate_audit_report_pdf(
        audit_result: EnforcementPerformance,
        conf_thr: float, 
        quality_thrs: Tuple[float, float],
        path_to_logo: Path,
        output_folder: Path) -> None:

    plt.ioff()
    # metrics table
    col_labels = ("Metrics", "Values")
    row_labels = list(audit_result.metrics_table.index.values)
    metrics_fig = plotTwoColumnTable(row_labels, audit_result.metrics_table['value'].tolist(),
        'Metrics Summary', col_labels, (0.7, 0.3), (16 / 2.54, 4 / 2.54))
    
    # confusion matrix
    x_classes = list(audit_result.cm_table.index.values)
    y_classes = list(audit_result.cm_table.columns)
    cm_fig = plotConfusionMatrix(audit_result.cm_table, "Confusion Matrix", x_classes, y_classes)

    # first page
    pdf = FPDF()
    pdf.add_page()
    report_tag = "Audit"
    addHeader(pdf, report_tag, path_to_logo)
    pdf.set_text_color(0, 0, 0)
    pdf.set_font('helvetica', '', size=10)
    pdf.cell(190, 5, f"Confidence threshold: {conf_thr * 100.}%",
        align='L', new_x=XPos.LMARGIN, new_y=YPos.NEXT)
    qual_thrs_str = f"{quality_thrs[0] * 100.}%, {quality_thrs[1] * 100.}%"
    pdf.cell(190, 5, f"Quality thresholds: {qual_thrs_str}", align='L', new_x=XPos.LMARGIN,
             new_y=YPos.NEXT)
    
    start = audit_result.audit_table.loc[0, Headers.datetime].strftime("%Y-%m-%d %H:%M")
    end = audit_result.audit_table.loc[audit_result.audit_table.index[-1],
        Headers.datetime].strftime("%Y-%m-%d %H:%M")
    pdf.cell(190, 5, f"Data: {len(audit_result.audit_table)} vehicles from {start} to {end}",
        align='L', new_x=XPos.LMARGIN, new_y=YPos.NEXT)
    imgFromMemoryInPDF(metrics_fig, pdf, x=35, y=45, h=40)
    imgFromMemoryInPDF(cm_fig, pdf, x=50, y=95, h=110)

    pdf.add_page()
    addHeader(pdf, report_tag, path_to_logo)

    # generate wrong predictions tables in pdf
    relevant_cols = ['HOV State_gt', 'LOV Confidence', 'Quality Score', 'Link_gt']
    printed_entries = 0
    for label, df in audit_result.wrong_pred_tables.items():
        printed_entries = _add_wrong_preds_table(pdf, df[relevant_cols],
                            f"Wrong {label} Predictions", printed_entries)
    
    output_folder.mkdir(parents=True, exist_ok=True)
    pdf.output(output_folder.joinpath('audit_report.pdf'))