from dataclasses import dataclass, is_dataclass
from pathlib import Path
import sys
from typing import Type, Any, List, Optional, Dict
from enum import Enum

import dacite
import yaml


def load_yaml_to_dict(dataclass_type: Type, config_file_path: Path) -> Any:
    assert is_dataclass(
        dataclass_type), "dataclass_type is not a dataclass object"
    with open(config_file_path, "r") as stream:
        try:
            return dacite.from_dict(
                data_class=dataclass_type,
                data=yaml.safe_load(stream),
                config=dacite.Config(
                    type_hooks={
                        HovDecision: HovDecision,
                        SanityCheckBehaviour: SanityCheckBehaviour
                    }),
            )
        except yaml.YAMLError as exc:
            sys.exit(f"Error loading yaml as dict: {exc}")


@dataclass
class PredictionColumnNames:
    date: str
    time: str
    front_passenger_count: str
    rear_passenger_count: str
    total_passenger_count: str
    total_passenger_confidence: str
    vehicle_image_link: str
    license_plate: Optional[str] = None
    license_plate_image_link: Optional[str] = None


@dataclass
class PredictionParams:
    prediction_col_names: PredictionColumnNames
    datetime_format: str


@dataclass
class GroundtruthColumnNames:
    date: str
    time: str
    front_passenger_count: str
    rear_passenger_count: str
    third_row_passenger_count: Optional[str] = None
    license_plate: Optional[str] = None


@dataclass
class GroundtruthParams:
    groundtruth_col_names: GroundtruthColumnNames
    datetime_format: str
    unreadable_gt_counts: List[str]
    allowed_frontcounts: List[str]
    allowed_rearcounts: List[str]
    gt_counts_validation: str

    def __post_init__(self):
        self.allowed_frontcounts.extend(self.unreadable_gt_counts)
        self.allowed_rearcounts.extend(self.unreadable_gt_counts)


@dataclass
class MatchingParams:
    timediff_tol: int
    ocr_max_dist: int


@dataclass
class ReportGenerationParams:
    traffic_plot_hours: int
    report_timespan: str
    vod_system_tag: str
    metrics_summary_style: str
    lpr_trigger_table_max_entries: int
    occu_misclass_table_max_entries: int
    gt_has_3rd_rowcount: bool
    plot_pred_confidences_hist: bool
    plot_error_prob_vs_confidence: bool


@dataclass
class VODPerformanceReportConfigs:
    """Parameters required to properly run all the VOD Performance analyses. """
    system_includes_lpr: bool

    prediction_params: PredictionParams
    groundtruth_params: GroundtruthParams

    matching_gt_pred_params: MatchingParams

    count_thresholds: List[int]

    report_gen_params: ReportGenerationParams


@dataclass
class StatisticsColNames:
    datetime: List[str]
    front_passenger_count: str
    rear_passenger_count: str
    vehicle_class: Optional[str] = None
    license_plate: Optional[str] = None


@dataclass
class TrafficStatsReportConfigs:
    """Parameters required to properly run all the Traffic Statistics. """
    input_timezone: str
    output_timezone: str
    datetime_format: str
    stats_col_names: StatisticsColNames


# vod_performance_report_2023 configs
class HovDecision(str, Enum):
    HOV = 'hov'
    LOV = 'lov'
    UNKNOWN = 'unknown'


@dataclass
class CountLabel:
    """
    Members:
        as_int: the integer value represented by the string (ie 2+ becomes 2, or more becomes 4)
        is_countable: set of bool to sanity check if this count label must correspond with countability label
                      if set to true, this label must correspond with countable (true) countability labels
                      if set to false, this label must correspond with not countable (false) countability labels
                      if set to [true, false], this label can correspond with any countability labels
        hov_decision: if this count should be considered one of LOV, HOV, or UNKNOWN
    """
    as_int: int
    is_countable: List[bool]
    hov_decision: HovDecision

    def __setattr__(self, name, value):
        """enforce unique bool values in is_countable list"""
        if name == 'is_countable':
            value = list(set(value))
        super().__setattr__(name, value)


@dataclass
class PredictionColumnNames2023:
    vehicle_id: str
    timestamp: str
    total_count: str
    total_confidence: str
    vehicle_class: str
    vehicle_image_link: str


@dataclass
class PredictionParams2023:
    prediction_col_names: PredictionColumnNames2023
    datetime_format: str


@dataclass
class GroundtruthColumnNames2023:
    vehicle_id: str
    timestamp: str
    total_count: str
    countability: str
    vehicle_image_link: str
    user: str


class SanityCheckBehaviour(str, Enum):
    REMOVE = 'remove'  # print a warning and remove the offending rows
    THROW = 'throw'  # throw and exception and crash the script


@dataclass
class GroundtruthParams2023:
    groundtruth_col_names: GroundtruthColumnNames2023
    datetime_format: str
    sanity_check_behaviour: SanityCheckBehaviour
    allowed_counts: Dict[str, CountLabel]
    allowed_countability: Dict[str, bool]
    skip_column_name_and_value: Dict[str, str]


@dataclass
class VODPerformanceReportConfigs2023:
    """Parameters required to properly run all the VOD Performance 2023 analyses. """
    prediction_params: PredictionParams2023
    groundtruth_params: GroundtruthParams2023

    count_threshold: int
    pdf_filename: str
