from enum import IntEnum
import logging
import numbers
from typing import Dict, Optional, Tuple

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

from vod_analytics.data_cleaning import removeRowsWithNanColValues


def reportWindowDetectionAccuracy(table_matched: pd.DataFrame,
                                  window_row: str) -> float:
    window_exists_gt = np.isfinite(table_matched[window_row +
                                                 'count_gt'].astype(float))
    window_exists_pred = np.isfinite(table_matched[window_row +
                                                   'count_pred'].astype(float))
    accuracy_window_det = accuracy_score(window_exists_gt, window_exists_pred)
    print(
        f'The {window_row} window detection accuracy is: {accuracy_window_det*100:.1f}%'
    )
    return accuracy_window_det


def reportOccupancyMetrics(count_truth: pd.Series,
                           count_pred: pd.Series,
                           labels: list = [1, 2, 3],
                           title: str = 'total count') -> Dict[str, float]:
    # prepare for 3-class classification
    assert all([isinstance(L, numbers.Number)
                for L in labels]), "Labels must contain only numeric values."
    assert len(labels) == 3, "Must have exactly 3 labels"
    labels = np.sort(labels)
    count_truth = np.clip(count_truth.to_numpy(), 0, labels[-1])
    count_pred = np.clip(count_pred.to_numpy(), 0, labels[-1])

    # confusion matrix (CM)
    CM = confusion_matrix(count_truth, count_pred,
                          labels=labels).T  # transpose to have column truths

    def printConfusion(CM: np.ndarray):
        blank = ' ' * 14
        print(f'The iVOD {title} confusion matrix for {labels}-classes is:\n')
        print(blank + 'True Occupancy')
        print(blank + f'  {labels[0]}    {labels[1]}    {labels[2]}+\n' +
              blank + '-' * 15)
        print(
            f'Pred Occ.  {labels[0]} | {CM[0, 0]},  {CM[0, 1]},   {CM[0, 2]}')
        print(
            f'           {labels[1]} | {CM[1, 0]},   {CM[1, 1]},   {CM[1, 2]}')
        print(
            f'           {labels[2]}+| {CM[2, 0]},   {CM[2, 1]},   {CM[2, 2]}\n'
        )

    # metrics
    accuracy = np.sum(count_pred == count_truth) / len(count_truth)
    undercounting_percent = np.sum(count_pred < count_truth) / len(count_truth)
    overcounting_percent = np.sum(count_pred > count_truth) / len(count_truth)

    # print summary of occupancy counting performance metrics
    printConfusion(CM)
    print(f'The iVOD {title} accuracy is: {100*accuracy:.1f}%')
    print(f'The iVOD yields {100*undercounting_percent:.1f}% and'
          f'{100*overcounting_percent:.1f}% of under- and over-counting.')
    print(
        f'\nDetailed precision/recall/f1 scores for the {labels} {title} classes:\n'
    )
    print(f'truth {len(count_truth)} pred: {len(count_pred)}')
    print(classification_report(count_truth, count_pred, digits=3))
    print('-' * 75)

    # pack output
    return {
        'accuracy': accuracy,
        'overcounting': overcounting_percent,
        'undercounting': undercounting_percent,
        'CM': CM
    }


# Create ROC curves showing the curve if we minimize False Positives vs False Negatives
# for the binary HOV decision for occupancy thresholds of 2+ and 3+
# HO = false (no ticket), LO = true (ticket)
# convert count + probability to HO / LO decisions
class HOVStatus(IntEnum):
    HO = 0
    LO = 1


def binarizeDecision(counts: np.ndarray, probabilities: np.ndarray,
                     prob_threshold: float, count_threshold: int,
                     decision_mode: str) -> np.ndarray:
    pred_multiplier = {
        'minimize_fps':
        lambda counts, probabilities: np.logical_and(
            counts < count_threshold, probabilities >= prob_threshold),
        'minimize_fns':
        lambda counts, probabilities: np.logical_or(
            counts < count_threshold, probabilities < prob_threshold),
        'count_only':
        lambda counts, _: np.array(counts < count_threshold, dtype=bool)
    }

    assert len(counts) == len(probabilities)
    return HOVStatus.LO * np.ones_like(
        counts) * pred_multiplier[decision_mode](counts, probabilities)


class CountType(IntEnum):
    FRONT_REAR_COUNTS = 0
    TOTAL_COUNT = 1


def reportCountsTprAndFpr(
    count_type: CountType,
    table_matched_noNan: pd.DataFrame,
    count_threshold: int,
    prob_thresholds: Optional[np.ndarray] = None,
    total_count_col_names: Tuple[str,
                                 str] = ("total_count_pred", "total_count_gt")) -> Dict[str, list]:
    assert count_type in iter(
        CountType), f"{count_type} Count_Type is not supported."

    if prob_thresholds is None:
        prob_thresholds = np.arange(0.0, 1.0, 0.01)

    total_count_col_pred, total_count_col_gt = total_count_col_names
    assert total_count_col_pred and total_count_col_gt, "total_count_col_names cannot contain empty strings."

    # get prediction confidences
    prediction_count = table_matched_noNan[total_count_col_pred].to_numpy(
    ).astype(int)

    if table_matched_noNan['total_count_confidence_pred'].dtype != np.float64:
        table_matched_noNan[
            'total_count_confidence_pred'] = table_matched_noNan[
                'total_count_confidence_pred'].str.replace('%',
                                                           '').astype(float)
    prediction_confidences = table_matched_noNan[
        'total_count_confidence_pred'] / 100

    decision_types = ['minimize_fps', 'minimize_fns', 'count_only']
    number_of_cases = len(decision_types)

    tpr_allcases = [[] for i in range(number_of_cases)]
    fpr_allcases = [[] for i in range(number_of_cases)]
    cm_allcases = [[] for i in range(number_of_cases)]
    # get groundtruth values
    if count_type == CountType.FRONT_REAR_COUNTS:
        gt_vals = (table_matched_noNan['frontcount_gt'] +
                   table_matched_noNan['rearcount_gt'] <
                   count_threshold).astype(int)
    else:
        gt_vals = (table_matched_noNan[total_count_col_gt].astype(int) <
                   count_threshold).astype(int)

    divide_safely = lambda num, den: num / den if den != 0 else np.nan
    for th in prob_thresholds:
        # we have a couple different ways to binarize our HOV decision so we test all of them
        for idx, decision_type in enumerate(decision_types):
            preds = binarizeDecision(prediction_count, prediction_confidences,
                                     th, count_threshold, decision_type)
            cm_allcases[idx] = confusion_matrix(gt_vals, preds)
            tn, fp, fn, tp = cm_allcases[idx].ravel()
            tpr_allcases[idx].append(divide_safely(tp, tp + fn))
            fpr_allcases[idx].append(divide_safely(fp, fp + tn))

    return {
        'tpr_allcases': tpr_allcases,
        'fpr_allcases': fpr_allcases,
        'decision_types': decision_types,
        'tpr_count_only': tpr_allcases[2][0],
        'fpr_count_only': fpr_allcases[2][0]
    }


def _groupProbabilitiesByErrorTypeForFrontRearCounts(
    table_matched_readable: pd.DataFrame,
    table_matched_unreadable: pd.DataFrame,
    total_count_col_names: Tuple[str,
                                 str] = ("total_count_pred", "total_count_gt")) -> Dict[str, pd.Series]:
    total_count_col_pred, total_count_col_gt = total_count_col_names
    assert total_count_col_pred and total_count_col_gt, "total_count_col_names cannot contain empty strings."

    # remove rows with nan values for the specified rows
    table_matched_readable_filtered = removeRowsWithNanColValues(
        table_matched_readable,
        ['total_count_confidence_pred', 'rearcount_gt'])
    table_matched_unreadable_filtered = removeRowsWithNanColValues(
        table_matched_unreadable, ['total_count_confidence_pred'])

    # label each prediction as either correct or incorrect based on the corresponding groundtruth
    correctness_pred = pd.Series(np.where(
        (table_matched_readable_filtered['frontcount_gt']
         == table_matched_readable_filtered['frontcount_pred']) &
        (table_matched_readable_filtered['rearcount_gt']
         == table_matched_readable_filtered['rearcount_pred']), 'correct',
        'incorrect'),
                                 name='correctness')

    combined = pd.concat([
        table_matched_readable_filtered['total_count_confidence_pred'],
        correctness_pred
    ],
                         axis=1)

    # divide the incorrect into overcounting and undercounting errors
    conditions = [
        table_matched_readable_filtered[total_count_col_pred] <
        table_matched_readable_filtered['frontcount_gt'] +
        table_matched_readable_filtered['rearcount_gt'],
        table_matched_readable_filtered[total_count_col_pred] >
        table_matched_readable_filtered['frontcount_gt'] +
        table_matched_readable_filtered['rearcount_gt']
    ]
    choices = ["Undercounted", "Overcounted"]
    combined["error_types_pred"] = np.select(conditions,
                                             choices,
                                             default="Correct")
    if table_matched_unreadable_filtered[
            'total_count_confidence_pred'].dtype != np.float64:
        table_matched_unreadable_filtered[
            'total_count_confidence_pred'] = table_matched_unreadable_filtered[
                'total_count_confidence_pred'].str.replace('%',
                                                           '').astype(float)
    if combined['total_count_confidence_pred'].dtype != np.float64:
        combined['total_count_confidence_pred'] = combined[
            'total_count_confidence_pred'].str.replace('%', '').astype(float)
    return {
        'correct':
        combined['total_count_confidence_pred'].loc[
            combined.correctness == 'correct'].reset_index(drop=True),
        'incorrect':
        combined['total_count_confidence_pred'].loc[
            combined.correctness == 'incorrect'].reset_index(drop=True),
        'low quality':
        table_matched_unreadable_filtered['total_count_confidence_pred'].
        sort_values().reset_index(drop=True),
        'undercounted':
        combined['total_count_confidence_pred'].loc[
            combined.error_types_pred == 'Undercounted'].reset_index(
                drop=True),
        'overcounted':
        combined['total_count_confidence_pred'].loc[
            combined.error_types_pred == 'Overcounted'].reset_index(drop=True),
        'total readable':
        combined['total_count_confidence_pred'].sort_values().reset_index(
            drop=True)
    }


def _groupProbabilitiesByErrorTypeForTotalCount(
    table_matched_readable: pd.DataFrame,
    table_matched_unreadable: pd.DataFrame,
    total_count_col_names: Tuple[str,
                                 str] = ("total_count_pred", "total_count_gt")) -> Dict[str, pd.Series]:
    """
    Classify and then organize the prediction error and the readable, unreadable entries.
    Separate incorrect and correct predictions, then separate the incorrect predictions into 'overcounted' and 'undercounted'.
    Return a dict containing {'correct', 'incorrect', 'low quality', 'undercounted', 'overcounted', 'total readable'}
    """
    total_count_col_pred, total_count_col_gt = total_count_col_names
    assert total_count_col_pred and total_count_col_gt, "total_count_col_names cannot contain empty strings."

    # remove rows with nan values for the specified rows
    table_matched_readable_filtered = removeRowsWithNanColValues(
        table_matched_readable, ['total_count_confidence_pred'])
    logging.info(
        f'Removed {len(table_matched_readable) - len(table_matched_readable_filtered)} rows with nan values from table_matched_readable'
    )
    # set it up so any value > 3 is just 3
    table_matched_readable_filtered[
        total_count_col_pred] = table_matched_readable_filtered[
            total_count_col_pred].where(
                table_matched_readable_filtered[total_count_col_pred].astype(
                    int) < 3, 3).astype(int)

    # label each prediction as either correct or incorrect based on the corresponding groundtruth
    correctness_pred = pd.Series(np.where(
        table_matched_readable_filtered[total_count_col_gt] ==
        table_matched_readable_filtered[total_count_col_pred], 'correct',
        'incorrect'),
                                 name='correctness')

    table_error_stats = pd.concat([
        table_matched_readable_filtered[total_count_col_pred],
        table_matched_readable_filtered[total_count_col_gt],
        table_matched_readable_filtered['total_count_confidence_pred'].
        reset_index(drop=True),
        correctness_pred.reset_index(drop=True)
    ],
                                  axis=1,
                                  join='inner')
    # divide the incorrect into overcounting and undercounting errors
    conditions = [
        table_matched_readable_filtered[total_count_col_pred] <
        table_matched_readable_filtered[total_count_col_gt],
        table_matched_readable_filtered[total_count_col_pred] >
        table_matched_readable_filtered[total_count_col_gt]
    ]
    choices = ["Undercounted", "Overcounted"]
    table_error_stats["error_types_pred"] = np.select(conditions,
                                                      choices,
                                                      default="Correct")

    # remove rows with nan values for the specified rows
    table_matched_unreadable_filtered = removeRowsWithNanColValues(
        table_matched_unreadable, ['total_count_confidence_pred'])
    logging.info(
        f'Removed {len(table_matched_unreadable) - len(table_matched_unreadable_filtered)} rows with nan values from table_matched_readable'
    )
    if table_matched_unreadable_filtered[
            'total_count_confidence_pred'].dtype != np.float64:
        table_matched_unreadable_filtered[
            'total_count_confidence_pred'] = table_matched_unreadable_filtered[
                'total_count_confidence_pred'].str.replace('%',
                                                           '').astype(float)
    if table_error_stats['total_count_confidence_pred'].dtype != np.float64:
        table_error_stats['total_count_confidence_pred'] = table_error_stats[
            'total_count_confidence_pred'].str.replace('%', '').astype(float)

    return {
        'correct':
        table_error_stats['total_count_confidence_pred'].loc[
            table_error_stats.correctness == 'correct'].reset_index(drop=True),
        'incorrect':
        table_error_stats['total_count_confidence_pred'].loc[
            table_error_stats.correctness == 'incorrect'].reset_index(
                drop=True),
        'low quality':
        table_matched_unreadable_filtered['total_count_confidence_pred'].
        sort_values().reset_index(drop=True),
        'undercounted':
        table_error_stats['total_count_confidence_pred'].loc[
            table_error_stats.error_types_pred == 'Undercounted'].reset_index(
                drop=True),
        'overcounted':
        table_error_stats['total_count_confidence_pred'].loc[
            table_error_stats.error_types_pred == 'Overcounted'].reset_index(
                drop=True),
        'total readable':
        table_error_stats['total_count_confidence_pred'].sort_values(
        ).reset_index(drop=True)
    }


def groupProbabilitiesByErrorType(
    count_type: CountType,
    table_matched_readable: pd.DataFrame,
    table_matched_unreadable: pd.DataFrame,
    total_count_col_names: Tuple[str,
                                 str] = ("total_count_pred", "total_count_gt")) -> Dict[str, pd.Series]:
    assert count_type in iter(
        CountType), f"{count_type} Count_Type is not supported."
    if count_type == CountType.FRONT_REAR_COUNTS:
        return _groupProbabilitiesByErrorTypeForFrontRearCounts(
            table_matched_readable, table_matched_unreadable,
            total_count_col_names)
    elif count_type == CountType.TOTAL_COUNT:
        return _groupProbabilitiesByErrorTypeForTotalCount(
            table_matched_readable, table_matched_unreadable,
            total_count_col_names)
