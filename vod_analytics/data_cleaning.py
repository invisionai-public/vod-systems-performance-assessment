from typing import List

import numpy as np
import pandas as pd


def removeRowsWithNanColValues(data_to_filter: pd.DataFrame,
                               cols_to_filter_by: List[str]) -> pd.DataFrame:
    '''check for and remove any nan or empty data points'''
    # done explicitly to avoid `SettingWithCopyWarning`
    data_to_filter = data_to_filter.copy()
    for col_name in cols_to_filter_by:
        # replace all 'nan' strings with np.nan
        data_to_filter[col_name].replace('', np.nan, inplace=True)
        data_to_filter[col_name].replace('(?i)nan',
                                         np.nan,
                                         regex=True,
                                         inplace=True)
        data_to_filter[col_name].replace('(?i)n/a',
                                         np.nan,
                                         regex=True,
                                         inplace=True)
        data_to_filter[col_name].replace('(?i)no window',
                                         np.nan,
                                         regex=True,
                                         inplace=True)

        if (data_to_filter[col_name].isna().any()):
            nan_values = data_to_filter[data_to_filter[col_name].isna()]
            print(f"Some {col_name} values = nan. Removing {len(nan_values)}"
                  f"({len(nan_values)/len(data_to_filter)*100:.3f}%) items.")
            print("Removing...")
            print(nan_values)
            data_to_filter = data_to_filter.dropna(
                subset=[col_name]).reset_index(drop=True)
        else:
            print(f"All {col_name} values valid.")

    print(f"Processing {len(data_to_filter)} samples")
    return data_to_filter
