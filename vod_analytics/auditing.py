from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List, Tuple, Optional, Union, Literal

import dacite
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
import yaml

from vod_analytics.audit_helpers import HeadersAndFormats, HovLabelingRules, HovPredictionRules, TrafficTable


@dataclass
class AuditConfigs:

    @dataclass
    class MatchConfigs:
        ts_timedelta_tol: str
        direction: Literal["nearest", "forward", "backward"] = "nearest"

    groundtruth_format: HeadersAndFormats
    prediction_format: HeadersAndFormats
    gt_pred_matching: MatchConfigs
    labels: Dict[str, List[str]]
    HOV_labeling_rules: HovLabelingRules
    HOV_prediction_rules: HovPredictionRules

    def __post_init__(self) -> None:
        #  parse rules (grammar-complaince is asserted as a result)
        self.HOV_labeling_rules.parse(allowed_vars_and_vals=self.labels)
        self.HOV_prediction_rules.parse(allowed_vars_and_vals={var: None for var in self.prediction_format.hov_rules})

        # check consistency btwn gt format and labels and labeling rules
        assert set(self.groundtruth_format.hov_rules.keys()) == set(self.labels.keys()), \
            "Groundtruth hov rules column headers must match label names"
        

    def from_yaml(sample_file: Path) -> 'AuditConfigs':
        with open(sample_file, "r") as stream:
            contents = yaml.safe_load(stream)
        return dacite.from_dict(AuditConfigs, contents)


def hov_confusion_matrix(hov_gt: List[str], hov_pred: List[str], labels_gt: List[str], labels_pred: List[str]
                         ) -> pd.DataFrame:
    """ returns a confusion matrix as a pandas DataFrame with labeled truth columns and prediction columns. """

    labels = sorted(set([*labels_gt, *labels_pred]))
    CM_table = pd.DataFrame(confusion_matrix(hov_gt, hov_pred, labels=labels).transpose(),
        columns=[f"true_{l}" for l in labels], index=[f"pred_{l}" for l in labels])
    
    return CM_table.loc[[f"pred_{l}" for l in labels_pred], [f"true_{l}" for l in labels_gt]].copy()


def report_hov_performance(hov_gt: List[str], hov_pred: List[str]) -> Dict[str, pd.DataFrame]:
    """ All the metrics are computed from these two confusion matrices (numbers are exemplary):

    |pred_classes    |true_LOV |true_HOV |true_UNKNOWN |
    | :---           | :---    | :---    | :---        |
    |pred_LOV_sure   |     491 |       3 |          12 |
    |pred_LOV_maybe  |     169 |       2 |          49 |
    |pred_HOV        |      44 |     578 |          37 |
    |pred_UNKNOWN    |      40 |      69 |         341 |

    which is referred to as CM43, and its reduced version CM33:

    |pred_classes    |true_LOV |true_HOV |true_UNKNOWN |
    | :---           | :---    | :---    | :---        |
    |pred_LOV        |     660 |       5 |          61 |
    |pred_HOV        |      44 |     578 |          37 |
    |pred_UNKNOWN    |      40 |      69 |         341 |

    All the metrics are computed based on the elements of these two matrices and refered to by index and column names, 
    so refer to the code itself for the definition of the metrics.
    """

    CM43 = hov_confusion_matrix(hov_gt, hov_pred,
        labels_gt=["LOV", "HOV", "UNKNOWN"], labels_pred=["LOV_sure", "LOV_maybe", "HOV", "UNKNOWN"])
    CM33 = hov_confusion_matrix(hov_gt, [h.split("_")[0] for h in hov_pred],  # drops _sure and _maybe
        labels_gt=["LOV", "HOV", "UNKNOWN"], labels_pred=["LOV", "HOV", "UNKNOWN"])
    
    def tpr_and_fpr_from_square_cm(confusion_matrix: pd.DataFrame, true_col_name: str) -> Tuple[float, float]:
        true_mask = confusion_matrix.columns == true_col_name
        assert true_mask.sum() == 1, \
            f"{true_col_name} must match exactly one of column names: {confusion_matrix.columns}"
        false_mask = confusion_matrix.columns != true_col_name
        conf_mat_arr = confusion_matrix.to_numpy()
        tpr = conf_mat_arr[true_mask, true_mask].sum() / conf_mat_arr[:, true_mask].sum()
        fpr = conf_mat_arr[true_mask, false_mask].sum() / conf_mat_arr[:, false_mask].sum()
        return tpr, fpr

    metrics = {
        "Provable violators caught in the LOV-sure bucket": CM43.loc["pred_LOV_sure", "true_LOV"] \
            / CM43.loc[:, "true_LOV"].sum(),
        "Provable violators caught in the LOV-maybe bucket": CM43.loc["pred_LOV_maybe", "true_LOV"] \
            / CM43.loc[:, "true_LOV"].sum(),
        "Provable Violators not caught (miss rate)": CM33.loc[["pred_HOV", "pred_UNKNOWN"], "true_LOV"].sum() \
            / CM33.loc[:, "true_LOV"].sum(),
        "LOV-sure bucket precision": CM43.loc["pred_LOV_sure", "true_LOV"] / CM43.loc["pred_LOV_sure", :].sum(), 
        "LOV-maybe bucket precision": CM43.loc["pred_LOV_maybe", "true_LOV"] / CM43.loc["pred_LOV_maybe", :].sum(),
        "LOV-sure bucket FPR": CM43.loc["pred_LOV_sure", ["true_HOV", "true_UNKNOWN"]].sum() \
            / CM43.loc[:, ["true_HOV", "true_UNKNOWN"]].stack().sum(),
        "LOV-maybe bucket FPR": CM43.loc["pred_LOV_maybe", ["true_HOV", "true_UNKNOWN"]].sum() \
            / CM43.loc[:, ["true_HOV", "true_UNKNOWN"]].stack().sum(),
        "Overall LOV predictions precision": CM33.loc["pred_LOV", "true_LOV"] / CM33.loc["pred_LOV", :].sum(),
        "Overall LOV predictions TPR and FPR": tpr_and_fpr_from_square_cm(CM33, "true_LOV"),
        "HOV predictions TPR and FPR": tpr_and_fpr_from_square_cm(CM33, "true_HOV"),
        "UNKNOWN predictions TPR and FPR": tpr_and_fpr_from_square_cm(CM33, "true_UNKNOWN"),
        "Unreadable rate according to groundtruth": CM33.loc[:, "true_UNKNOWN"].sum() / CM33.stack().sum(),
        "Unreadable rate according to predictions": CM33.loc["pred_UNKNOWN", :].sum() / CM33.stack().sum(),
        "Undetected unreadables in the LOV-sure bucket": CM43.loc["pred_LOV_sure", "true_UNKNOWN"] \
            / CM43.loc["pred_LOV_sure", :].sum(),
        "Undetected unreadables in the LOV-maybe bucket": CM43.loc["pred_LOV_maybe", "true_UNKNOWN"] \
            / CM43.loc["pred_LOV_maybe", :].sum(),
        "Min-max true violation rate": (CM33.loc[:, "true_LOV"].sum() / CM33.stack().sum(),
                                        CM33.loc[:, ["true_LOV", "true_UNKNOWN"]].stack().sum() / CM33.stack().sum()),
        "Best-guess true violation rate": CM33.loc[:, "true_LOV"].sum() \
            / CM33.loc[:, ["true_LOV", "true_HOV"]].stack().sum(),
        "Min-max predicted violation rate": ((CM33.loc["pred_LOV", :].sum() / CM33.stack().sum(),
                                     CM33.loc[["pred_LOV", "pred_UNKNOWN"], :].stack().sum() / CM33.stack().sum())),
        "Best-guess predicted violation rate": CM33.loc["pred_LOV", :].sum() \
            / CM33.loc[["pred_LOV", "pred_HOV"], :].stack().sum(),                             
    }

    def print_into_percentage_or_percentage_interval(value_or_values: Union[float, Tuple[float, float]]) -> str:
        one_dim_array = np.expand_dims(np.array(value_or_values), axis=0).flatten()
        return " - ".join([f"{value * 100:.1f}%" for value in one_dim_array])

    metrics_table = pd.DataFrame(metrics.values(), index = metrics.keys(), columns=[""])
    metrics_table.iloc[:, 0] = metrics_table.iloc[:, 0].apply(print_into_percentage_or_percentage_interval)
    return {"metrics": metrics_table, "CM43": CM43, "CM33": CM33}


def run_audit_pipeline(gt_file: Path, pred_file: Path, config_file: Path) -> pd.DataFrame:
    configs = AuditConfigs.from_yaml(config_file)
    table_gt = TrafficTable(tag="review_gt", file=gt_file, headers_and_formats=configs.groundtruth_format)
    table_pred = TrafficTable(tag="VOD", file=pred_file, headers_and_formats=configs.prediction_format)

    table_gt.add_hov_determination_inplace(configs.HOV_labeling_rules.evaluate)
    table_pred.add_hov_determination_inplace(configs.HOV_prediction_rules.evaluate)

    matched, _ = table_pred.match_timestamps_against(table_gt,
        tolerance=pd.Timedelta(configs.gt_pred_matching.ts_timedelta_tol),
        direction=configs.gt_pred_matching.direction)
    report = report_hov_performance(matched.loc[:, f"hov_determination_{table_gt._tag}"].to_list(),
                                    matched.loc[:, f"hov_determination_{table_pred._tag}"].to_list())
    return report
