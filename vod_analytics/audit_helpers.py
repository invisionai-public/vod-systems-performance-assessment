from ast import literal_eval
from dataclasses import dataclass, field, fields
import operator as op
from pathlib import Path
from typing import Dict, Any, List, Tuple, Callable, NewType, Literal, Optional, Set

import numpy as np
import pandas as pd


@dataclass
class HeadersAndFormats:
    """ Dataclass that contains the header names and formats for parsing timestamp, hov rule variables and miscellaneous
    columns of a spreadsheet, e.g.
    ```python
    HeadersAndFormats(timestamp={"Date": "%Y-%m-%d", "Time": "%H:%M:%S.%f"},
                      hov_rules={"visibility": "percentage", "p_violation_HOV2": "percentage"},
                      other={"count": "number"})
    ```
   describes such a file:
    ```
    Date, Time, visibility, p_violation_HOV2, count
    2024-09-30, 01:00:00.000, 65.0%, 75.0%, 1
    2024-09-30, 01:05:00.000, 40.0%, 80.0%, 3
    2024-09-30, 02:06:00.000, 90.0%, 70.0%, 1
    ```
    """
    timestamp: Dict[str, str]
    hov_rules: Dict[str, Literal["string", "number", "percentage"]]
    other: Dict[str, Literal["string", "number", "percentage"]]


class TrafficTable:
    """ Utility class to load tabular traffic data of from a CSV or XLS file with special methods for parsing
    timestamps, columns and HOV stats as per the specified HeadersAndFormats (all headers must exist in the file).

    - public attributes: timestamp_col, hov_col, dataframe
    - public methods: and methods: delta_col, drop_rows_inplace,add_delta_to_adjacent_vehicles_inplace,
    add_hov_determination_inplace, match_timestamps_against, print_summary
    """
        
    def __init__(self, tag: str, file: Path, headers_and_formats: HeadersAndFormats):
        loader = {".csv": pd.read_csv, ".xls": pd.read_excel}
        assert file.suffix in loader.keys(), f"only {list(loader.keys())} file extensions are supported."
        
        self._tag = tag  # used as a suffix for auto-parsed columns and a short name in logs
        self.timestamp_col = f"timestamp_{tag}"
        df = loader[file.suffix](file, dtype=str,
                                 usecols=[*headers_and_formats.timestamp,
                                          *headers_and_formats.hov_rules, *headers_and_formats.other])
        
        # add the timestamp column
        ts_func = lambda x: pd.to_datetime("_".join(x), format="_".join(headers_and_formats.timestamp.values()))
        df[self.timestamp_col] = df[headers_and_formats.timestamp.keys()].apply(ts_func, axis=1)
        df.sort_values(self.timestamp_col, inplace=True)
        df.drop(headers_and_formats.timestamp.keys(), axis=1, inplace=True)

        # parse non-string columns properly
        non_ts_cols = {**headers_and_formats.hov_rules, **headers_and_formats.other}
        for col_name, type_name in non_ts_cols.items():
            if type_name == "number":
                df[col_name] = df[col_name].astype(float)
            elif type_name == "percentage":
                df[col_name] = df[col_name].str.strip('%').astype(float) / 100.0
        
        self._df = df
        self._hov_rules_variables = list(headers_and_formats.hov_rules)
    
    @property
    def dataframe(self):
        return self._df

    def delta_col(self, adjacent_case: Literal["previous", "next", "nearest"]) -> str:
        """ name of the auxiliary column representing delta (elapsed seconds) from or to the adjacent vehicle."""
        return f"delta_to_{adjacent_case}_vehicle_{self._tag}"
    
    @property
    def hov_col(self) -> str:
        return f"hov_determination_{self._tag}"
    
    def add_delta_to_adjacent_vehicles_inplace(self) -> None:
        delta_col = self.delta_col
        delta_to_prev_veh = np.diff(self._df[self.timestamp_col])
        self._df[delta_col("previous")] = [np.nan, *delta_to_prev_veh]
        self._df[delta_col("previous")] = self._df[delta_col("previous")].dt.total_seconds()
        self._df[delta_col("next")] = [*delta_to_prev_veh, np.nan]
        self._df[delta_col("next")] = self._df[delta_col("next")].dt.total_seconds()
        self._df[delta_col("nearest")] = np.min((self._df[delta_col("previous")], self._df[delta_col("next")]), axis=0)
    
    def add_hov_determination_inplace(self, evaluation_rule: Callable[[Dict[str, Any]], str]) -> None:
        """ adds the HOV determination to the table's dataframe in a column named as specified by hov_col property.
        Note that an evaluation function must be provided that maps a dictionary with essential column keys to an
        HOV determination. """

        self._df[self.hov_col] = self._df[self._hov_rules_variables].apply(lambda x: evaluation_rule(dict(x)), axis=1)
    
    def drop_rows_inplace(self, drop_condition: pd.Series) -> None:
        self._df.drop(self._df[drop_condition].index, inplace=True)
    
    def match_timestamps_against(self, other: "TrafficTable", tolerance: pd.Timedelta,
            direction: Literal["nearest", "forward", "backward"]="nearest") -> Tuple[pd.DataFrame, pd.DataFrame]:
        """ matches the table against another (used as the left-join reference) and returns the matched and unmatched
        dataframes. """

        matching = pd.merge_asof(other._df, self._df,
                            left_on=other.timestamp_col, right_on=self.timestamp_col,
                            tolerance=tolerance, direction=direction)
        matched = matching[matching[self.timestamp_col].notnull()].copy()
        unmatched = matching[matching[self.timestamp_col].isnull()].copy()
        print(f"Out of {len(other._df)} samples of interest in {other._tag}, ",
            f"{len(matched)} were matched to an entry in {self._tag}.")

        return matched, unmatched

    def print_summary(self) -> None:
        print(f"{self._tag} data contains {len(self._df.index)} entries in the datetime range:")
        print([self._df.loc[self._df.index[i], self.timestamp_col] for i in [0, -1]])
        print(f"with {len(self._df.index) - len(np.unique(self._df[self.timestamp_col]))} repeated timestamps.")

def add_rule_parse_and_eval(original_class):
    """ decorator to add rule parsing and evaluation capability to dataclasses that define HOV business rules."""
    
    UnivariateCondition = NewType("UnivariateCondition", Tuple[str, Callable[[Any, Any], bool], Any])
    # e.g. ("count", op.eq, "2+"), i.e. variable_name, operation and operand

    MultivariateRule = NewType("MultiVariateRule", Tuple[str, List[UnivariateCondition]])
    # e.g. ("HOV", [("count", op.eq, "2+"), ("visibility", op.eq, "poor")]) 
    
    def parse(self, allowed_vars_and_vals: Dict[str, Optional[List[str]]]=None) -> None:
        """ parses HOV rules, i.e. a collection of multivariate rules composed of ANDed univariate conditions. The input
        'self' originates from a configuration such as:
        ```
        LOV:
          - count: "== '1'"
            visibility: "in ['medium', 'good']"
        HOV:
          - count: "=='2'"
            visibility: "in ['medium', 'good']"
          - count: "=='2+'"
            visibility: "== 'poor'"
        ```
        loaded into dataclass fields as per the 'original_class'. Each field value is of type List[Dict[str, str]]),
        e.g. the LOV field above has this value: [{"count": "== '1'", "visibility": "in ['medium', 'good']"}] where 
        dictionary values like "== '1'" are referred to as the operator_operand_string.
        Finally, these values get parsed and reorganized into a list of MultivariateRules (self._parsed_rules), e.g.:
        ```
        [
            ("LOV", [("count", op.eq, "1"), ("visibility", lambda x, y: x in y, ["medium", "good"])]),
            ("HOV", [("count", op.eq, "2"), ("visibility", lambda x, y: x in y, ["medium", "good"])]),
            ("HOV", [("count", op.eq, "2+"), ("visibility", op.eq, "poor")]),
        ]
        ```
        provided that each univariate condition (variable, operation and operand) complies to this grammar:
        - the variable name should belong to the 'allowed_variables';
        - only one of these 2-character operations is allowed: "<=", ">=", "==" or "in";
        - the operand is literally evaluatable;
        - if allowed choices are provided in 'allowed_vars_and_vals[variable]', the operand must be composed of those.
        """
        allowed_2_character_ops = {"<=": op.le, ">=": op.ge, "==": op.eq, "in": lambda x, y: x in y}
        parsed_rules: List[MultivariateRule] = []
        all_variables: Set[str] = set()
        for hov_field in fields(self)[0:-1]:
            unparsed_rules = getattr(self, hov_field.name)
            if unparsed_rules is None: continue
            for rule in unparsed_rules:
                multivariate_condition: List[UnivariateCondition] = []
                for variable, operator_operand_string in rule.items():
                    all_variables.add(variable)
                    assert operator_operand_string[0:2] in allowed_2_character_ops.keys(), \
                        f"{operator_operand_string} does not start with {list(allowed_2_character_ops.keys())}."
                    try:
                        operand = literal_eval(operator_operand_string[2:].strip())
                    except Exception as err:
                        raise ValueError(f"{operator_operand_string} not parsable due to: {err}.")
                    if allowed_vars_and_vals[variable] is not None:
                        operand_as_set = set([operand]) if isinstance(operand, str) else set(operand)
                        assert operand_as_set <= set(allowed_vars_and_vals[variable]), (f"Condition {variable}"
                            f"{operator_operand_string} has operand value outside: {allowed_vars_and_vals[variable]}.")
                    multivariate_condition.append(
                        UnivariateCondition((variable, allowed_2_character_ops[operator_operand_string[0:2]], operand)))
                parsed_rules.append(MultivariateRule((hov_field.name, multivariate_condition)))
        assert all_variables <= set(allowed_vars_and_vals), \
            f"Variables {all_variables} in conditions exceed allowed variables: {set(allowed_vars_and_vals)}."
        self._parsed_rules = parsed_rules
        self._all_variables = all_variables

    def evaluate(self, case_of_interest: Dict[str, Any]) -> str:
        """ Evaluates a case of interest against parsed rules and returns the rule-based HOV determination.
        If we consider a scenario where the entire rule base is composed of the 3 examples in the parse doc-string, i.e.
        - if count == 1 and visibility in ["medium", "good"]: return "LOV"
        - if count == 2 and visibility in ["medium", "good"]: return "HOV"
        - if count == 2+ and visibility in ["medium", "good"]: return "HOV"
        and assume the dataclass that gets decorated has 3 fields "LOV", "HOV" and "INVALID", any annotation dict with
        keys "count" and "visibility" can get checked against the rules in order (controlled by the order of the fields 
        in the original_class dataclass). This check may evaluate to "LOV" or "HOV" if one of the above condition is 
        met, otherwise "INVALID" (fallback state) is returned.
        """
        assert self._all_variables <= set(case_of_interest.keys()), \
            f"case of interest does not contain all the needed variables: {self.all_variables}"
        for rule in self._parsed_rules:
            hov_state, multivariate_condition = rule
            is_rule_met = all(operation(case_of_interest[variable_name],
                                        operand) for variable_name, operation, operand in multivariate_condition)
            if is_rule_met:
                return hov_state
        fallback_state = fields(self)[-1].name
        assert getattr(self, fallback_state) is None, "The last state must be reserved for when all other rules fail."
        return fallback_state

    # bestow rule parsing and evaluation to the rule class
    original_class.parse = parse
    original_class.evaluate = evaluate
    return original_class


UnparsedRules = Optional[List[Dict[str, str]]]


@dataclass
@add_rule_parse_and_eval
class HovLabelingRules:
    SKIP: UnparsedRules = None
    LOV: UnparsedRules = None
    HOV: UnparsedRules = None
    UNKNOWN: UnparsedRules = None
    INVALID: Literal[None] = field(default=None, init=False)


@dataclass
@add_rule_parse_and_eval
class HovPredictionRules:
    LOV_sure: UnparsedRules = None
    LOV_maybe: UnparsedRules = None
    HOV: UnparsedRules = None
    UNKNOWN: Literal[None] = field(default=None, init=False)
