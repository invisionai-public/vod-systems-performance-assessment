# How to Configure and Run a VOD Performance Audit

This document describes how to write an `audit_configs.yaml` for running a VOD system performance audit using the tools
in the vod_analytics package, i.e. the `audit_helpers` and `auditing` modules. It also discusses briefly how to run the
end-to-end audit and interpret the results.

## Groundtruh and prediction files format

The format of these two files is described using the `audit_helpers.HeadersAndFormats` dataclass with a yaml
representation like this:
```yaml
prediction_format:
  timestamp:
    Datetime: '%Y-%m-%d_%H:%M:%S.%f'
  hov_rules:  # these headers should match variable names in HOV prediction rules
    visibility: percentage
    p_violation_HOV2: percentage
  other:  # the below headers must also exist for the pred file to be loaded
    count: number
```
where the timestamp field can include one or multiple column headers and their formats. This one or multiple columns
will be concatenated and parsed by `audit_helper.TrafficTable` into the `TrafficTable.dataframe` pandas DataFrame as a
column named `timestamp_tag` with a `pd.Timestamp` type. The other columns under `hov_rules` and `other` will be parsed 
into the DataFrame while preserving the column name, e.g. visibility, p_violation_HOV2 and count. Only `string`, `number`
and `percentage` literals are allowed here. The first two are loaded as strings and floats respectively, while a 
percentage like `'65.4%'` is loaded as a float `0.654`. All columns are expected to exist, but only the `hov_rules`
affiliated columns are expected to be compatible with their respective HOV rules variables, i.e. with labeling rules for
the groundtruth file and with prediction rules for prediction files.

Predictions are matched against the groundtruth file using these settings:
```yaml
gt_pred_matching:
  ts_timedelta_tol: 1ms
  direction: nearest
```
that control the left-join operation (the gt file is the reference for `nearest`, `forward` and `backward` choices).

## Labels and HOV labeling (groundtruthing) rules

The vod_analytics package allows for defining an arbitrary set of semi-dependent labels such as:
```yaml
labels:
  count:
    - 0+
    - '1'  # quotes are needed on anything that might not be read automatically as a string
    - 1+
    - '2'
    - 2+
    - more
    - skip
  visibility:
    - poor
    - medium
    - good
    - skip
```
The above illustrative example lends itself well to an indepth analysis of HOV2 problems. For HOV3 problems, one can add
`'3'` and `'3+'` labels too. Having this said, the intention of this package is not to promote any particular set of 
labels or labeling scheme. Rather, the audit tools are designed to be flexible to allow users to design their own 
labeling choices as long as they comply with the rule composition grammar explained at the end of this subsection.

Going back to the current example, the notation `'+'` is used to convey the uncertainty when labeling the
occupancy truth based on images acquired by a vision-based VOD system. If an image (or set of images) acquired from
a vehicle is human-intelligible, the reviewer is required to use a solid number like `'1'` or `'2'`. On the other hand, 
if the vehicle image is not clearly revealing the occupancy, the reviewers are demanded to use `'i+'` where `i` is the
minimum number of passengers they can clearly see and the `+` sign signifies uncertainty (inability of knowing for sure
whether more hidden occupants exist). To keep the reviewers even more honest, this labeling scheme interrogates them about an extra 3-level `visibility` label to build in an intentional redundancy. This redundancy necessitates a poor visibility to be accompanied with an `'i+'` count, and a medium or good visibility with a solid integer count. A count of `'more'`, a.k.a
"more than 2+" which entails any of `'3'`, `'3+'`, `'4'` or so forth, can be accompanied by any visibility. 
With this dependency in mind, an annotation such as `{"count": "1", "visibility": "poor"}` is `INVALID`
because a poor visibility implies uncertainty, while a solid integer count of 1 implies certainty (only attainable
at good or medium visibility). This simple dependency/redundancy can be exploited to denoise the groundtruth.
For example, one reviewer can be asked to label count and another to label visibility; and, the annotation can be
assembled only if not INVALID. Or a single reviewer can be interrogated for both which would make them think harder through
integrating this validation logic into the annotation tool.

A set of labeling rules, not only defines all such valid combinations but also describes how they are mapped to the main
groundtruth HOV states of `'SKIP'`, `'LOV'`, `'HOV'`, `'UNKNOWN'` (and `'INVALID'`). The following python code snippet
shows how one would code the described logic to map an annotation dictionary to these states:
```python
def map_to_hov_gt_state(annotation: Dict[str, str]) -> Literal['SKIP', 'LOV', 'HOV', 'UNKNOWN', 'INVALID']:

    if annotation["count"] == 'skip' and annotation["visibility"] == 'skip':
        return 'SKIP'
    elif annotation["count"] == '1' and annotation["visibility"] in ['medium', 'good']:
        return 'LOV'
    elif (annotation["count"] == '2' and annotation["visibility"] in ['medium', 'good']
    ) or (annotation["count"] == '2+' and annotation["visibility"] == 'poor'
    ) or annotation["count"] == 'more':
        return 'HOV'
    elif annotation["count"] in ['0+', '1+'] and annotation["visibility"] == 'poor':
        return 'UNKNOWN'
    else:
        return 'INVALID'  # this is reserved for when the 'SKIP', 'LOV', 'HOV' and 'UNKNOWN' conditions are missed
```
If we remove the boilerplate and rewrite as a yaml string, these rules become:
```yaml
HOV_labeling_rules:
  SKIP:
    - count: "== 'skip'"
      visibility: "== 'skip'"
  LOV:
    - count: "== '1'"
      visibility: "in ['medium', 'good']"
  HOV:
    - count: "=='2'"
      visibility: "in ['medium', 'good']"
    - count: "=='2+'"
      visibility: "== 'poor'"
    - count: "=='more'"
  UNKNOWN:
    - count: "in ['0+', '1+']"
      visibility: "== 'poor'"
  # INVALID: no rules accepted here (keep commented or delete)
```
The above is an example of how `audit_helpers` and `auditing` modules expect the users to describe the HOV labeling rules.
Notice the particular grammar:
1. Labeling rules are expected to be listed under the 4 orderly categories of `'SKIP'`, `'LOV'`, `'HOV'`, `'UNKNOWN'`. The
order is enforced by the backend even if the yaml string ordering changes.
2. Under each category, a list of multivariate condition dictionaries is provided. For example, under `'HOV'` we have:
`[{"count": "=='2'", "visibility": "in ['medium', 'good']"}, {"count": "=='2+'", "visibility": "=='poor'"}, {"count": "=='more'"}]` 
3. Each multivariate condition dictionary must have label names as keys, e.g. only count and visibility are acceptable here
(although not all labels need to exist in every dictionary).
4. Each multivariate condition dictionary has string values such as `"in ['medium', 'good']"` which are referred to as
operation-operand strings.
5. The first 2 characters of the operation-operand string defines the operation and the rest becomes the operand. Only
`"=="`, `"<="`, `">="` and `"in"` are allowed as operations.
6. operands must be literally evaluatable to a string or a list of strings (or also numbers in case of prediction rules),
and these strings should be within the allowed options for a label, e.g. `"== 'excellent'"` is not allowed for the 
variable `visibility` in the above example.

Note that grammar mistakes in rules or annotations cause an exception to be raised when parsing `audit_helpers.HovLabelingRules`
or when evaluating an annotation into its HOV groundtruth state. This is different from a grammar-compliant annotation
dictionary that evaluates to `'INVALID'` based on successfully parsed grammar-compliant rules. See examples of each case
in the below code snippet:

```python
from vod_analytics.audit_helpers import HovLabelingRules

labels = {"count": ["0+", "1", "1+", "2", "2+", "more", "skip"], "visibility": ['poor', 'medium', 'good', 'skip']}

# for illustration purposes, we create a rule set that has a single LOV rule and everything else will be None (default)
single_rule = HovLabelingRules(LOV=[{"count": "== '1'", "visibility": "in ['medium', 'good']"}])
single_rule.parse(allowed_vars_and_vals=labels)
# => this parses successfully

annot_1_hov_gt = single_rule.evaluate({"count": "2", "visibility": "good"})
# => 'INVALID' because {"count": "2", "visibility": "good"} is grammar-compliant but does not satisfy any rule

annot_2_hov_gt = single_rule.evaluate({"total_count": "2", "visibility": "good"})
# => this raises an error because the variable "count" is missing

another_single_rule = HovLabelingRules(LOV=[{"count": "== '1'", "visibility": "== 'excellent'"}])
another_single_rule.parse(allowed_vars_and_vals=labels) 
# => this raises an error because 'excellent' is not an allowed option for visibility per the labels used in parsing
```


## Predictions and HOV prediction rules

Similar to labeling rules, HOV prediction rules are implemented in `audit_helpers.HovPredictionRules` using a decorated
dataclass with orderly fields of `'LOV_sure'`, `'LOV_maybe'`, `'HOV'` (and `'UNKNOWN'`) which define the HOV prediction
states. Compared to the HOV groundtruth states, the low occupancy vehicle (LOV) state is split into two, and the
fallback state is `'UNKNOWN'` instead of `'INVALID'`. Here is an example of a user's definition of such rules in the
YAML configuration:
```yaml 
HOV_prediction_rules: 
  LOV_sure:
    - p_violation_HOV2: ">= 0.9"
      visibility: ">=0.95"
  LOV_maybe:
    - p_violation_HOV2: ">= 0.5"
      visibility: ">=0.7"
  HOV:
    - p_violation_HOV2: "<= 0.499"
      visibility: ">= 0.7"
  # UNKNOWN: no rules accepted here (keep commented or delete)
```
Note the `'_sure'` and `'_maybe'` suffixes in `audit_helpers.HovPredictionRules` are devised to bestow flexibility in
HOV business rules, e.g. predictions evaluated to `'LOV_sure'` can be prioritized for enforcement or toll adjustment
after a single human review, but vehicle cases evaluated to `'LOV_maybe'` can be pursued only if two independent human
reviewers vet them to be indeed LOV. Again, the intention of this document is not to promote a particular set of business
rules or any particular set of prediction quantities. Intead, our main goal is to illustrate the possibilities offered
by the `vod_analytics` package for conducting a generic audit and setting the minimum number of necessary constraints for
that audit to be meaningful. Hence, if the LOV sub-division is not desired by the user, `'LOV_sure'` can be left empty 
in the configs which would leave `'LOV_maybe'` to be the sole LOV prediction category by the VOD system. 

The rule composition grammar is quite similar to what explained before about labeling rules. When working with number-valued
variables in the rules, unlike labeling rules, it is not necessary to provide a finite set of allowed values during parsing.
The variable names, however, are still needed. In such cases, an `allowed_vars_and_vals` dictionary with None values is
provided. See the below code snippet that illustrates this, as well as some evaluation result and error examples:

```python
from vod_analytics.audit_helpers import HovPredictionRules

# for illustration purposes, we create a rule set that has a single HOV rule and everything else will be None (default)
single_rule = HovPredictionRules(HOV=[{"p_violation_HOV2": "<= 0.499", "visibility": ">= 0.7"}])
single_rule.parse(allowed_vars_and_vals={"p_violation_HOV2": None, "visibility": None})
# => this parses successfully

vehicle_1_hov_pred = single_rule.evaluate({"p_violation_HOV2": 0.5, "visibility": 0.3})
# => 'UNKNOWN' because the prediction contains the necessary keys but does not meet the single rule so falls back to UNK

vehicle_2_hov_pred = single_rule.evaluate({"p_violation_HOV2": 0.5})
# => this raises an error because the visibility variable is missing

another_single_rule =  HovPredictionRules(HOV=[{"p_violation_HOV2": "< 0.499"}])
another_single_rule.parse(allowed_vars_and_vals={"p_violation_HOV2": None}) 
# => this raises an error because '<' is not an allowed 2-character operator

dual_hov_rule =  HovPredictionRules(HOV=[{"p_violation_HOV2": "<= 0.499"}, {"vehicle_type": "BUS"}])
dual_hov_rule.parse(allowed_vars_and_vals={"p_violation_HOV2": None, "vehicle_type": ["CAR", "TRUCK", "BIKE", "BUS"]})
# => this parses successfully
vehicle_3_hov_pred = dual_hov_rule.evaluate({"p_violation_HOV2": 0.9, "vehicle_type": "BUS"})
# => this evaluates to 'HOV' (even though the LOV probability is high, the vehicle is predicted as a BUS thus HOV)
```

## End-to-end audit pipeline and results

A well written yaml file allows the function `run_audit_pipeline` in the `auditing` module to run the entire pipeline
with the supplied groundtruth (gt) and prediction (pred) files. See the `audit_configs.yaml` and the gt and pred files
in the sample-data folder of the repo for a thorough example. The pipeline returns a report consisting of a metrics table
and two confusion matrices. Let's discuss the confusion matrices first. Remember the HOV groundtruth states and prediction
states, i.e. {`'SKIP'`, `'LOV'`, `'HOV'`, `'UNKNOWN'`, `'INVALID'`} and {`'LOV_sure'`, `'LOV_maybe'`, `'HOV'`, `'UNKNOWN'`}.
The confusion matrix `C43` is the result of arranging the multiclass classification results in truth columns and prediction
rows. Here is an example:

|pred_classes    |true_LOV |true_HOV |true_UNKNOWN |
| :---           | :---    | :---    | :---        |
|pred_LOV_sure   |     491 |       3 |          12 |
|pred_LOV_maybe  |     169 |       2 |          49 |
|pred_HOV        |      44 |     578 |          37 |
|pred_UNKNOWN    |      40 |      69 |         341 |

Note that skipped and invalid cases are dropped out from the classification results. In many cases, there is no substantial
distinction between operational business rules imposed on `'LOV_sure'` vs. `'LOV_maybe'` predictions, so it is useful
to sum the first two rows and also analyze this reduced symmetrical `CM33` matrix:

|pred_classes    |true_LOV |true_HOV |true_UNKNOWN |
| :---           | :---    | :---    | :---        |
|pred_LOV        |     660 |       5 |          61 |
|pred_HOV        |      44 |     578 |          37 |
|pred_UNKNOWN    |      40 |      69 |         341 |

Many metrics can be extracted from the rows and columns of these two matrices. Refer to the actual code in the 
`run_audit_pipeline.report_hov_performance` function for the definition of the metrics implemented in the report.
The code should be self-explanatory (clearer than an English description) since all metrics are ratios of elements in 
`CM34` and `CM33` DataFrame matrices as referenced by their row and column names. An example of a metrics table is
provided below:

|Metric                                              |Value          |
| :---                                               | :---          |
|"Provable violators caught in the LOV-sure bucket"  | 66.0%         |
|"Provable violators caught in the LOV-maybe bucket" | 22.7%         |
|"Provable Violators not caught (miss rate)"         | 11.3%         |
|"LOV-sure bucket precision"                         | 97.0%         |
|"LOV-maybe bucket precision"                        | 76.8%         |
|"LOV-sure bucket FPR"                               | 1.4%          |
|"LOV-maybe bucket FPR"                              | 4.7%          |
|"Overall LOV predictions precision"                 | 90.9%         |
|"Overall LOV predictions TPR and FPR"               | 88-7% - 6.0%  |
|"HOV predictions TPR and FPR"                       | 88.7% - 6.8%  |
|"UNKNOWN predictions TPR and FPR"                   | 77.7% - 7.8%  |
|"Unreadable rate according to groundtruth"          | 23.9%         |
|"Unreadable rate according to predictions"          | 24.5%         |
|"Undetected unreadables in the LOV-sure bucket"     | 2.4%          |
|"Undetected unreadables in the LOV-maybe bucket"    | 22.3%         |
|"Min-max true violation rate"                       | 40.5% - 64.5% |
|"Best-guess true violation rate"                    | 53.3%         |
|"Min-max predicted violation rate"                  | 39.6% - 64.1% |
|"Best-guess predicted violation rate"               | 52.4%         |
