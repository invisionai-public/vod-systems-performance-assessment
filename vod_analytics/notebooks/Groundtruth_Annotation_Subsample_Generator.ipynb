{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "-ky9JozXVsBa"
      },
      "source": [
        "# Groundtruth Template Generator for Annotation\n",
        "\n",
        "This notebook helps you generate a groundtruth template file to be filled out by human reviewers. The purpose is facilitating\n",
        "vehicle occupancy detection (VOD) performance assessment using natural traffic. The template is generated from a random \n",
        "balanced subsample of the VOD system prediction CSV file used as input. The license plate, `LPR_gt`, and  passenger count,\n",
        "i.e. `frontcount_gt` and `rearcount_gt`, columns in the template file are intentionally left empty so as to be filled by\n",
        "reviewers. The idea is to compare them later with their corresponding predictions, see the high-level workflow described\n",
        "in the *readme* for using all the tools in the repository.\n",
        "\n",
        "Here is the detailed workflow for using the current notebook:\n",
        "\n",
        "1. Download the latest version of the notebook from [here](https://gitlab.com/invisionai-public/vod-systems-performance-assessment/-/raw/main/Groundtruth_Annotation_Subsample_Generator.ipynb?inline=false).\n",
        "2. Launch Colab from [here](https://colab.research.google.com/notebooks/intro.ipynb?utm_source=scs-index#recent=true) and select the *Upload* tab and choose the file that you downloaded in the first step (it will most likely be in your *Downloads* folder).\n",
        "3. Choose a desired sample size. The default is `300` (means `100` random picks from each of total predicted \n",
        "occupancy classes 1, 2 and 3+). \n",
        "4. Next, press *Run all* from the *Runtime menu*.\n",
        "5. Next, upload the *prediction CSV* file when prompted. Note that this CSV file needs to contain all the columns listed\n",
        "in `REQUIRED_PRED_COLS`.\n",
        "\n",
        "This should result in generation of a semi-filled groundtruth template file. You can download this file from the left pane\n",
        "of Colab, submit it to your annotation team to go over each vehicle transit and fill the groundtruth values, and then\n",
        "use for performance assessment. The name of this CSV file should `gt_subsampleX` preppended to the name of the prediction\n",
        "CSV file used as input."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 16,
      "metadata": {
        "cellView": "form",
        "id": "m5ZU9DlRUFB-"
      },
      "outputs": [],
      "source": [
        "import pandas as pd\n",
        "import numpy as np\n",
        "from google.colab import files\n",
        "\n",
        "#@title Input Desired Subsample Size and Upload Prediction File { vertical-output: true }\n",
        "#@markdown Size of sample in template file\n",
        "sample_size = 300 #@param {type:\"integer\"}\n",
        "\n",
        "print('\\nPlease Upload the Predictions CSV File')\n",
        "uploaded_pred = files.upload()"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {},
      "outputs": [],
      "source": [
        "# requirements\n",
        "REQUIRED_PRED_COLS = ['Date', 'Time', 'Vehicle class', 'LPR', 'Total number of Passengers', 'Link to LPR file',\n",
        "                      'Link to picture of 1st row']\n",
        "RENAMED_OUTPUT_COLS = {'Date': 'Date_gt', 'Time': 'Time_gt', 'Link to LPR file':'Link to LPR image', \n",
        "                        'Link to picture of 1st row': 'Link to VOD image'}\n",
        "FINAL_OUTPUT_COLS = ['Date_gt', 'Time_gt', 'LPR_gt', 'frontcount_gt', 'rearcount_gt', 'Link to LPR image', 'Link to VOD image']\n",
        "VEHICLE_TYPES_OF_INTEREST = ['CAR', 'VAN']\n",
        "\n",
        "# read the prediction CSV file\n",
        "prediction_filename = list(uploaded_pred.keys())[0]\n",
        "df = pd.read_csv(prediction_filename, usecols=REQUIRED_PRED_COLS, dtype='str')\n",
        "\n",
        "# TODO: add some checks/assertions before getting into business\n",
        "\n",
        "#explicitly type non string columns\n",
        "df['Datetime'] = pd.to_datetime(arg=df['Date'] + ' ' + df['Time'])\n",
        "df['Total number of Passengers'] = df['Total number of Passengers'].astype('int')\n",
        "\n",
        "# select only vehicles of interest\n",
        "df = df[df['Vehicle class'].isin(VEHICLE_TYPES_OF_INTEREST)].dropna()\n",
        "\n",
        "# create occupancy class (1, 2 and 3+) column\n",
        "df['occupancy class'] = df['Total number of Passengers']\\\n",
        "                          .where(cond=df['Total number of Passengers'] > 0, other='indeterminate')\\\n",
        "                          .where(cond=df['Total number of Passengers'] < 3, other='3+')\n",
        "\n",
        "# create subsample dataframes\n",
        "seed = int(df.loc[0, 'Datetime'].timestamp()) # to have pseudorandom seed\n",
        "subsample_size = int(sample_size / 3)\n",
        "ones = df[df['occupancy class'] == 1].sample(n=subsample_size, random_state=seed)\n",
        "twos = df[df['occupancy class'] == 2].sample(n=subsample_size, random_state=seed)\n",
        "threes = df[df['occupancy class'] == '3+'].sample(n=subsample_size, random_state=seed)\n",
        "\n",
        "# recombine and sort subsamples\n",
        "subsample = pd.concat([ones, twos, threes]).sort_index()\n",
        "\n",
        "# keep only necessary columns and rename\n",
        "subsample['frontcount_gt'] = ''\n",
        "subsample['rearcount_gt'] = ''\n",
        "subsample['LPR_gt'] = ''\n",
        "subsample = subsample.rename(columns=RENAMED_OUTPUT_COLS)\n",
        "subsample = subsample[FINAL_OUTPUT_COLS]\n",
        "\n",
        "# the next two lines of code are specific to the CSV prediction from Invision's Dashboard (comment out/change if needed)\n",
        "subsample['Link to VOD image'] = subsample['Link to VOD image'].str.replace(\"row_image/1/\",\n",
        "                                                                            \"row_image/0/\").str.replace(\" \", \"%20\")\n",
        "subsample['Link to LPR image'] = subsample['Link to LPR image'].str.replace(\" \", \"%20\")\n",
        "\n",
        "# output template file\n",
        "output_filename = (f'gt_{sample_size}subsample_' + prediction_filename).replace('vehicles_FROM_', '')\n",
        "subsample.to_csv(output_filename, index=False)\n",
        "print(f\"\"\"GT template file for annotation generated successfully. \n",
        "The file is called {output_filename}.\n",
        "Download it from Colab's left pane.\"\"\")"
      ]
    }
  ],
  "metadata": {
    "colab": {
      "collapsed_sections": [],
      "name": "Invision AI Ground Truth Template File Generator.ipynb",
      "provenance": []
    },
    "kernelspec": {
      "display_name": "Python 3.9.13 64-bit",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.9.13"
    },
    "vscode": {
      "interpreter": {
        "hash": "36cf16204b8548560b1c020c4e8fb5b57f0e4c58016f52f2d4be01e192833930"
      }
    }
  },
  "nbformat": 4,
  "nbformat_minor": 2
}
