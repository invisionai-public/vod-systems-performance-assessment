# Creates docker container that mimics the Google Colab Jupyter Notebook environment
FROM python:3.10.12

COPY scripts/ /vod-assessment/scripts
COPY vod_analytics/ /vod-assessment/vod_analytics
COPY requirements.txt /vod-assessment
COPY tests/ /vod-assessment/tests

WORKDIR /vod-assessment
RUN python3 -m pip install -r requirements.txt